import re

file_contents = open("actorcritic.txt").read()

average = lambda x : sum(x) / len(x)

print(len(list(map(float, re.findall("Stability rate: ([0-9]*.[0-9]*)", file_contents)))))

stable = average(list(map(float, re.findall("Stability rate: ([0-9]*.[0-9]*)", file_contents))))
	

success = average(list(map(float, re.findall("Success rate: ([0-9]*.[0-9]*)", file_contents))))


angle = average(list(map(float, re.findall("Angle error: ([0-9]*.[0-9]*)", file_contents))))


iterations = average(list(map(float, re.findall("Iterations: ([0-9]*.[0-9]*)", file_contents))))


infeasible = average(list(map(float, re.findall("Infeasible rate: ([0-9]*.[0-9]*)", file_contents))))


failed_ik_rate = average(list(map(float, re.findall("Failed IK rate: ([0-9]*.[0-9]*)", file_contents))))


print("stable", stable)

print("success", success)

print("angle", angle)

print("iterations", iterations)

print("infeasible", infeasible)

print("failed_ik_rate", failed_ik_rate)
