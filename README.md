# INSTALL

1. Install PyRep: [https://github.com/stepjam/PyRep](https://github.com/stepjam/PyRep)
2. `pip install -r requirements.txt`

# RUN

Run `python learn_to_grasp.py`

`--train placement`: Trains placement network

`--mode analytical`: Collects data using analytical rotation for transformation rotation required to make an obejct upright

`--mode evaluate`: Evaluate performance of saved placement rotation network

`--mode isstable`: Collects data for binary classification of if a pose is stable if dropped
