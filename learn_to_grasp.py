import argparse
from datetime import datetime
import os
import os.path as osp

# from gwi.evalaute_binary import evalute_binary
from gwi.scenes import PyRepGraspSetup
from gwi.training import Supervisor
from gwi.models import PlacementNetwork
# from gwi.baseline import run_baseline
import gwi.models, wandb

parser = argparse.ArgumentParser()
parser.add_argument('--resume', type=str, default=None)
parser.add_argument('--train', type=str, default=None)
parser.add_argument('--mode', type=str, default=None)
parser.add_argument('--loop', type=str, default=None)
parser.add_argument('--id', type=str, default=None)
parser.add_argument('--dataroot', type=str, default="A")
parser.add_argument('--model', type=str, default="pix2pix")

args = parser.parse_args()

SCENE_FILE = osp.join(osp.dirname(osp.abspath(__file__)),
                    'data', 'vrep', 'scenes', 'panda_grasp.ttt')
MESH_PATH = osp.join(osp.dirname(osp.abspath(__file__)), 'data/objects/dexnet_adv/')

if args.train == 'placement':

    # Make directory to save models
    time = datetime.now().strftime("%Y-%m-%d-%H:%M")
    model_path = 'models/' + time + '/'
    try:
        os.mkdir(model_path)
    except:
        pass
    # Create PyRep scene
    scene = PyRepGraspSetup(SCENE_FILE, MESH_PATH, headless=True)
    trainer = Supervisor(scene)

    # Create and load network and training data
    model = PlacementNetwork(model_path)
    if args.resume == 'true':
        model.import_model('model.pt')
        print('Imported model!')
    # model.get_training_data()
    # model.train(50)
    # Train model
    while True:
        # Evaluate model
        model.network.eval()
        # success_rate, angle_error = trainer.evaluate_gripper_placement(250, model)
        success_rate_cl, angle_error_cl = trainer.evaluate_gripper_placement(5, model, max_closed_loop=15)
        break
        wandb.log({'Success': success_rate}, step=model.epoch)
        wandb.log({'Success cl': success_rate_cl}, step=model.epoch)

        wandb.log({'Angle error': wandb.Histogram(angle_error)}, step=model.epoch)
        wandb.log({'Angle error cl': wandb.Histogram(angle_error_cl)}, step=model.epoch)

        # Train model
        model.train(50)
    
elif args.train == "binary":

    scene = PyRepGraspSetup(SCENE_FILE, MESH_PATH, headless=True)

    # evalute_binary(scene)

elif args.train == "baseline":

    scene = PyRepGraspSetup(SCENE_FILE, MESH_PATH, headless=False)

    # run_baseline(scene)

elif args.mode == 'retest':
    # Make directory to save models
    time = '2020-09-20-20:42'
    model_path = '/placingobjects/models/' + time + '/'
    dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + model_path

    ordered_path = sorted(list(os.listdir(dir_path)), key=lambda x: int(x[:-3]))

    # Create PyRep scene
    scene = PyRepGraspSetup(SCENE_FILE, MESH_PATH, headless=False)
    trainer = Supervisor(scene)

    # Create and load network and training data
    model = PlacementNetwork(model_path)
    print('Imported model @ epoch ', model.epoch)

    # Evaluate model
    # model.network.eval()
    # success_rate, angle_error = trainer.evaluate_placement(250, model)
    # success_rate_cl, angle_error_cl = trainer.evaluate_placement(250, model, max_closed_loop=15)

    # wandb.log({'Success': success_rate}, step=model.epoch)
    # wandb.log({'Success cl': success_rate_cl}, step=model.epoch)

    # wandb.log({'Angle error': wandb.Histogram(angle_error)}, step=model.epoch)
    # wandb.log({'Angle error cl': wandb.Histogram(angle_error_cl)}, step=model.epoch)

    # Train model
    for p in ordered_path:
        if int(p[:-3]) == 125:

            model.import_model(dir_path + '125.pt')#models/' + time + '/' + p)
            print('Imported model @ epoch ', model.epoch)

            # Evaluate model
            model.network.eval()
            # success_rate, angle_error = trainer.evaluate_placement(250, model)
            success_rate_cl, angle_error_cl = trainer.evaluate_placement(300, model, max_closed_loop=15)

            # wandb.log({'Success': success_rate}, step=model.epoch)
            wandb.log({'Success cl': success_rate_cl}, step=model.epoch)

            # wandb.log({'Angle error': wandb.Histogram(angle_error)}, step=model.epoch)
            wandb.log({'Angle error cl': wandb.Histogram(angle_error_cl)}, step=model.epoch)


else:    
    scene = PyRepGraspSetup(SCENE_FILE, MESH_PATH, headless=False)
    trainer = Supervisor(scene, args.mode, args.loop, args.id)

    trainer.run()
