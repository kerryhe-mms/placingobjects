import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data
import torch.nn.functional as F
import torchvision
import gc
print("import")
import wandb
import cv2

import numpy as np

import torchvision.models as models
from torchvision import transforms
from PIL import Image
import matplotlib.pyplot as plt
import h5py
import os
print("impor1")
from torch import nn
from torch.autograd import Variable
from torch.utils import data
from torch.utils.data import Dataset
from torchvision.models.resnet import model_urls

class new_PCNN(nn.Module):
    def __init__(self):
        super().__init__()
        self.resnet = torchvision.models.resnet50(pretrained=True)
        self.resnet.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.resnet.fc = nn.Linear(in_features=2048, out_features=1024, bias=True)

        self.linear_layers = nn.Sequential(
            nn.Linear(in_features=3072, out_features=1024, bias=True),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(in_features=1024, out_features=1, bias=True),
            nn.Sigmoid()
        )


    def forward(self, x1, x2, x3):
        x1 = self.resnet(x1)
        x2 = self.resnet(x2)
        x3 = self.resnet(x3)
        # x4 = self.resnet(x4)

        x1 = x1.view(x1.size(0), -1)
        x2 = x2.view(x2.size(0), -1)
        x3 = x3.view(x3.size(0), -1)
        # x4 = x4.view(x4.size(0), -1)

        x = torch.cat((x1, x2, x3), dim=1)
        x = self.linear_layers(x)
        # x = compute_rotation_matrix_from_ortho6d(x)

        return x


batch_size= 1
img_dimensions = 64

# Normalize to the ImageNet mean and standard deviation
# Could calculate it for the cats/dogs data set, but the ImageNet
# values give acceptable results here.
img_transforms = transforms.Compose([
    transforms.Resize((img_dimensions, img_dimensions)),
    transforms.ToTensor()
    # transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225] )
    ])

img_test_transforms = transforms.Compose([
    transforms.Resize((img_dimensions,img_dimensions)),
    transforms.ToTensor()
    # transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225] )
    ])

def check_image(path):
    try:
        im = Image.open(path)
        return True
    except:
        return False

def import_model(network, path):
    checkpoint = torch.load(path)

    if type(checkpoint) is dict:
        network.load_state_dict(checkpoint['state_dict'])
        # self.optimizer.load_state_dict(checkpoint['optimizer'])
        # self.epoch = checkpoint['epoch']
    else:
        network.load_state_dict(checkpoint)
    

validation_data_path = "/home/rhys/Desktop/placingobjects/data/stable_vs_unstable_3/validation/"
validation_data = torchvision.datasets.ImageFolder(root=validation_data_path,transform=img_test_transforms)

num_workers = 6
validation_data_loader = torch.utils.data.DataLoader(validation_data, batch_size=32, shuffle=True, num_workers=num_workers)
# test_data_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=False, num_workers=num_workers)
loss_fn = torch.nn.BCELoss()

if torch.cuda.is_available():
    device = torch.device("cuda") 
else:
    device = torch.device("cpu")

model = new_PCNN()
model.to(device)
import_model(model, "/home/rhys/Desktop/placingobjects/data/models/2020-09-29-00:40/2.pt")


model.eval()
num_correct = 0 
num_examples = 0
valid_loss = 0
for batch in validation_data_loader:
    # inputs, targets = batch
    x, targets = batch

    # print(batch[0].shape)
    x, targets = batch

    get_channel = lambda index : torch.from_numpy(np.expand_dims(x[:, index, :, :], 1) - 0.5)

    test_x = np.expand_dims(x[:, 0, :, :], 1)
    # print(test_x.max())
    # print((test_x - 0.5).max())

    x1 = get_channel(0)
    x2 = get_channel(1)
    x3 = get_channel(2)
    # inputs = inputs.to(device)
    x1 = x1.to(device)
    x2 = x2.to(device)
    x3 = x3.to(device)
    output = model(x1, x2, x3).squeeze()
    targets = targets.to(device)
    loss = loss_fn(output,targets.float()) 
    valid_loss += loss.data.item() * x1.size(0)

    # print(output)
    # print(output, targets)
    

    output = (output>0.5).float()
    correct = (output == targets.float()).float().sum()

    temp = x1.squeeze().cpu().numpy()
    # print(temp.min())
    # print(temp.max())
    
    temp = (temp + 0.5) * 128

    # temp = np.transpose(temp, (1, 2, 0))
    temp = cv2.resize(temp, (640, 640))
    # print(temp.shape)

    # cv2.imshow("temo", temp.astype(np.uint8))
    # cv2.waitKey(0)
    # print(targets)
    
    # correct = torch.eq(torch.max(F.softmax(output, dim=1), dim=1)[1], targets).view(-1)
    num_correct += correct #torch.sum(correct).item()
    num_examples += x1.shape[0]

valid_loss /= len(validation_data_loader.dataset)
print(valid_loss)