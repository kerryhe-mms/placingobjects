import math,random, numpy as np, cv2
import sys
import numpy as np, cv2
from scipy.ndimage.filters import maximum_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion
import matplotlib.pyplot as pp
from skimage.feature import peak_local_max
from scipy import ndimage as ndi
import matplotlib.pyplot as plt
from skimage.feature import peak_local_max
from skimage import data, img_as_float
from matplotlib import pyplot
from keras.utils import to_categorical
from keras.applications.vgg16 import VGG16
from keras.models import Model
from keras.layers import Dense
from keras.layers import Flatten
from keras import optimizers
from tensorflow import keras
from keras.optimizers import SGD
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator
from keras import applications
from keras.callbacks import ModelCheckpoint
import cv2
from keras import backend as K
from scipy.spatial.transform import Rotation
from keras.backend.tensorflow_backend import set_session
from keras.backend.tensorflow_backend import clear_session
from keras.backend.tensorflow_backend import get_session
import tensorflow, gc
from .scenes import py_ang
import wandb


model = None
def reset_keras():
    global model
    sess = get_session()
    clear_session()
    sess.close()
    sess = get_session()

    try:
        del model # this is from global space - change this as you need
    except:
        pass

    gc.collect() # if it's done something you should see a number being outputted

    # use the same config as you used to create the session
    config = tensorflow.ConfigProto()
    config.gpu_options.per_process_gpu_memory_fraction = 1
    config.gpu_options.visible_device_list = "0"
    set_session(tensorflow.Session(config=config))

    prev_model = applications.VGG16(weights='imagenet', include_top=False, input_shape=(224,224,3))

    # build a classifier model to put on top of the convolutional model
    top_model = Sequential()
    top_model.add(Flatten(input_shape=prev_model.output_shape[1:]))
    top_model.add(Dense(256, activation='relu'))

    top_model.add(Dense(1, activation='sigmoid'))

    model = Model(inputs=prev_model.input, outputs=top_model(prev_model.output))

    model.load_weights("weights-improvement-22-0.97.hdf5")
    return model


def evalute_binary(scene):
    global model
    scene.reset()

    joint_pos = scene.robot.og_joints
    joint_pos[1]-=0.2
    scene.robot.robot.set_joint_positions(joint_pos)
    n_obj = len(scene.objects)

    scene.robot.robot.set_joint_positions(scene.robot.og_joints)
    scene.pr.step()

    success_count = 0
    stable_count = 0
    total_count = 0
    angle_history = []
    iteration_history = []
    for obj in scene.objects:
        obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
        obj.set_dynamic(False)

    for _ in range(500):
        obj_idx = random.randint(0, n_obj - 1)


        # Add noise to angle
        alpha = random.random()*2*np.pi
        beta = random.random()*np.pi
        gamma = random.random()*2*np.pi
        r = Rotation.from_euler('xyz', [alpha, beta, gamma], degrees=False)
        quat = list(r.as_quat())


        # Add noise to position
        pos = [0.5, 0, 0.5]
        pos[0] += (random.random()*2-1)*0.025
        pos[1] += (random.random()*2-1)*0.025
        pos[2] += (random.random()*2-1)*0.025


        # Place object in random pose in front of camera
        scene.objects[obj_idx].set_quaternion(quat)
        scene.objects[obj_idx].set_position(pos)
        scene.pr.step()

        indices = []

        full_list = []

        reset_keras()
        max_q = 0
        max_quat = None


        resolution = 30
        rm = Rotation.from_quat(scene.objects[obj_idx].get_quaternion())

        image = np.zeros((resolution,resolution))
        for i in range(resolution):
            for j in range(resolution):


                phi = i * 2 * math.pi / resolution
                theta = j * math.pi / resolution

                y = math.sin(phi)
                x = math.cos(phi)
                qx = x * math.sin(theta/2)
                qy = y * math.sin(theta/2)
                qz = 0
                qw = math.cos(theta/2)

                quat = [qx,qy,qz,qw]

                
                new_q = Rotation.from_quat(quat)

                new_rotation = (new_q * rm).as_quat()

                scene.objects[obj_idx].set_quaternion(list(new_rotation))

                scene.pr.step()

                depth_front = scene.front_camera.capture_depth().img
                depth_left = scene.left_camera.capture_depth().img
                depth_right = scene.right_camera.capture_depth().img

                size = (224,224)
                # Prepare image for network
                depth_left = cv2.resize(depth_left, dsize=size)
                depth_front = cv2.resize(depth_front, dsize=size)
                depth_right = cv2.resize(depth_right, dsize=size)
                x = np.stack((depth_left, depth_front, depth_right), axis=-1)
                x = x*2 - 0.5

                quality = 1 - model.predict(np.expand_dims(x, axis=0))

                if quality > max_q:
                    max_q = quality
                    max_quat = list(new_rotation)

                full_list.append(x)

                indices.append((i,j))


        im = 255 - image


        # Place object at lowest possible point
        scene.objects[obj_idx].set_dynamic(True)
        collision = True
        z = 0
        while collision:
            scene.objects[obj_idx].set_quaternion(max_quat)
            scene.objects[obj_idx].set_position([0, 0.5, z])
            scene.pr.step()

            distance = np.linalg.norm((np.array(scene.objects[obj_idx].get_position()) - np.array([0, 0.5, z])))
            angle_distance = 1 - np.abs(np.dot(max_quat, scene.objects[obj_idx].get_quaternion()))

            if angle_distance < 1e-05 and distance < 0.1:
                collision = False

            z += 0.005

        scene.objects[obj_idx].set_dynamic(False)
        scene.pr.step()
        scene.objects[obj_idx].set_dynamic(True)

        # Record movement while object is unstable
        position_history = []
        count = 0
        
        while count < 5:
            position_history.append(scene.objects[obj_idx].get_position())
            if len(position_history) > 1:
                p1 = np.array(position_history[-1])
                p2 = np.array(position_history[-2])
                disp = np.linalg.norm(p1 - p2)
                if (disp < 0.001):
                    count += 1
            scene.pr.step()


        # Calculate angle to upright vector
        gt = Rotation.from_euler('xyz', scene.objects_zero[obj_idx], degrees=True)
        gt_matrix = np.array(gt.as_matrix())
        axis = gt_matrix[2]

        final = Rotation.from_quat(scene.objects[obj_idx].get_quaternion())
        final_matrix = np.array(final.as_matrix())
        final_axis = np.matmul(final_matrix, axis)

        angle_final = py_ang([0, 0, 1], final_axis) * 180 / np.pi

        if (angle_final < 15):
            success_count += 1

        initial_matrix = np.array(Rotation.from_quat(max_quat).as_matrix())
        initial_axis = np.matmul(initial_matrix, axis)
        angle_initial = py_ang([0, 0, 1], initial_axis) * 180 / np.pi

        if np.abs(angle_final - angle_initial) < 15:
            stable_count += 1
        
        angle_history.append(angle_initial)
        # iteration_history.append(iterations)

        # Remove object from scene
        scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
        scene.objects[obj_idx].set_dynamic(False)

        total_count += 1

        success_rate = success_count / total_count
        stable_rate = stable_count / total_count
        angle_rate = sum(angle_history) / total_count
        # iteration_rate = sum(iteration_history) / total_count

        print("Success rate: ", success_rate)
        print("Stability rate: ", stable_rate)
        print("Angle error: ", angle_rate)
        print("Iterations: ", 900)
        print("Trials: ", total_count)
    
    print(angle_rate)



        # # Calculate angle to upright vector
        # gt = Rotation.from_quat(max_quat)
        # gt_matrix = np.array(gt.as_matrix())
        # axis = gt_matrix[2]

        # final = Rotation.from_quat(scene.objects[obj_idx].get_quaternion())
        # final_matrix = np.array(final.as_matrix())
        # final_axis = np.matmul(final_matrix, axis)

        # angle = py_ang([0, 0, 1], final_axis) * 180 / np.pi

        # if (angle < 15):
        #     success_count += 1
        #     # print(angle)
        #     # print('stable')

        # # print("Predicted Quality: %0.3f" % (maxVal / 255))
        
        # scene.objects[obj_idx].set_quaternion(quat)

        # total_count += 1
        # print(success_count / total_count)
        # if total_count == 200:
        #     print("FINISHED: ",success_count / total_count)

        #     return


        # scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
        # scene.objects[obj_idx].set_dynamic(False)

        # plt.savefig("/home/kerry/fig.png")

        # wandb.log({'accurary': success_count / total_count, "Image": wandb.Image(cv2.imread("/home/kerry/fig.png"), caption="Predicted Quality: %0.3f" % (max_q / 255))})



                
