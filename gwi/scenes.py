"""
Experimental setup scene

Author: Doug Morrison
"""

import glob
import os
import os.path as osp
import random
import re

import numpy as np

from pyrep import PyRep
from pyrep.const import PrimitiveShape
from pyrep.objects.shape import Shape
from pyrep.objects.vision_sensor import VisionSensor
from pyrep.objects.object import Object

from .robots import PyRepPanda
from .cameras import PyRepCamera
from .models import PlacementNetwork

from scipy.spatial.transform import Rotation
from pyquaternion import Quaternion
import cv2

from mpl_toolkits import mplot3d

import numpy as np
import matplotlib.pyplot as plt
import math

import itertools


class PyRepGraspSetup:
    """
    Convenience wrapper to hold a robot grasping setup.
    """
    def __init__(self, scene_file, mesh_path, objects=None, headless=False):
        self.pr = PyRep()
        self.pr.launch(scene_file, headless=headless)
        self.timestep = 0.05 #1.0/200.
        self.pr.set_simulation_timestep(self.timestep)
        self.pr.step_ui()

        self.pr.start()
        self.pr.step()

        self.robot = PyRepPanda()
        self.bucketposition = [0, 0.5, 0]
        # self.camera = PyRepCamera('DepthCam')

        self.front_camera = PyRepCamera('FrontCam')
        self.left_camera = PyRepCamera('LeftCam')
        self.right_camera = PyRepCamera('RightCam')
        self.back_camera = PyRepCamera('BackCam')

        self.table = Shape('placementTable')
        self.table_og_position = self.table.get_position()

        self.mesh_path = mesh_path
        self.objects = objects or []
        self.objects_path = []
        self.objects_zero = []

        self._consecutive_failures = 0
        self._consecutive_aborts = 0

        # self.placement_camera = VisionSensor.create(resolution=[300, 300],
        #                      position=[0.5, -0.5, 0.5],
        #                      orientation=[np.deg2rad(-90), 0, np.deg2rad(180)],
        #                      far_clipping_plane=0.3)

        self.reset()

    def step_for_time(self, time):
        for _ in range(int(time/self.timestep)+1):
            self.pr.step()

    def attempt_grasp(self, grasp, remove=True, reset=True):
        success = False
        aborted = True
        try:
            grasp_gen = self.robot.grasp_procedure(grasp, self.timestep)
            for finished in grasp_gen:
                self.pr.step()
                if finished == True:
                    break

            success, o = self.check_grasp_success()
            if remove and success:
                self.remove_object(o)

            aborted = False
        except Exception as e:
            # print(e)
            pass

        if aborted:
            self._consecutive_aborts += 1
            # Check for stuck objects
            s, o = self.check_grasp_success()
            if s:
                if remove:
                    self.remove_object(o)
                else:
                    self.replace_objects([o], force=True)
        else:
            self._consecutive_aborts = 0

        if success:
            self._consecutive_failures = 0
        else:
            self._consecutive_failures += 1

        # Reset
        if reset:
            if False:
                if len(self.objects) == 0 or self._consecutive_failures > 25 or self._consecutive_aborts > 5:
                    self.reset()
                    self._consecutive_failures = 0
                    self._consecutive_aborts = 0
                else:
                    self.robot.reset(self.pr)
            else:
                self.robot.reset(self.pr)
            self.step_for_time(0.5)

            # Make sure everything is back inside.
            self.replace_objects()

        return success, aborted

    def reset(self, objects='Meshes'):
        self.robot.reset(self.pr)
        self.pr.step()
        for o in self.objects:
            o.remove()
        self.objects = []
        self.objects_path = []
        self.objects_zero = []
        n = np.random.randint(10, 15)
        
        if objects == 'Blocks':
            self.add_blocks(n=n)
        elif objects == 'Meshes':
            #self.add_meshes('data/objects/dexnet_adv/', n=n)
            self.add_meshes('data/objects/test_objects/', scaling_factor=0.00075)
        elif objects == 'Frankenshapes':
            self.add_frankenshapes(n)
        else:
            return None

        self.step_for_time(1.5)

    def replace_objects(self, objects=None, force=False):
        if objects is None:
            objects = self.objects
        for s in objects:
            sp = s.get_position()
            p = self.bucketposition
            if force or (abs(sp[0]-p[0]) > 0.2 or abs(sp[1]-p[1]) > 0.2 or abs(sp[2]-p[2]) > 0.2):
                p[2] += random.uniform(0.25, 0.4)
                p[0] += random.uniform(-0.1, 0.1)
                p[1] += random.uniform(-0.1, 0.1)
                s.set_position(p)
                s.set_orientation([random.random(), random.random(), random.random()])
                self.step_for_time(1.5)

    def add_mesh(self, mesh, make_convex=True):
        s = Shape.create_mesh(mesh.vertices.flatten().tolist(), mesh.faces.flatten().tolist())
        if make_convex:
            s = s.get_convex_decomposition(morph=True)
        s.set_dynamic(True)
        s.set_respondable(True)
        s.set_mass(0.25)
        p = self.bucketposition
        p[2] += random.uniform(0.25, 0.4)
        p[0] += random.uniform(-0.1, 0.1)
        p[1] += random.uniform(-0.1, 0.1)
        s.set_position(p)
        s.set_orientation([random.random(), random.random(), random.random()])
        self.objects.append(s)
        return s

    def add_vhacd_meshes(self, meshes):
        if not isinstance(meshes, list):
            return self.add_mesh(meshes, make_convex=False)
        ss = []
        for m in meshes:
            s = Shape.create_mesh(m.vertices.flatten().tolist(), m.faces.flatten().tolist())
            if ss:
                s.set_color(ss[0].get_color())
            ss.append(s)
        s = self.pr.group_objects(ss)
        s.set_dynamic(True)
        s.set_respondable(True)
        s.set_collidable(True)
        s.set_mass(0.25)
        p = self.bucketposition
        p[2] += random.uniform(0.25, 0.4)
        p[0] += random.uniform(-0.1, 0.1)
        p[1] += random.uniform(-0.1, 0.1)
        s.set_position(p)
        s.set_orientation([random.random(), random.random(), random.random()])
        self.objects.append(s)
        return s

    def import_mesh(self, path, scaling_factor=1.0):
        # new_scaling_factor = ((random.random()*2-1) *0.5+1) * scaling_factor
        s = Shape.import_mesh(path, scaling_factor=scaling_factor)
        # s = s.get_convex_decomposition(morph=True)
        s.set_dynamic(True)
        s.set_respondable(True)
        s.set_mass(0.000000000015)
        p = self.bucketposition
        p[2] += random.uniform(0.25, 0.4)
        p[0] += random.uniform(-0.1, 0.1)
        p[1] += random.uniform(-0.1, 0.1)
        s.set_position(p)
        s.set_orientation([random.random(), random.random(), random.random()])
        self.objects.append(s)
        self.objects_path.append(path)

        match = re.match(".*[A-z]*-([0-9]*)-([0-9]*)-([0-9]*)", path)
        x, y, z = match.groups()
        self.objects_zero.append([float(x), float(y), float(z)])

        return s

    def add_meshes(self, path, n=None, scaling_factor=1.0):
        meshes = glob.glob(osp.join(path, '*'))
        if n is None:
            [self.import_mesh(m, scaling_factor=scaling_factor) for m in meshes]
            self.obj_name = [i.split("/")[-1][:-4] for i in meshes]
            print(self.obj_name)
        else:
            if not isinstance(n, int) or n < 1:
                raise ValueError('n must be a positive integer.')
            for _ in range(n):
                self.import_mesh(random.choice(meshes), scaling_factor=scaling_factor)

    def add_frankenshape(self):
        n = np.random.randint(2, 6)
        fss = []
        color = [np.random.random(), np.random.random(), np.random.random()]
        p = self.bucketposition
        p[2] += np.random.uniform(0.1, 0.4)
        while len(fss) < n:
            ps = list(p)
            ps[2] += np.random.uniform(-0.03, 0.03)
            ps[0] += np.random.uniform(-0.03, 0.03)
            ps[1] += np.random.uniform(-0.03, 0.03)
            s = Shape.create(
                np.random.choice(PrimitiveShape),
                [
                    np.random.uniform(0.01, 0.05),
                    np.random.uniform(0.01, 0.05),
                    np.random.uniform(0.01, 0.075)
                ],
                mass=np.random.uniform(0.05, 0.20),
                position=ps,
                orientation=[np.random.uniform(0, np.pi), np.random.uniform(0, np.pi), np.random.uniform(0, np.pi)],
                color=color
            )
            for fs in fss:
                if not s.check_collision(fs):
                    s.remove()
                    break
            else:
                fss.append(s)

        if len(fss) > 1:
            s = self.pr.group_objects(fss)
        else:
            s = fss[0]
        return s

    def add_frankenshapes(self, n):
        for _ in range(n):
            self.objects.append(self.add_frankenshape())

    def add_blocks(self, n):
        for _ in range(n):
            p = self.bucketposition
            p[2] += random.uniform(0.25, 0.4)
            p[0] += random.uniform(-0.1, 0.1)
            p[1] += random.uniform(-0.1, 0.1)
            s = Shape.create(
                PrimitiveShape.CUBOID,
                [
                    np.random.uniform(0.015, 0.050),
                    np.random.uniform(0.015, 0.050),
                    np.random.uniform(0.050, 0.100)
                ],
                mass=np.random.uniform(0.15, 0.50),
                position=p,
                orientation=[np.random.uniform(0, np.pi), np.random.uniform(0, np.pi), np.random.uniform(0, np.pi)],
                color=[np.random.random(), np.random.random(), np.random.random()]
            )
            self.objects.append(s)

    def check_grasp_success(self):
        for o in self.objects:
            if o.get_position()[2] > 0.25:
                return True, o
        return False, None

    def check_placement_success(self, obj_idx):
        if self.objects[obj_idx].check_collision(self.robot.gripper):
            return True
        return False

    def remove_object(self, o):
        self.objects.remove(o)
        o.remove()

    def grasped_object(self):
        # Determine which object is grasped by distance
        tip_position = np.array(self.robot.robot.get_tip().get_position())
        obj_displacement = [np.array(o.get_position()) - tip_position for o in self.objects]
        obj_distance = [np.linalg.norm(obj_disp) for obj_disp in obj_displacement]

        min_idx = obj_distance.index(min(obj_distance))
        return min_idx, self.objects_path[min_idx]

    def get_placement(self, obj_idx, stable_pose, max_checks = 100):
        # Find quaternion representation of 4x4 transformation matrix
        if len(stable_pose) == 4:
            rot_mat = stable_pose[:3]
            rot_mat = np.array([np.array(rot_mat[i][:3]) for i in range(3)])
            rot_mat = Rotation.from_matrix(rot_mat)
            target_quaternion = Quaternion(np.ndarray.tolist(rot_mat.as_quat()))

            # Calculate desired quaternion rotation relative to arm
            tip_quaternion = Quaternion(self.robot.robot.get_tip().get_quaternion())
            obj_quaternion = Quaternion(self.objects[obj_idx].get_quaternion())
            quaternion = list(tip_quaternion * obj_quaternion.inverse * target_quaternion)
        else:
            gripper_rotation = Rotation.from_quat(self.robot.robot.get_tip().get_quaternion())

            # desired_rotation = Rotation.from_matrix(stable_pose)

            new_rotation = stable_pose * gripper_rotation

            new_euler = new_rotation.as_euler('xyz', degrees=True)
            new_euler[2] = gripper_rotation.as_euler('xyz', degrees=True)[2]
            new_rotation = Rotation.from_euler('xyz', new_euler, degrees=True)

            quaternion = list(new_rotation.as_quat())

        POSITION_X = random.random()*2 - 1
        POSITION_Y = random.random()*2 - 1
        POSITION_Z = random.random()
        position = [POSITION_X, POSITION_Y, POSITION_Z]

        checks = 0

        while not self.robot.check_ik_and_collision(position, quaternion=quaternion, ignore_collisions=False):
            if checks < max_checks:
                POSITION_X = random.random()*2 - 1
                POSITION_Y = random.random()*2 - 1
                POSITION_Z = random.random()
                position = [POSITION_X, POSITION_Y, POSITION_Z]
                print('Trying again: ', position)
            else:
                return None, None
            checks += 1

        return position, quaternion

    def attempt_placement(self, centre, quaternion, obj_idx):
        aborted = True

        self.robot.position_history = []

        try:
            grasp_gen = self.robot.placement_procedure(centre, quaternion, self.objects[obj_idx])
            for finished in grasp_gen:
                self.pr.step()
                if finished == True:
                    break

            aborted = False
        except Exception as e:
            print(e)

        if aborted:
            self._consecutive_aborts += 1
        else:
            self._consecutive_aborts = 0

        return aborted


    def show_camera(self):
        # Y_OFFSET = 0.25
        # Z_OFFSET = 0.05
        # position = self.placement_camera.get_position()
        # position[1] += Y_OFFSET
        # position[2] += Z_OFFSET

        position = [0.5, 0, 0.5]

        path = self.robot.robot.get_path(position=position, 
                                   euler=[0, np.deg2rad(180), 0], 
                                   ignore_collisions=True)

        done = False
        while not done:
            done = path.step()
            self.pr.step()

    def capture_image(self):

        img1 = rgb_img
        img2 = depth_img

        mask = cv2.inRange(img2, np.array([0,0,0]), np.array([10,10,10]))
        res = cv2.bitwise_and(img1,img1, mask= mask)

        hsv = cv2.cvtColor(res, cv2.COLOR_BGR2HSV)

        mask = cv2.inRange(hsv, np.array([110,0,100]), np.array([130,20,255]))
        res = cv2.bitwise_and(hsv,hsv, mask= mask)

        res = cv2.cvtColor(res, cv2.COLOR_HSV2BGR)
        res[np.where((res==[0,0,0]).all(axis=2))] = [255,255,255]

        return rgb_img, depth_img

    def get_obj_transform(self, obj_idx):
        obj = self.objects[obj_idx]
        obj_quat = obj.get_quaternion()
        obj_rotation = Rotation.from_quat(obj_quat)
        obj_inv = obj_rotation.inv()

        desired_rotation = Rotation.from_euler('xyz', self.objects_zero[obj_idx], degrees=True)
        transform_rotation = desired_rotation * obj_inv
        return transform_rotation

    def get_closest_trasform(self, obj_idx):
        gt = Rotation.from_euler('xyz', self.objects_zero[obj_idx], degrees=True)
        gt_matrix = np.array(gt.as_matrix())
        gt_axis = gt_matrix[2]

        current = Rotation.from_quat(self.objects[obj_idx].get_quaternion())
        current_matrix = np.array(current.as_matrix())
        current_axis = np.matmul(current_matrix, gt_axis)

        projection = current_axis[:-1]
        axis = [-projection[1], projection[0]]
        angle = -py_ang([0, 0, 1], current_axis)

        quat = axisangle_to_quaternion(axis, angle)
        
        return Rotation.from_quat(quat)

    def open_gripper(self, max_steps=1000):
        steps = 0
        while self.robot.gripper.get_open_amount()[0] < 0.9 and self.robot.gripper.get_open_amount()[1] < 0.9 and steps < max_steps:
            self.robot.gripper.actuate(1.0, 1.0)
            self.pr.step()
            steps += 1
            if steps == max_steps:
                print("OPEN GRIPPER TIMEOUT")
        

    def close_gripper(self, max_steps=100, max_force=50):
        steps = 0
        while not self.robot.gripper.actuate_force(max_force) and steps < max_steps:
            self.pr.step()
            steps += 1
            if steps == max_steps:
                print("CLOSE GRIPPER TIMEOUT")

    def object_purgatory(self, obj_idx):
        self.objects[obj_idx].set_dynamic(False)
        self.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
        self.pr.step()  


    def check_object_dropped(self, obj_idx):
        obj_pos = self.objects[obj_idx].get_position()
        gripper_pos = self.robot.robot.get_tip().get_position()

        return math.sqrt(sum([(a - b) ** 2 for a, b in zip(obj_pos, gripper_pos)])) > 0.1


def depth_to_cv(img, filename):
    img = np.array(img, dtype='float32') # Convert list to array
    img = img*255 # Scale from [0,1] to [0,255]
    img = np.stack((img,)*3, axis=-1)
    
    return img
    

def rgb_to_cv(img, filename):
    img = np.array(img, dtype='float32') # Convert list to array
    img = img*255 # Scale from [0,1] to [0,255]

    return img


def save_depth_rgb(camera, filename):
    depth_img = camera.capture_depth().img
    rgb_img = camera.capture_rgb()

    depth_img = depth_to_cv(depth_img)
    rgb_img = rgb_to_cv(rgb_img)

    cv2.imwrite('/home/kerry/images/'+ filename +'depth.png', depth_img) 
    cv2.imwrite('/home/kerry/images/'+ filename +'color.png', rgb_img) 



def segment_image(depth_img, rgb_image):
    img1 = rgb_to_cv(rgb_img)
    img2 = depth_to_cv(depth_img)

    mask = cv2.inRange(img2, np.array([0,0,0]), np.array([90,90,90]))
    res = cv2.bitwise_and(img1,img1, mask= mask)

    hsv = cv2.cvtColor(res, cv2.COLOR_BGR2HSV)

    mask = cv2.inRange(hsv, np.array([5,5,0]), np.array([255,255,255]))
    res = cv2.bitwise_and(img2,img2, mask= mask)
    res = res / 255

    return res

def py_ang(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'    """
    cosang = np.dot(v1, v2)
    sinang = np.linalg.norm(np.cross(v1, v2))
    return np.arctan2(sinang, cosang)

def axisangle_to_quaternion(axis, angle):
    axis = np.array(axis)
    axis /= np.linalg.norm(axis)

    qx = axis[0] * np.sin(angle / 2)
    qy = axis[1] * np.sin(angle / 2)
    qz = 0
    qw = np.cos(angle / 2)

    return [qx, qy, qz, qw]