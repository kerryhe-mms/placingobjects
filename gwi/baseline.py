import math,random, numpy as np, cv2
import sys
import numpy as np, cv2
from scipy.ndimage.filters import maximum_filter
from scipy.ndimage.morphology import generate_binary_structure, binary_erosion
import matplotlib.pyplot as pp
from skimage.feature import peak_local_max
from scipy import ndimage as ndi
import matplotlib.pyplot as plt
from skimage.feature import peak_local_max
from skimage import data, img_as_float
from matplotlib import pyplot
import cv2
from scipy.spatial.transform import Rotation
from keras.backend.tensorflow_backend import set_session
from keras.backend.tensorflow_backend import clear_session
from keras.backend.tensorflow_backend import get_session
import tensorflow, gc
from .scenes import py_ang
import wandb
import copy
import open3d

def rotation_matrix_from_vectors(vec1, vec2):
    """ Find the rotation matrix that aligns vec1 to vec2
    :param vec1: A 3d "source" vector
    :param vec2: A 3d "destination" vector
    :return mat: A transform matrix (3x3) which when applied to vec1, aligns it with vec2.
    """
    a, b = (vec1 / np.linalg.norm(vec1)).reshape(3), (vec2 / np.linalg.norm(vec2)).reshape(3)
    v = np.cross(a, b)
    c = np.dot(a, b)
    s = np.linalg.norm(v)
    kmat = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    rotation_matrix = np.eye(3) + kmat + kmat.dot(kmat) * ((1 - c) / (s ** 2))
    return rotation_matrix

front_transform = np.array([[-8.93487276e-08,-9.99999881e-01,-1.19252895e-07,1.28871725e-07],
            [-4.88281134e-04,-8.93487276e-08,9.99999762e-01,-4.99621463e-01],
            [-9.99999762e-01,1.19252895e-07,-4.88191727e-04,7.75244066e-01],
            [0,0,0,1]])

left_transform = np.array([[-1.00000000e+00,1.80001045e-14,1.50995812e-07,4.99999925e-01],
            [ 1.50995812e-07,1.19209304e-07,1.00000006e+00,-5.00000075e-01],
            [ 1.42155439e-15,1.00000006e+00,-1.78813956e-07,2.50000104e-01],
            [0,0,0,1]])

right_transform = np.array([[ 1.00000000e+00,0.00000000e+00,0.00000000e+00,-5.00000000e-01],
            [ 0.00000000e+00,-8.34465126e-07,1.00000006e+00,-4.99999821e-01],
            [-0.00000000e+00,-1.00000006e+00,-8.34465126e-07,2.50000283e-01],
            [0,0,0,1]])

rotation1 = np.eye(3,3)
rotation2 = Rotation.from_euler('z', 90, degrees=True).as_matrix()
rotation3 = Rotation.from_euler('z', 90, degrees=True).as_matrix()
rotation4 = Rotation.from_euler('z', 90, degrees=True).as_matrix()
rotation5 = Rotation.from_euler('x', 90, degrees=True).as_matrix()
rotation6 = Rotation.from_euler('x', 180, degrees=True).as_matrix()



rotation_list = [rotation1, rotation3, rotation4, rotation5, rotation6]

rotation_list = list(map(Rotation.from_matrix, rotation_list))

def run_baseline(scene):
    
    joint_pos = scene.robot.og_joints
    joint_pos[1]-=0.2
    scene.robot.robot.set_joint_positions(joint_pos)
    n_obj = len(scene.objects)

    scene.robot.robot.set_joint_positions(scene.robot.og_joints)
    scene.pr.step()

    success_count = 0
    stable_count = 0
    total_count = 0
    angle_history = []
    iteration_history = []



    for obj in scene.objects:
        obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
        obj.set_dynamic(False)

    for _ in range(500):

        

        obj_idx = random.randint(0, n_obj - 1)

        # Add noise to angle
        alpha = random.random()*2*np.pi
        beta = random.random()*np.pi
        gamma = random.random()*2*np.pi
        r = Rotation.from_euler('xyz', [alpha, beta, gamma], degrees=False)
        quat = list(r.as_quat())

        # Add noise to position
        pos = [0.5, 0, 0.5]
        # pos[0] += (random.random()*2-1)*0.025
        # pos[1] += (random.random()*2-1)*0.025
        # pos[2] += (random.random()*2-1)*0.025

        # Place object in random pose in front of camera
        scene.objects[obj_idx].set_quaternion(quat)
        scene.objects[obj_idx].set_position(pos)
        scene.pr.step()
        accumulated_rotation = Rotation.from_matrix(np.eye(3,3))

        pcd = open3d.geometry.PointCloud()

        for rot in rotation_list:

            r = scene.objects[obj_idx].get_quaternion()
            obj_rotation = Rotation.from_quat(r)
            new_rotation = rot * obj_rotation

            final_rotation = new_rotation.as_quat()

            accumulated_rotation = rot * accumulated_rotation

            scene.objects[obj_idx].set_quaternion(list(final_rotation))

            scene.pr.step()
            # input()

            depth_front = scene.front_camera.capture_depth().img
            depth_left = scene.left_camera.capture_depth().img
            depth_right = scene.right_camera.capture_depth().img
          
            depth_left[depth_left >= 0.4] = float('NaN')
            depth_right[depth_right >= 0.4] = float('NaN')
            depth_front[depth_front >= 0.4] = float('NaN')

            i = open3d.camera.PinholeCameraIntrinsic(300, 300, -259.81, -259.81, 150, 150)

            dl = open3d.geometry.Image(depth_left)
            dr = open3d.geometry.Image(depth_right)
            df = open3d.geometry.Image(depth_front)

            inv_rot = np.asarray(accumulated_rotation.as_matrix())
            # print(inv_rot)      
            new_rot = np.zeros((4,4))
            new_rot[:3, :3] = inv_rot
            new_rot[3,3] = 1


            position_vector = copy.deepcopy(pos)
            position_vector.append(1)
            position_vector = np.array(position_vector)

            inv_left = np.linalg.inv(left_transform) 
            m_left = left_transform @ position_vector
            print(m_left)

            left_matrix = np.eye(4,4)
            left_matrix[:, 3] = position_vector.T

            left_matrix = np.linalg.inv(left_matrix @ new_rot @ np.linalg.inv(left_matrix))

            print(left_matrix)

            inv_right = np.linalg.inv(right_transform)
            m_right = right_transform @ position_vector
            
            right_matrix = np.eye(4,4)
            right_matrix[:, 3] = m_right.T

            right_matrix = np.linalg.inv(right_matrix @ new_rot @ np.linalg.inv(right_matrix))

            
            # print(m)

            inv_front = np.linalg.inv(front_transform)
            m_front = front_transform @ position_vector
            # print(m)

            front_matrix = np.eye(4,4)
            front_matrix[:, 3] = m_front.T

            front_matrix = np.linalg.inv(front_matrix @ new_rot @ np.linalg.inv(front_matrix))


            

            pcd_l = open3d.geometry.PointCloud.create_from_depth_image(dl, i, extrinsic=left_transform)

            pcd_r = open3d.geometry.PointCloud.create_from_depth_image(dr, i, extrinsic=right_transform)

            pcd_f = open3d.geometry.PointCloud.create_from_depth_image(df, i, extrinsic=front_transform)

            pcd_l = pcd_l.transform(left_matrix)
            pcd_r = pcd_r.transform(left_matrix)
            pcd_f = pcd_f.transform(left_matrix)

            pcd_new = pcd_f + pcd_l + pcd_r

            # open3d.visualization.draw_geometries([pcd_new])

            pcd += pcd_new

            # open3d.visualization.draw_geometries([pcd])


        # tetra_mesh, pt_map = open3d.geometry.TetraMesh.create_from_point_cloud(pcd)

        # for alpha in np.logspace(np.log10(0.5), np.log10(0.03), num=5):
        #     print(f"alpha={alpha:.3f}")
        #     mesh = open3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(
        #         pcd, alpha, tetra_mesh, pt_map)
        #     mesh.compute_vertex_normals()
        #     open3d.visualization.draw_geometries([mesh], mesh_show_back_face=True)

        #     pcd_final = mesh.sample_points_uniformly(number_of_points=10000)

        # pcd = pcd_final

        center = pcd.get_center()

        plane_model, inliers = pcd.segment_plane(0.0005, 3, 5000)



        [a, b, c, d] = plane_model

        print(f"Plane model: {a:.2f}x + {b:.2f}y + {c:.2f}z + {d:.2f} = 0")

        inlier_cloud = pcd.select_by_index(inliers)
        inlier_cloud.paint_uniform_color([1.0, 0, 0])

        in_center = inlier_cloud.get_center()

        direction = center - in_center

        #select_by_index

        outlier_cloud = pcd.select_by_index(inliers, invert=True)

        outlier_cloud.paint_uniform_color([0.0, 1.0, 0.1])

        inlier_cloud.estimate_normals()

        n = np.asarray(inlier_cloud.normals)
        vector = np.mean(n, axis = 0)
    
        # print("dot" , np.dot(vector, direction))
        # sin_angle

        # print("angle", angle * 180 / np.pi)

        # open3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])


        # print(vector)
        down_vector = None
        if np.dot(vector, direction) > 0:
            down_vector = [0, 0, 1]
        else:
            down_vector = [0, 0, -1]





        #Find Dot Product and Cross Product
        dot = np.dot(vector, down_vector)
        cross = np.cross(vector, down_vector)
        #Get the sine value from cross product
        sin = np.linalg.norm(cross)

        #Construct matrix

        R = rotation_matrix_from_vectors(vector, down_vector)
        # G = np.zeros((3, 3))
        # G[:3,:3] = R
        # G[3,3] = 1

        matrix = Rotation.from_matrix(R) * accumulated_rotation.inv()



        r = scene.objects[obj_idx].get_quaternion()
        obj_rotation = Rotation.from_quat(r)
        new_rotation = matrix * obj_rotation

        final_rotation = new_rotation.as_quat()

        scene.objects[obj_idx].set_quaternion(list(final_rotation))

        scene.pr.step()



        scene.objects[obj_idx].set_dynamic(True)
        collision = True
        z = 0
        while collision:
            scene.objects[obj_idx].set_quaternion(list(new_rotation.as_quat()))
            scene.objects[obj_idx].set_position([0, 0.5, z])
            scene.pr.step()

            distance = np.linalg.norm((np.array(scene.objects[obj_idx].get_position()) - np.array([0, 0.5, z])))
            angle_distance = 1 - np.abs(np.dot(new_rotation.as_quat(), scene.objects[obj_idx].get_quaternion()))

            if angle_distance < 1e-05 and distance < 0.1:
                collision = False

            z += 0.005

        scene.objects[obj_idx].set_dynamic(False)
        scene.pr.step()
        scene.objects[obj_idx].set_dynamic(True)

        # Record movement while object is unstable
        position_history = []
        count = 0
        t = 0
            
        while count < 5 and t < 1000:
            position_history.append(scene.objects[obj_idx].get_position())
            if len(position_history) > 1:
                p1 = np.array(position_history[-1])
                p2 = np.array(position_history[-2])
                disp = np.linalg.norm(p1 - p2)
                if (disp < 0.0005):
                    count += 1
            scene.pr.step()
            t += 1


            # Calculate angle to upright vector
        gt = Rotation.from_euler('xyz', scene.objects_zero[obj_idx], degrees=True)
        gt_matrix = np.array(gt.as_matrix())
        axis = gt_matrix[2]

        final = Rotation.from_quat(scene.objects[obj_idx].get_quaternion())
        final_matrix = np.array(final.as_matrix())
        final_axis = np.matmul(final_matrix, axis)

        angle_final = py_ang([0, 0, 1], final_axis) * 180 / np.pi

        if (angle_final < 15):
            success_count += 1
            print("success")

        initial_matrix = np.array(new_rotation.as_matrix())
        initial_axis = np.matmul(initial_matrix, axis)
        angle_initial = py_ang([0, 0, 1], initial_axis) * 180 / np.pi

        if np.abs(angle_final - angle_initial) < 15:
            stable_count += 1
            print("stable")


        scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
        scene.objects[obj_idx].set_dynamic(False)



    print(stable_count)
    print(success_count)

