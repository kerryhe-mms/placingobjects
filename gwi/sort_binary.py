import cv2, numpy as np, glob, pickle
l = glob.glob("../data/training_data/*")

for index,i in enumerate(l):
    print(i)
    i = open(i, 'rb')
    print(i)
    d = pickle.load(i)
    print(d)
    q = np.stack((d["depth_left"], d["depth_right"], d["depth_front"]), axis=-1)
    if d["placements"][0]["stable"]:
            cv2.imwrite("../data/stable/" + str(index+100000) + ".png",q)
    else:
            cv2.imwrite("../data/unstable/" + str(index+100000) + ".png",q)
