# from .evalaute_binary import reset_keras
import glob,pickle, os, time, threading, queue, random, math, numpy as np, torch, cv2

from termcolor import cprint

import tensorboardX
from torch import nn

from .robots import IKGraspFilter
from .models import PlacementNetwork
from .training_data import TrainingData, to_placement
from .scenes import save_depth_rgb, segment_image, py_ang

from scipy.spatial.transform import Rotation
from pyrep.errors import ConfigurationPathError, ConfigurationError
from pyrep.const import ConfigurationPathAlgorithms

from mpl_toolkits import mplot3d

from matplotlib.colors import LogNorm

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import datetime

import sys
sys.path.append("/home/rhys/Desktop/placingobjects/pix2pix")
from options.test_options import TestOptions
from data import create_dataset
from models import create_model
from util.visualizer import save_images
from util import html

import copy

import itertools

class Supervisor:
    def __init__(self, scene, placementmode="analytical", loop="open", id=None):
        torch.manual_seed(0)

        # Main components
        self.scene = scene

        self.placementmode = placementmode
        self.index = 0
        self.loop = loop
        self.id = id


    def spawn_upright(self):
        for i, obj in enumerate(self.scene.objects):
            obj.set_dynamic(False)
            obj.set_position([i/5 - 2, -1, 0.05])

            obj_quat = obj.get_quaternion()
            obj_rotation = Rotation.from_quat(obj_quat)
            obj_inv = obj_rotation.inv()

            desired_rotation = Rotation.from_euler('xyz', self.scene.objects_zero[i], degrees=True)
            transform_rotation = desired_rotation * obj_inv

            transform_euler = transform_rotation.as_euler('xyz', degrees=True)
            transform_euler[2] = random.random()*360

            new_rotation = Rotation.from_euler('xyz', transform_euler, degrees=True) * obj_rotation

            obj.set_quaternion(list(new_rotation.as_quat()))
            # obj.set_quaternion([0, 0, 0, 1])
            # obj.set_dynamic(True)

        while True:
            # print(Rotation.from_quat(self.scene.objects[0].get_quaternion()).as_euler('xyz', degrees=True))
            self.scene.pr.step()


    def run(self):
        self.spawn_upright()
        self.placement()

    def placement(self):

        for _ in range(1):

            if self.placementmode == "analytical":
                # Find the data by sampling with noise and finding the rotation to move to the ground truth
                print("Analytical data collection")

                orig_position = [0.5, 0, 0.5]
                joint_pos = self.scene.robot.og_joints
                joint_pos[1]-=0.2
                self.scene.robot.robot.set_joint_positions(joint_pos)
                n_obj = len(self.scene.objects)

                for obj in self.scene.objects:
                    obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                    obj.set_dynamic(False)
                count = 0

                for ss in range(50):
                    print("Collecting set: ", ss)
                    for alpha in np.linspace(0, 2*np.pi, 16):
                        for beta in np.linspace(0, np.pi, 8):
                            for gamma in np.linspace(0, 2*np.pi, 16):
                                obj_idx = random.randint(0, n_obj - 1)

                                # Add noise to angle
                                alpha += (random.random()*2 - 1)*2*np.pi/15/2
                                beta += (random.random()*2 - 1)*np.pi/7/2
                                gamma += (random.random()*2 - 1)*2*np.pi/15/2

                                #Add noise to position
                                pos = copy.deepcopy(orig_position)
                                pos[0] += (random.random()*2-1)*0.05
                                pos[1] += (random.random()*2-1)*0.05
                                pos[2] += (random.random()*2-1)*0.05

                                # Set new angle and position
                                r = Rotation.from_euler('xyz', [alpha, beta, gamma], degrees=False)
                                quat = list(r.as_quat())
                                self.scene.objects[obj_idx].set_quaternion(quat)
                                self.scene.objects[obj_idx].set_position(pos)


                                self.scene.pr.step()

                                depth_front = self.scene.front_camera.capture_depth().img
                                depth_left = self.scene.left_camera.capture_depth().img
                                depth_right = self.scene.right_camera.capture_depth().img
                                depth_back = self.scene.back_camera.capture_depth().img
                                cv2.imwrite("front.png", depth_front)
                                cv2.imwrite("left.png", depth_front)
                                cv2.imwrite("right.png", depth_front)
                                input("plz kill me")
                                self.save_upright_td(depth_left, depth_right, depth_front, depth_back, obj_idx)

                                # Set object aside, pick random object
                                self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])

            elif self.placementmode == 'evaluate':
                model = PlacementNetwork()
                model.import_model('model.pt')

                self.evaluate_placement(250, model, max_closed_loop=1)
                self.evaluate_placement(250, model, max_closed_loop=15)

                # if self.loop == "closed":
                #     self.evaluate_gripper_placement(5, model, max_closed_loop=5, teleport=False)
                # else:
                #     self.evaluate_gripper_placement(5, model, max_closed_loop=1, teleport=False)
           
            elif self.placementmode == 'isstable':
                print("Sample object stability")
                #This samples placing objects and saves the position history

                joint_pos = self.scene.robot.og_joints
                joint_pos[1]-=0.2
                self.scene.robot.robot.set_joint_positions(joint_pos)
                n_obj = len(self.scene.objects)

                for obj in self.scene.objects:
                    obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                    obj.set_dynamic(False)

                saved_unstable = 884
                saved_stable = 952

                SAMPLE = 999

                
                obj_idx = 0
                tic = datetime.datetime.now()
                while obj_idx <= 0:
                    for alpha in np.linspace(0, 2*np.pi, 8):
                        for beta in np.linspace(0, np.pi, 4):
                            for gamma in np.linspace(0, 2*np.pi, 8):

                                # Sample random object
                                # obj_idx = random.randint(0, n_obj - 1)
                                self.scene.objects[obj_idx].set_dynamic(True)
                                r = None
                                # quat = None
                                if random.randint(0, 2) == 0 or saved_unstable >= SAMPLE:
                                    obj = self.scene.objects[obj_idx]
                                    obj_quat = obj.get_quaternion()
                                    obj_rotation = Rotation.from_quat(obj_quat)
                                    obj_inv = obj_rotation.inv()

                                    desired_rotation = Rotation.from_euler('xyz', self.scene.objects_zero[obj_idx], degrees=True)
                                    transform_rotation = desired_rotation * obj_inv

                                    transform_euler = transform_rotation.as_euler('xyz', degrees=True)
                                    transform_euler[2] = random.random()*360

                                    transform_euler[0] += (random.random() * 2 - 1) * 0.1
                                    transform_euler[1] += (random.random() * 2 - 1) * 0.1
                                  

                                    new_rotation = Rotation.from_euler('xyz', transform_euler, degrees=True) * obj_rotation
                                    r = new_rotation
                                    # quat = list(r.as_quat())
                                    # obj.set_quaternion(list(new_rotation.as_quat()))

                                else:
                                    # Add noise to angle
                                    alpha += (random.random()*2 - 1)*2*np.pi/7/2
                                    beta += (random.random()*2 - 1)*np.pi/3/2
                                    gamma += (random.random()*2 - 1)*2*np.pi/7/2


                                    # Set new angle and position
                                    r = Rotation.from_euler('xyz', [alpha, beta, gamma], degrees=False)
                                quat = list(r.as_quat())

                                #Add noise to position
                                pos = [0.5, 0, 0.5]
                                pos[0] += (random.random()*2-1)*0.1
                                pos[1] += (random.random()*2-1)*0.1
                                pos[2] += (random.random()*2-1)*0.1

                                self.scene.objects[obj_idx].set_quaternion(quat)
                                self.scene.objects[obj_idx].set_position(pos)

                                self.scene.pr.step()
                                depth_front = self.scene.front_camera.capture_depth().img
                                depth_left = self.scene.left_camera.capture_depth().img
                                depth_right = self.scene.right_camera.capture_depth().img
                                depth_back = self.scene.back_camera.capture_depth().img


                                td = TrainingData(depth_left, depth_front, depth_right, depth_back)

                                # Place object at lowest possible point
                                collision = True
                                z = 0
                                while collision:
                                    self.scene.objects[obj_idx].set_quaternion(list(r.as_quat()))
                                    self.scene.objects[obj_idx].set_position([0, 0.5, z])
                                    self.scene.pr.step()

                                    distance = np.linalg.norm((np.array(self.scene.objects[obj_idx].get_position()) - np.array([0, 0.5, z])))
                                    angle_distance = 1 - np.abs(np.dot(r.as_quat(), self.scene.objects[obj_idx].get_quaternion()))

                                    if angle_distance < 1e-05 and distance < 0.1:
                                        collision = False

                                    z += 0.025

                                # Record movement while object is unstable
                                position_history = []
                                angle_history = []
                                count = 0
                                
                                while count < 5:
                                    position_history.append(self.scene.objects[obj_idx].get_position())
                                    angle_history.append(self.scene.objects[obj_idx].get_quaternion())
                                    if len(position_history) > 1:
                                        p1 = np.array(position_history[-1])
                                        p2 = np.array(position_history[-2])
                                        disp = np.linalg.norm(p1 - p2)
                                        if (disp < 0.001):
                                            count += 1
                                    self.scene.pr.step()


                                gt = Rotation.from_euler('xyz', self.scene.objects_zero[obj_idx], degrees=True)
                                gt_matrix = np.array(gt.as_matrix())
                                axis = gt_matrix[2]

                                # final = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
                                final_matrix = np.array(r.as_matrix())
                                final_axis = np.matmul(final_matrix, axis)

                                angle_from_start = py_ang([0, 0, 1], final_axis) * 180 / np.pi

                                # Calculate angle to upright vector
                                gt = Rotation.from_euler('xyz', self.scene.objects_zero[obj_idx], degrees=True)
                                gt_matrix = np.array(gt.as_matrix())
                                axis = gt_matrix[2]

                                final = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
                                final_matrix = np.array(final.as_matrix())
                                final_axis = np.matmul(final_matrix, axis)

                                angle_from_final = py_ang([0, 0, 1], final_axis) * 180 / np.pi
                                #print("angle_from_final: ", angle_from_final)
                                #print("angle_from_start: ", angle_from_start)

                                if (angle_from_final < 15 and angle_from_start  < 15):
                                    saved_stable += 1

                                    td.dir_path = "./data/stable"
                                    if saved_stable < SAMPLE:
                                        td.write_file(obj_idx)

                                    print("Stable: ", saved_stable, " / ", SAMPLE)

                                else:
                                    saved_unstable += 1
                                    td.dir_path = "./data/unstable"
                                    if saved_unstable < SAMPLE:
                                        td.write_file(obj_idx)

                                    print("Unstable: ", saved_unstable, " / ", SAMPLE)
                                
                                if saved_stable >= SAMPLE and saved_unstable >= SAMPLE:
                                    self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                                    self.scene.objects[obj_idx].set_dynamic(False)

                                    toc = datetime.datetime.now()
                                    print("Finished object: ", obj_idx, "Time elapsed: ", toc - tic)
                                    tic = datetime.datetime.now()

                                    saved_stable = 0
                                    saved_unstable = 0
                                    obj_idx += 1

                                    if obj_idx > 30: # TEMP FIX SINCE OBJ 0-30 ALREADY HAVE 200 SAMPLED
                                        SAMPLE = 800



                                # Set object aside, pick random object
                                self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                                self.scene.objects[obj_idx].set_dynamic(False)

                self.scene.reset()

            if self.placementmode == "gripper":
                for obj in self.scene.objects:
                    obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                    obj.set_dynamic(False)

                n_obj = len(self.scene.objects)
                TOTAL = 10

                for obj_idx in range(50):
                    # Sample random object
                    success_count = 0

                    # try:
                    #     # Sample random object
                    #     stable_count = 0
                    #     unstable_count = 0

                    #     #Find numnber of stable
                    #     f = open("data/stable_gf.csv")
                    #     for line in f:
                    #         if line.split(",")[0].strip() == self.scene.obj_name[obj_idx]:
                    #             stable_count = int(line.split(",")[1].strip())

                    #     f = open("data/unstable_gf.csv")
                    #     for line in f:
                    #         if line.split(",")[0].strip() == self.scene.obj_name[obj_idx]:
                    #             unstable_count = int(line.split(",")[1].strip())

                    #     success_count = stable_count + unstable_count
                    # except:
                    #     print("Fresh start")

                    while success_count < TOTAL:
                        # obj_idx = random.randint(0, n_obj - 1)

                        # Sample random object angle
                        alpha = random.random()*2*np.pi 
                        beta = random.random()*2*np.pi 
                        gamma = random.random()*2*np.pi 

                        # Add noise to position
                        pos = [0.5, 0, 0.5]
                        pos[0] += (random.random()*2-1)*0.01
                        pos[1] += (random.random()*2-1)*0.01
                        pos[2] += (random.random()*2-1)*0.01

                        # Set new angle and position
                        r = Rotation.from_euler('xyz', [alpha, beta, gamma], degrees=False)
                        quat = list(r.as_quat())

                        # Sample random gripper angle
                        _alpha_ = random.random()*2*np.pi 
                        _beta_ = random.random()*np.pi + np.pi 
                        _gamma_ = random.random()*2*np.pi 

                        gripper_matrix = Rotation.from_euler('xyz', [_alpha_, _beta_, _gamma_], degrees=False).as_matrix()
                        gripper_axis = np.matmul(gripper_matrix, np.array([0, 0, 1]))
                        gripper_2D = gripper_axis[:-1]
                        gripper_theta = np.arctan2(gripper_2D[1], gripper_2D[0])
                        transform_rotation = Rotation.from_euler('xyz', [0, 0, -gripper_theta], degrees=False)
                        gripper_rotation = Rotation.from_euler('xyz', [_alpha_, _beta_, _gamma_], degrees=False)
                        new_rotation = transform_rotation * gripper_rotation

                        while self.scene.robot.gripper.get_open_amount()[0] < 0.9 and self.scene.robot.gripper.get_open_amount()[1] < 0.9:
                            self.scene.robot.gripper.actuate(1.0, 1.0)
                            self.scene.pr.step()

                        for obj in self.scene.objects:
                            obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                            obj.set_dynamic(False)

                        try: 
                            # Get joint configuration
                            joint = self.scene.robot.robot.get_configs_for_tip_pose([0.5, 0, 0.5], 
                                                                                    quaternion=list(new_rotation.as_quat()), 
                                                                                    ignore_collisions=True,
                                                                                    trials=1, 
                                                                                    max_configs=1)[0]

                            self.scene.robot.robot.set_joint_positions(joint)

                            # Bring object to scene
                            self.scene.objects[obj_idx].set_quaternion(quat)
                            self.scene.objects[obj_idx].set_position(pos)

                            # Ensure object does not collide with gripper
                            if self.scene.objects[obj_idx].check_collision():
                                self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                                continue

                            self.scene.pr.step()
                            
                            # Close gripper fingers
                            steps = 0
                            while not self.scene.robot.gripper.actuate_force(50) and steps < 50:
                                self.scene.pr.step()
                                steps += 1

                            # Take picture
                            depth_front = self.scene.front_camera.capture_depth().img
                            depth_left = self.scene.left_camera.capture_depth().img
                            depth_right = self.scene.right_camera.capture_depth().img


                            rgb_left = self.scene.left_camera.capture_rgb()
                            rgb_right = self.scene.right_camera.capture_rgb()
                            rgb_front = self.scene.front_camera.capture_rgb() 

                            td = TrainingData(depth_left, depth_front, depth_right, rgb_left, rgb_front, rgb_right)

                            transform_rotation = self.scene.get_closest_trasform(obj_idx)
                            quaternion = transform_rotation.as_quat()
                            joint_pos = copy.deepcopy(self.scene.robot.og_joints)
                            joint_pos[1]-=0.2
                            self.scene.robot.robot.set_joint_positions(joint_pos)#self.scene.robot.og_joints)
                            
                            self.scene.pr.step()
                            self.scene.pr.step()
                            self.scene.pr.step()
                            self.scene.pr.step()
                            self.scene.pr.step()

                            depth_front = self.scene.front_camera.capture_depth().img
                            depth_left = self.scene.left_camera.capture_depth().img
                            depth_right = self.scene.right_camera.capture_depth().img

                            td.add(depth_front, depth_left, depth_right)


                            # Place object at lowest possible point ---------------------------------------------------------------------------
                            collision = True
                            self.scene.objects[obj_idx].set_dynamic(True)
                            z = 0
                            while collision:
                                self.scene.objects[obj_idx].set_quaternion(list(r.as_quat()))
                                self.scene.objects[obj_idx].set_position([0, 0.5, z])
                                self.scene.pr.step()

                                distance = np.linalg.norm((np.array(self.scene.objects[obj_idx].get_position()) - np.array([0, 0.5, z])))
                                angle_distance = 1 - np.abs(np.dot(r.as_quat(), self.scene.objects[obj_idx].get_quaternion()))

                                if angle_distance < 1e-05 and distance < 0.1:
                                    collision = False

                                z += 0.025

                            # Record movement while object is unstable
                            position_history = []
                            angle_history = []
                            count = 0
                            
                            while count < 5:
                                position_history.append(self.scene.objects[obj_idx].get_position())
                                angle_history.append(self.scene.objects[obj_idx].get_quaternion())
                                if len(position_history) > 1:
                                    p1 = np.array(position_history[-1])
                                    p2 = np.array(position_history[-2])
                                    disp = np.linalg.norm(p1 - p2)
                                    if (disp < 0.001):
                                        count += 1
                                self.scene.pr.step()


                            gt = Rotation.from_euler('xyz', self.scene.objects_zero[obj_idx], degrees=True)
                            gt_matrix = np.array(gt.as_matrix())
                            axis = gt_matrix[2]

                            # final = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
                            final_matrix = np.array(r.as_matrix())
                            final_axis = np.matmul(final_matrix, axis)

                            angle_from_start = py_ang([0, 0, 1], final_axis) * 180 / np.pi

                            # Calculate angle to upright vector
                            gt = Rotation.from_euler('xyz', self.scene.objects_zero[obj_idx], degrees=True)
                            gt_matrix = np.array(gt.as_matrix())
                            axis = gt_matrix[2]

                            final = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
                            final_matrix = np.array(final.as_matrix())
                            final_axis = np.matmul(final_matrix, axis)

                            angle_from_final = py_ang([0, 0, 1], final_axis) * 180 / np.pi

                            # Write to file
                            stable = False
                            if (angle_from_final < 15 and angle_from_start  < 15):
                                stable = True

                            placement = to_placement(quaternion=quaternion, stable=stable)

                            td.add_placement(placement)
                            success_count = td.write_file(self.scene.obj_name[obj_idx], success_count=success_count)
                            # success_count += 1

                            # Send object away and open gripper ------------------------------------------------------------------
                            self.scene.objects[obj_idx].set_dynamic(False)
                            self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                            self.scene.pr.step()  

                            while self.scene.robot.gripper.get_open_amount()[0] < 0.9 and self.scene.robot.gripper.get_open_amount()[1] < 0.9:
                                self.scene.robot.gripper.actuate(1.0, 1.0)
                                self.scene.pr.step()

                            print('Object: ', obj_idx, "\tSuccess: ", success_count, " / ", TOTAL)

                        except Exception as e:
                            print(e)
                            # pass

                        # Set object aside, pick random object
                        self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])

            if self.placementmode == "gripper_isstable":
                for obj in self.scene.objects:
                    obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                    obj.set_dynamic(False)

                n_obj = len(self.scene.objects)


                for obj_idx in range(50):
                    # Sample random object
                    stable_count = 0
                    unstable_count = 0

                    #Find numnber of stable
                    f = open("data/stable.csv")
                    for line in f:
                        if line.split(",")[0].strip() == self.scene.obj_name[obj_idx]:
                            stable_count = int(line.split(",")[1].strip())

                    f = open("data/unstable.csv")
                    for line in f:
                        if line.split(",")[0].strip() == self.scene.obj_name[obj_idx]:
                            unstable_count = int(line.split(",")[1].strip())

                    while stable_count <= 1000 or unstable_count <= 1000:
                        # obj_idx = random.randint(0, n_obj - 1)

                        if random.randint(0, 2) == 0 or unstable_count >= 1000:
                            obj = self.scene.objects[obj_idx]
                            obj_quat = obj.get_quaternion()
                            obj_rotation = Rotation.from_quat(obj_quat)
                            obj_inv = obj_rotation.inv()

                            desired_rotation = Rotation.from_euler('xyz', self.scene.objects_zero[obj_idx], degrees=True)
                            transform_rotation = desired_rotation * obj_inv

                            transform_euler = transform_rotation.as_euler('xyz', degrees=True)
                            transform_euler[2] = random.random()*360

                            transform_euler[0] += (random.random() * 2 - 1) * 0.1
                            transform_euler[1] += (random.random() * 2 - 1) * 0.1
                            

                            new_rotation = Rotation.from_euler('xyz', transform_euler, degrees=True) * obj_rotation
                            r = new_rotation
                            # quat = list(r.as_quat())
                            # obj.set_quaternion(list(new_rotation.as_quat()))

                        else:
                            # Add noise to angle
                            alpha = (random.random()*2 - 1)*2*np.pi
                            beta = (random.random()*2 - 1)*np.pi
                            gamma = (random.random()*2 - 1)*2*np.pi


                            # Set new angle and position
                            r = Rotation.from_euler('xyz', [alpha, beta, gamma], degrees=False)

                        # Add noise to position
                        pos = [0.5, 0, 0.5]
                        pos[0] += (random.random()*2-1)*0.01
                        pos[1] += (random.random()*2-1)*0.01
                        pos[2] += (random.random()*2-1)*0.01

                        # Set new angle and position
                        # r = Rotation.from_euler('xyz', [alpha, beta, gamma], degrees=False)
                        quat = list(r.as_quat())

                        # Sample random gripper angle
                        _alpha_ = random.random()*2*np.pi 
                        _beta_ = random.random()*np.pi + np.pi 
                        _gamma_ = random.random()*2*np.pi 

                        while self.scene.robot.gripper.get_open_amount()[0] < 0.9 and self.scene.robot.gripper.get_open_amount()[1] < 0.9:
                            self.scene.robot.gripper.actuate(1.0, 1.0)
                            self.scene.pr.step()

                        for obj in self.scene.objects:
                            obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                            obj.set_dynamic(False)

                        try: 
                            # Get joint configuration
                            joint = self.scene.robot.robot.get_configs_for_tip_pose([0.5, 0, 0.5], 
                                                                                    euler=[_alpha_, _beta_, _gamma_], 
                                                                                    ignore_collisions=True,
                                                                                    trials=1, 
                                                                                    max_configs=1)[0]

                            self.scene.robot.robot.set_joint_positions(joint)

                            # Bring object to scene
                            self.scene.objects[obj_idx].set_quaternion(quat)
                            self.scene.objects[obj_idx].set_position(pos)

                            # Ensure object does not collide with gripper
                            if self.scene.objects[obj_idx].check_collision():
                                self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                                continue

                            self.scene.pr.step()
                            
                            # Close gripper fingers
                            steps = 0
                            while not self.scene.robot.gripper.actuate_force(50) and steps < 50:
                                self.scene.pr.step()
                                steps += 1

                            # Take picture
                            depth_front = self.scene.front_camera.capture_depth().img
                            depth_left = self.scene.left_camera.capture_depth().img
                            depth_right = self.scene.right_camera.capture_depth().img


                            rgb_left = self.scene.left_camera.capture_rgb()
                            rgb_right = self.scene.right_camera.capture_rgb()
                            rgb_front = self.scene.front_camera.capture_rgb() 

                            td = TrainingData(depth_left, depth_front, depth_right, rgb_left, rgb_front, rgb_right)
                            td.dir_path =  os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + '/data/training_data_gripper_stable'

                            transform_rotation = self.scene.get_closest_trasform(obj_idx)
                            quaternion = transform_rotation.as_quat()


                            # Place object at lowest possible point ---------------------------------------------------------------------------
                            collision = True
                            self.scene.objects[obj_idx].set_dynamic(True)
                            z = 0
                            while collision:
                                self.scene.objects[obj_idx].set_quaternion(list(r.as_quat()))
                                self.scene.objects[obj_idx].set_position([0, 0.5, z])
                                self.scene.pr.step()

                                distance = np.linalg.norm((np.array(self.scene.objects[obj_idx].get_position()) - np.array([0, 0.5, z])))
                                angle_distance = 1 - np.abs(np.dot(r.as_quat(), self.scene.objects[obj_idx].get_quaternion()))

                                if angle_distance < 1e-05 and distance < 0.1:
                                    collision = False

                                z += 0.025

                            # Record movement while object is unstable
                            position_history = []
                            angle_history = []
                            count = 0
                            
                            while count < 5:
                                position_history.append(self.scene.objects[obj_idx].get_position())
                                angle_history.append(self.scene.objects[obj_idx].get_quaternion())
                                if len(position_history) > 1:
                                    p1 = np.array(position_history[-1])
                                    p2 = np.array(position_history[-2])
                                    disp = np.linalg.norm(p1 - p2)
                                    if (disp < 0.001):
                                        count += 1
                                self.scene.pr.step()


                            gt = Rotation.from_euler('xyz', self.scene.objects_zero[obj_idx], degrees=True)
                            gt_matrix = np.array(gt.as_matrix())
                            axis = gt_matrix[2]

                            # final = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
                            final_matrix = np.array(r.as_matrix())
                            final_axis = np.matmul(final_matrix, axis)

                            angle_from_start = py_ang([0, 0, 1], final_axis) * 180 / np.pi

                            # Calculate angle to upright vector
                            gt = Rotation.from_euler('xyz', self.scene.objects_zero[obj_idx], degrees=True)
                            gt_matrix = np.array(gt.as_matrix())
                            axis = gt_matrix[2]

                            final = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
                            final_matrix = np.array(final.as_matrix())
                            final_axis = np.matmul(final_matrix, axis)

                            angle_from_final = py_ang([0, 0, 1], final_axis) * 180 / np.pi

                            # Write to file
                            stable = False
                            if (angle_from_final < 15 and angle_from_start  < 15):
                                stable = True
                                stable_count += 1
                            else:
                                unstable_count += 1

                            placement = to_placement(quaternion=quaternion, stable=stable)

                            td.add_placement(placement)
                            if stable_count <= 1000 and stable:
                                td.write_file(self.scene.obj_name[obj_idx], success_count=5)
                            elif unstable_count <= 1000 and not stable:
                                td.write_file(self.scene.obj_name[obj_idx], success_count=5)

                            # success_count += 1

                            # Send object away and open gripper ------------------------------------------------------------------
                            self.scene.objects[obj_idx].set_dynamic(False)
                            self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                            self.scene.pr.step()  

                            while self.scene.robot.gripper.get_open_amount()[0] < 0.9 and self.scene.robot.gripper.get_open_amount()[1] < 0.9:
                                self.scene.robot.gripper.actuate(1.0, 1.0)
                                self.scene.pr.step()

                            print('Object: ', obj_idx, "\tstable_count: ", stable_count, " / ", 1000, "\tunstable_count: ", unstable_count, " / ", 1000)

                        except Exception as e:
                            print(e)
                            # pass

                        # Set object aside, pick random object
                        self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
            
            
            if self.placementmode == "gripper_segmentation":
                for obj in self.scene.objects:
                    obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                    obj.set_dynamic(False)

                n_obj = len(self.scene.objects)


                success_count = 0

                while success_count < 2000:
                    for obj_idx in range(50):
                    # Sample random object
                    
                        # obj_idx = random.randint(0, n_obj - 1)

                        # Sample random object angle
                        alpha = random.random()*2*np.pi 
                        beta = random.random()*2*np.pi 
                        gamma = random.random()*2*np.pi 

                        # Add noise to position
                        pos = [0.5, 0, 0.5]
                        pos[0] += (random.random()*2-1)*0.01
                        pos[1] += (random.random()*2-1)*0.01
                        pos[2] += (random.random()*2-1)*0.01

                        # Set new angle and position
                        r = Rotation.from_euler('xyz', [alpha, beta, gamma], degrees=False)
                        quat = list(r.as_quat())

                        # Sample random gripper angle
                        _alpha_ = random.random()*2*np.pi 
                        _beta_ = random.random()*np.pi + np.pi 
                        _gamma_ = random.random()*2*np.pi 

                        while self.scene.robot.gripper.get_open_amount()[0] < 0.9 and self.scene.robot.gripper.get_open_amount()[1] < 0.9:
                            self.scene.robot.gripper.actuate(1.0, 1.0)
                            self.scene.pr.step()

                        for obj in self.scene.objects:
                            obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                            obj.set_dynamic(False)

                        try: 
                            # Get joint configuration
                            joint = self.scene.robot.robot.get_configs_for_tip_pose([0.5, 0, 0.5], 
                                                                                    euler=[_alpha_, _beta_, _gamma_], 
                                                                                    ignore_collisions=True,
                                                                                    trials=1, 
                                                                                    max_configs=1)[0]

                            self.scene.robot.robot.set_joint_positions(joint)

                            # Bring object to scene
                            self.scene.objects[obj_idx].set_quaternion(quat)
                            self.scene.objects[obj_idx].set_position(pos)

                            # Ensure object does not collide with gripper
                            if self.scene.objects[obj_idx].check_collision():
                                self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                                continue

                            self.scene.pr.step()
                            
                            # Close gripper fingers
                            steps = 0
                            while not self.scene.robot.gripper.actuate_force(50) and steps < 50:
                                self.scene.pr.step()
                                steps += 1

                            # Take picture
                            depth_front = self.scene.front_camera.capture_depth().img
                            depth_left = self.scene.left_camera.capture_depth().img
                            depth_right = self.scene.right_camera.capture_depth().img


                            rgb_left = self.scene.left_camera.capture_rgb()
                            rgb_right = self.scene.right_camera.capture_rgb()
                            rgb_front = self.scene.front_camera.capture_rgb() 

                            td = TrainingData(depth_left, depth_front, depth_right, rgb_left, rgb_front, rgb_right)

                            # transform_rotation = self.scene.get_closest/_trasform(obj_idx)
                            # quaternion = transform_rotation.as_quat()

                             # Get joint configuration
                            # joint = self.scene.robot.robot.get_configs_for_tip_pose([0.5, 0, 0.5], 
                            #                                                         euler=[_alpha_, _beta_, _gamma_], 
                            #                                                         ignore_collisions=True,
                            #                                                         trials=1, 
                            #                                                         max_configs=1)[0]

                            # self.scene.robot.robot.set_joint_positions(joint)
 
                            self.scene.robot.robot.set_joint_positions(self.scene.robot.og_joints)
                            self.scene.pr.step()

                            depth_front = self.scene.front_camera.capture_depth().img
                            depth_left = self.scene.left_camera.capture_depth().img
                            depth_right = self.scene.right_camera.capture_depth().img

                            td.add(depth_front, depth_left, depth_right)
                            td.write_file(self.scene.obj_name[obj_idx], success_count=5)

                        except:
                            pass

                        # Set object aside, pick random object
                        self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                        self.scene.objects[obj_idx].set_dynamic(False)

    def evaluate_placement(self, trials, model, max_closed_loop=1):
        self.scene.reset()
        # quality_model = reset_keras()

        joint_pos = self.scene.robot.og_joints
        joint_pos[1]-=0.2
        self.scene.robot.robot.set_joint_positions(joint_pos)
        n_obj = len(self.scene.objects)

        self.scene.robot.robot.set_joint_positions(self.scene.robot.og_joints)
        self.scene.pr.step()

        for obj in self.scene.objects:
            obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
            obj.set_dynamic(False)

        success_count = 0
        stable_count = 0
        angle_history = []
        iteration_history = []
        self.scene.table.set_position([-5, -5, -5])
        
        stability_network = PlacementNetwork(stable_network=True)
        stability_network.import_model("/home/rhys/Desktop/placingobjects/data/models/2020-10-01-18:58_set4/1.pt")

        num_trails = [0] * n_obj
        success_per_object = [0] * n_obj

        for enum in range(trials):

            print("Evaluation: ", enum, "/", trials, "\tSuccess rate: ", success_count / max(1, enum))

            # Pick random object
            obj_idx = random.randint(0, n_obj - 1)

            # print(obj_idx)

            for _ in range(5):

                # Add noise to angle
                alpha = random.random()*2*np.pi
                beta = random.random()*np.pi
                gamma = random.random()*2*np.pi
                r = Rotation.from_euler('xyz', [alpha, beta, gamma], degrees=False)
                quat = list(r.as_quat())


                # Add noise to position
                pos = [0.5, 0, 0.5]
                pos[0] += (random.random()*2-1)*0.05
                pos[1] += (random.random()*2-1)*0.05
                pos[2] += (random.random()*2-1)*0.05


                # Place object in random pose in front of camera
                self.scene.objects[obj_idx].set_quaternion(quat)
                self.scene.objects[obj_idx].set_position(pos)
                self.scene.pr.step()

                iterations = 0
                highest_quality = 0
                new_rotation = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
                use_best = True

                for _ in range(max_closed_loop/5):

                    # Capture image
                    depth_front = self.scene.front_camera.capture_depth().img
                    depth_left = self.scene.left_camera.capture_depth().img
                    depth_right = self.scene.right_camera.capture_depth().img
                    # depth_back = self.scene.back_camera.capture_depth().img

                    size = (64,64)
                    # Prepare image for network
                    depth_left = cv2.resize(depth_left, dsize=size)
                    depth_front = cv2.resize(depth_front, dsize=size)
                    depth_right = cv2.resize(depth_right, dsize=size)
                    # depth_back = cv2.resize(depth_back, dsize=size)

                    # x = np.stack((depth_left, depth_front, depth_right), axis=0)
                    # x = x*2 - 0.5
                    # x1 = np.expand_dims(x, axis=0)
                    x1 = np.expand_dims(depth_left, axis=(0, 1)) * 2 - 0.5
                    x2 = np.expand_dims(depth_front, axis=(0, 1)) * 2 - 0.5
                    x3 = np.expand_dims(depth_right, axis=(0, 1)) * 2 - 0.5
                    # x4 = np.expand_dims(depth_back, axis=(0, 1)) * 2 - 0.5

                    # Run image through rotation network
                    placement = model.network(torch.from_numpy(x1).to('cuda'), torch.from_numpy(x2).to('cuda'), torch.from_numpy(x3).to('cuda')).squeeze().cpu().detach().numpy()
                    transform_rotation = Rotation.from_matrix(placement)

                    equality = 1 - stability_network.network(torch.from_numpy(x1).to('cuda'), torch.from_numpy(x2).to('cuda'), torch.from_numpy(x3).to('cuda')).squeeze().cpu().detach().numpy()
                    print(equality)




                    # size = (224,224)
                    # # Prepare image for network
                    # depth_left = cv2.resize(depth_left, dsize=size)
                    # depth_front = cv2.resize(depth_front, dsize=size)
                    # depth_right = cv2.resize(depth_right, dsize=size)
                    # x = np.stack((depth_left, depth_right, depth_front), axis=-1)
                    # x = x*2 - 0.5
                    # x = np.expand_dims(x, axis=0)

                    


                    # Find solved rotation of object
                    obj_rotation = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
                    new_rotation = transform_rotation * obj_rotation

                    rot_magnitude = 1 - np.abs(np.dot([0, 0, 0, 1], transform_rotation.as_quat()))
                    # if rot_magnitude < 0.01:
                    #     break

                    # Set new rotation of object
                    self.scene.objects[obj_idx].set_quaternion(list(new_rotation.as_quat()))
                    self.scene.pr.step()

                    # Capture image -----------------------------------------------------------------------------------------------
                    depth_front = self.scene.front_camera.capture_depth().img
                    depth_left = self.scene.left_camera.capture_depth().img
                    depth_right = self.scene.right_camera.capture_depth().img

                    depth_left = cv2.resize(depth_left, dsize=size)
                    depth_front = cv2.resize(depth_front, dsize=size)
                    depth_right = cv2.resize(depth_right, dsize=size)
                    # depth_back = cv2.resize(depth_back, dsize=size)

                    # x = np.stack((depth_left, depth_front, depth_right), axis=0)
                    # x = x*2 - 0.5
                    # x1 = np.expand_dims(x, axis=0)
                    x1 = np.expand_dims(depth_left, axis=(0, 1)) * 2 - 0.5
                    x2 = np.expand_dims(depth_front, axis=(0, 1)) * 2 - 0.5
                    x3 = np.expand_dims(depth_right, axis=(0, 1)) * 2 - 0.5

                    # x1_flip = np.flip(x1, axis=0).copy()
                    # x2_flip = np.flip(x2, axis=0).copy()
                    # x3_flip = np.flip(x3, axis=0).copy()

                    # print(x1.max())
                    quality = 1 - stability_network.network(torch.from_numpy(x1).to('cuda'), torch.from_numpy(x2).to('cuda'), torch.from_numpy(x3).to('cuda')).squeeze().cpu().detach().numpy()
                    print("quality: " + str(quality))


                    flip_quality = 1 - stability_network.network(torch.flip(torch.from_numpy(x1).to('cuda'), dims=(2,)), torch.flip(torch.from_numpy(x2).to('cuda'), dims=(2,)), torch.flip(torch.from_numpy(x3).to('cuda'), dims=(2,))).squeeze().cpu().detach().numpy()
                    print("flip_quality: " + str(flip_quality))
                    # Run image through quality network-------------------------------------------------------------------------------
                    # quality = 1 - quality #quality_model.predict(x)
                    if quality > highest_quality:
                        best_rotation = new_rotation
                        highest_quality = quality

                    if quality > 0.95 and rot_magnitude < 0.01:
                        use_best = False
                        break

                    # if flip_quality > highest_quality:
                    #     best_rotation = Rotation.from_euler("xyz", [180, 0, 0], degrees=True) * new_rotation
                    #     highest_quality = flip_quality

                    # if flip_quality > 0.95 and quality < 0.5:
                    #     new_rotation = Rotation.from_euler("xyz", [180, 0, 0], degrees=True) * new_rotation
                    #     use_best = False
                    #     break



                    iterations += 1

                if highest_quality > 0.95:
                    break

            if use_best:            
            # # Set new rotation of object
                # self.scene.objects[obj_idx].set_quaternion(list(best_rotation.as_quat()))
                # self.scene.pr.step()
                new_rotation = best_rotation


            # Place object at lowest possible point
            self.scene.objects[obj_idx].set_dynamic(True)
            collision = True
            z = 0
            while collision:
                self.scene.objects[obj_idx].set_quaternion(list(new_rotation.as_quat()))
                self.scene.objects[obj_idx].set_position([0, 0.5, z])
                self.scene.pr.step()

                distance = np.linalg.norm((np.array(self.scene.objects[obj_idx].get_position()) - np.array([0, 0.5, z])))
                angle_distance = 1 - np.abs(np.dot(new_rotation.as_quat(), self.scene.objects[obj_idx].get_quaternion()))

                if angle_distance < 1e-05 and distance < 0.1:
                    collision = False

                z += 0.005

            self.scene.objects[obj_idx].set_dynamic(False)
            self.scene.pr.step()
            self.scene.objects[obj_idx].set_dynamic(True)

            # Record movement while object is unstable
            position_history = []
            count = 0
            t = 0
            
            while count < 5 and t < 1000:
                position_history.append(self.scene.objects[obj_idx].get_position())
                if len(position_history) > 1:
                    p1 = np.array(position_history[-1])
                    p2 = np.array(position_history[-2])
                    disp = np.linalg.norm(p1 - p2)
                    if (disp < 0.0005):
                        count += 1
                self.scene.pr.step()
                t += 1


            # Calculate angle to upright vector
            gt = Rotation.from_euler('xyz', self.scene.objects_zero[obj_idx], degrees=True)
            gt_matrix = np.array(gt.as_matrix())
            axis = gt_matrix[2]

            final = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
            final_matrix = np.array(final.as_matrix())
            final_axis = np.matmul(final_matrix, axis)

            angle_final = py_ang([0, 0, 1], final_axis) * 180 / np.pi

            if (angle_final < 15):
                success_count += 1
                success_per_object[obj_idx] += 1
            num_trails[obj_idx] += 1
            print([0 if j == 0 else i / j * 100 for i, j in zip(success_per_object, num_trails)])


            initial_matrix = np.array(new_rotation.as_matrix())
            initial_axis = np.matmul(initial_matrix, axis)
            angle_initial = py_ang([0, 0, 1], initial_axis) * 180 / np.pi

            if np.abs(angle_final - angle_initial) < 15:
                stable_count += 1
            
            angle_history.append(angle_initial)
            iteration_history.append(iterations)

            # M = (new_rotation * gt.inv()).as_matrix()
            # L = np.arccos(max(min((M[0][0] + M[1][1] + M[2][2] - 1) / 2, -1), 1)) * 180 / np.pi

            # print("ANGLE: ", angle)
            # print("GEODESIC: ", L)


            # Remove object from scene
            self.scene.objects[obj_idx].set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
            self.scene.objects[obj_idx].set_dynamic(False)

        success_rate = success_count / trials
        stable_rate = stable_count / trials
        angle_history = np.array(angle_history)
        angle_rate = np.sum(angle_history) / trials
        iteration_history = np.array(iteration_history)
        iteration_rate = np.sum(iteration_history) / trials

        print("Success rate: ", success_rate)
        print("Stability rate: ", stable_rate)
        print("Angle error: ", angle_rate)
        print("Iterations: ", iteration_rate)
        print(angle_history)

        return success_rate, angle_history    

    def evaluate_gripper_placement(self, max_trials, model, max_closed_loop=1, teleport=True):
        self.scene.reset()
        # quality_model = reset_keras()

        # Import segmentation GAN
        opt = TestOptions().parse()  # get test options
        # hard-code some parameters for test
        opt.num_threads = 0   # test code only supports num_threads = 1
        opt.batch_size = 1    # test code only supports batch_size = 1
        opt.serial_batches = True  # disable data shuffling; comment this line if results on randomly chosen images are needed.
        opt.no_flip = True    # no flip; comment this line if results on flipped images are needed.
        opt.display_id = -1   # no visdom display; the test code saves the results to a HTML file.
        opt.input_nc = 4
        opt.output_nc = 1
        opt.name = "SemSeg"
        # opt.model = "pix52pix"
        opt.direction = "AtoB"
        opt.dataroot = "A"
        opt.load_iter = 0
        opt.epoch = 45
        print(opt)
        seg_model = create_model(opt)      # create a model given opt.model and other options
        seg_model.setup(opt)               # regular setup: load and print networks; create schedulers

        # test with eval mode. This only affects layers like batchnorm and dropout.
        # For [pix2pix]: we use batchnorm and dropout in the original pix2pix. You can experiment it with and without eval() mode.
        # For [CycleGAN]: It should not affect CycleGAN as CycleGAN uses instancenorm without dropout.

        stability_network = PlacementNetwork(stable_network=True)
        stability_network.import_model("/home/rhys/Desktop/placingobjects/data/models/2020-09-30-18:42_set1/1.pt")

        joint_pos = self.scene.robot.og_joints
        joint_pos[1]-=0.2
        self.scene.robot.robot.set_joint_positions(joint_pos)
        n_obj = len(self.scene.objects)
        trial = 0

        self.scene.robot.robot.set_joint_positions(self.scene.robot.og_joints)
        self.scene.pr.step()

        success_count = 0
        stable_count = 0
        infeasible_count = 0
        total_count = 0

        for obj in self.scene.objects:
            obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
            obj.set_dynamic(False)
            obj.set_mass(0.5)

        angle_history = []
        iteration_history = []

        fail_count = 0

        while trial < max_trials:

            # Sample random object
            if self.id:
                obj_idx = self.id
            else:
                obj_idx = random.randint(0, n_obj - 1)

            # Add noise to angle
            alpha = random.random()*2*np.pi 
            beta = random.random()*2*np.pi 
            gamma = random.random()*2*np.pi 

            # Add noise to position
            pos = [0.5, 0, 0.5]
            pos[0] += (random.random()*2-1)*0.01
            pos[1] += (random.random()*2-1)*0.01
            pos[2] += (random.random()*2-1)*0.01

            # Set new angle and position
            r = Rotation.from_euler('xyz', [alpha, beta, gamma], degrees=False)
            quat = list(r.as_quat())

            # Add noise to gripper angle
            _alpha_ = random.random()*2*np.pi 
            _beta_ = random.random()*np.pi 
            _gamma_ = random.random()*2*np.pi 

            gripper_matrix = Rotation.from_euler('xyz', [_alpha_, _beta_, _gamma_], degrees=False).as_matrix()
            gripper_axis = np.matmul(gripper_matrix, np.array([0, 0, 1]))
            gripper_2D = gripper_axis[:-1]
            gripper_theta = np.arctan2(gripper_2D[1], gripper_2D[0])
            transform_rotation = Rotation.from_euler('xyz', [0, 0, -gripper_theta], degrees=False)
            gripper_rotation = Rotation.from_euler('xyz', [_alpha_, _beta_, _gamma_], degrees=False)
            new_rotation = transform_rotation * gripper_rotation

            self.scene.robot.gripper.release()
            self.scene.open_gripper()
            self.scene.robot.robot.max_velocity = 1.0
            self.scene.table.set_position([-5, -5, -5])
            iterations = 0
            total_count += 1
            highest_quality = 0


            for obj in self.scene.objects:
                obj.set_position([-5.0+random.random()*2.5, 5.0+random.random()*2.5, 5.0+random.random()*2.5])
                obj.set_dynamic(False)
                obj.set_mass(0.5)


            try: 
                # Get joint configuration
                joint = self.scene.robot.robot.get_configs_for_tip_pose([0.5, 0, 0.5], 
                                                                        quaternion=list(new_rotation.as_quat()),
                                                                        ignore_collisions=True,
                                                                        trials=1, 
                                                                        max_configs=1)[0]

                self.scene.robot.robot.set_joint_positions(joint)
            except ConfigurationError:
                print('Failed to acheive randomly sampled gripper pose')
                continue

            # Bring object to scene
            self.scene.objects[obj_idx].set_quaternion(quat)
            self.scene.objects[obj_idx].set_position(pos)

            # Ensure object does not collide with gripper
            if self.scene.objects[obj_idx].check_collision():
                self.scene.object_purgatory(obj_idx)
                continue
            self.scene.pr.step()

            # Close gripper fingers
            self.scene.close_gripper(max_force=100)
            self.gripper_pos = self.scene.robot.gripper.get_joint_positions()
            # print(self.scene.objects[obj_idx].get_position())
            self.scene.robot.gripper.grasp(self.scene.objects[obj_idx])
            self.scene.objects[obj_idx].set_dynamic(True)
            completed = True
            obj_rotation = None

            # for i in range(5):
            #     print(self.scene.objects[obj_idx].get_position())
            #     self.scene.pr.step()   

            use_best = True 

            for _ in range(5):
                
                # Find rotation through NN
                for _ in range(max(1, max_closed_loop // 5)):
                    iterations += 1

                    dist_from_origin = np.linalg.norm(np.array([0.5, 0, 0.5]) - np.array(self.scene.objects[obj_idx].get_position()))
                    print("Taking photo...")
                    print(dist_from_origin)
                    if self.scene.check_object_dropped(obj_idx) or dist_from_origin > 0.075:
                        print("Object Dropped :(")
                        completed = False
                        self.scene.robot.gripper.release()
                        self.scene.object_purgatory(obj_idx)
                        break
                    
                    # ---------------------------------------------------
                    # PLACEMENT NETWORK
                    # ---------------------------------------------------
                    GAN_SIZE = (256, 256)

                    rgb_left = self.scene.left_camera.capture_rgb()
                    rgb_right = self.scene.right_camera.capture_rgb()
                    rgb_front = self.scene.front_camera.capture_rgb() 

                    rgb_left = cv2.resize(rgb_left, dsize=GAN_SIZE) * 2 - 1
                    rgb_front = cv2.resize(rgb_front, dsize=GAN_SIZE) * 2 - 1
                    rgb_right = cv2.resize(rgb_right, dsize=GAN_SIZE) * 2 - 1

                    depth_left = self.scene.left_camera.capture_depth().img
                    depth_right = self.scene.right_camera.capture_depth().img
                    depth_front = self.scene.front_camera.capture_depth().img

                    depth_left = np.expand_dims(cv2.resize(depth_left, dsize=GAN_SIZE), -1) * 4 - 1
                    depth_right = np.expand_dims(cv2.resize(depth_right, dsize=GAN_SIZE), -1) * 4 - 1
                    depth_front = np.expand_dims(cv2.resize(depth_front, dsize=GAN_SIZE), -1) * 4 - 1

                    rgbd_left = np.expand_dims(np.concatenate((rgb_left, depth_left), 2), 0)
                    rgbd_right = np.expand_dims(np.concatenate((rgb_right, depth_right), 2), 0)
                    rgbd_front = np.expand_dims(np.concatenate((rgb_front, depth_front), 2), 0)

                    rgbd_left = torch.from_numpy(rgbd_left).permute(0,3,1,2).type(torch.FloatTensor).to('cuda')
                    rgbd_right = torch.from_numpy(rgbd_right).permute(0,3,1,2).type(torch.FloatTensor).to('cuda')
                    rgbd_front = torch.from_numpy(rgbd_front).permute(0,3,1,2).type(torch.FloatTensor).to('cuda')

                    seg_model.eval()

                    seg_model.set_input({"A":rgbd_left})  # unpack data from data loader
                    seg_model.test()           # run inference
                    depth_left = seg_model.fake_B / 2  # get image results
                    depth_left = depth_left.cpu().numpy().squeeze()
                    seg_model.set_input({"A":rgbd_right})  # unpack data from data loader
                    seg_model.test()           # run inference
                    depth_right = seg_model.fake_B / 2  # get image results
                    depth_right = depth_right.cpu().numpy().squeeze()
                    seg_model.set_input({"A":rgbd_front})  # unpack data from data loader
                    seg_model.test()           # run inference
                    depth_front = seg_model.fake_B / 2  # get image results
                    depth_front = depth_front.cpu().numpy().squeeze()
                    cv2.imwrite("output.png", (depth_left + 1) * 120)

                    # Prepare image for network
                    self.scene.robot.gripper.set_joint_positions(self.gripper_pos)
                    self.scene.pr.step()
                    size = (64,64)
                    depth_left = cv2.resize(depth_left, dsize=size)
                    depth_front = cv2.resize(depth_front, dsize=size)
                    depth_right = cv2.resize(depth_right, dsize=size)
                    # TODO: FIX ORDER OF IMAGES
                    x1 = np.expand_dims(depth_left, axis=(0, 1)) 
                    x2 = np.expand_dims(depth_front, axis=(0, 1)) 
                    x3 = np.expand_dims(depth_right, axis=(0, 1)) 
                    # x4 = np.expand_dims(depth_back, axis=(0, 1)) * 2 - 0.5

                    # Run image through rotation network
                    placement = model.network(torch.from_numpy(x1).to('cuda'), torch.from_numpy(x2).to('cuda'), torch.from_numpy(x3).to('cuda')).squeeze().cpu().detach().numpy()
                    transform_rotation = Rotation.from_matrix(placement)

                    # Find solved rotation of object
                    gripper_rotation = Rotation.from_quat(self.scene.robot.robot.get_tip().get_quaternion())
                    new_rotation = transform_rotation * gripper_rotation

                    rot_magnitude = 1 - np.abs(np.dot([0, 0, 0, 1], transform_rotation.as_quat()))
                    print(rot_magnitude)

                    try:
                        path = self.scene.robot.robot.get_path(position=[0.5, 0, 0.5],
                                                            quaternion=list(new_rotation.as_quat()),
                                                            ignore_collisions=True)

                        done = False
                        steps = 0
                        while not done:
                            done = path.step()
                            self.scene.pr.step()
                            steps += 1
                            if steps >= 5000:
                                raise ConfigurationPathError
                            

                    except ConfigurationPathError:
                        self.scene.robot.gripper.release()
                        self.scene.object_purgatory(obj_idx)
                        completed = False
                        print('Failed to rotate to NN angle')
                        break

                    # Determine how to rotate it so it points forwards
                    gripper_matrix = Rotation.from_quat(self.scene.robot.robot.get_tip().get_quaternion()).as_matrix()
                    gripper_axis = np.matmul(gripper_matrix, np.array([0, 0, 1]))
                    gripper_2D = gripper_axis[:-1]
                    gripper_theta = np.arctan2(gripper_2D[1], gripper_2D[0])
                    transform_rotation = Rotation.from_euler('xyz', [0, 0, -gripper_theta], degrees=False)
                    gripper_rotation = Rotation.from_quat(self.scene.robot.robot.get_tip().get_quaternion())
                    new_rotation = transform_rotation * gripper_rotation

                    try:
                        path = self.scene.robot.robot.get_path(position=[0.5, 0, 0.5],
                                                            quaternion=list(new_rotation.as_quat()),
                                                            ignore_collisions=True)

                        done = False
                        steps = 0
                        while not done:
                            done = path.step()
                            self.scene.pr.step()
                            steps += 1
                            if steps >= 5000:
                                raise ConfigurationPathError

                    except ConfigurationPathError:
                        self.scene.robot.gripper.release()
                        self.scene.object_purgatory(obj_idx)
                        completed = False
                        print('Failed to rotate to frontfacing angle')

                        break

                    # ---------------------------------------------------
                    # QUALITY NETWORK
                    # ---------------------------------------------------
                    rgb_left = self.scene.left_camera.capture_rgb()
                    rgb_right = self.scene.right_camera.capture_rgb()
                    rgb_front = self.scene.front_camera.capture_rgb() 

                    rgb_left = cv2.resize(rgb_left, dsize=GAN_SIZE) * 2 - 1
                    rgb_front = cv2.resize(rgb_front, dsize=GAN_SIZE) * 2 - 1
                    rgb_right = cv2.resize(rgb_right, dsize=GAN_SIZE) * 2 - 1

                    depth_left = self.scene.left_camera.capture_depth().img
                    depth_right = self.scene.right_camera.capture_depth().img
                    depth_front = self.scene.front_camera.capture_depth().img

                    depth_left = np.expand_dims(cv2.resize(depth_left, dsize=GAN_SIZE), -1) * 4 - 1
                    depth_right = np.expand_dims(cv2.resize(depth_right, dsize=GAN_SIZE), -1) * 4 - 1
                    depth_front = np.expand_dims(cv2.resize(depth_front, dsize=GAN_SIZE), -1) * 4 - 1

                    rgbd_left = np.expand_dims(np.concatenate((rgb_left, depth_left), 2), 0)
                    rgbd_right = np.expand_dims(np.concatenate((rgb_right, depth_right), 2), 0)
                    rgbd_front = np.expand_dims(np.concatenate((rgb_front, depth_front), 2), 0)

                    rgbd_left = torch.from_numpy(rgbd_left).permute(0,3,1,2).type(torch.FloatTensor).to('cuda')
                    rgbd_right = torch.from_numpy(rgbd_right).permute(0,3,1,2).type(torch.FloatTensor).to('cuda')
                    rgbd_front = torch.from_numpy(rgbd_front).permute(0,3,1,2).type(torch.FloatTensor).to('cuda')

                    seg_model.eval()

                    seg_model.set_input({"A":rgbd_left})  # unpack data from data loader
                    seg_model.test()           # run inference
                    depth_left = seg_model.fake_B / 2  # get image results
                    depth_left = depth_left.cpu().numpy().squeeze()
                    seg_model.set_input({"A":rgbd_right})  # unpack data from data loader
                    seg_model.test()           # run inference
                    depth_right = seg_model.fake_B / 2  # get image results
                    depth_right = depth_right.cpu().numpy().squeeze()
                    seg_model.set_input({"A":rgbd_front})  # unpack data from data loader
                    seg_model.test()           # run inference
                    depth_front = seg_model.fake_B / 2  # get image results
                    depth_front = depth_front.cpu().numpy().squeeze()
                    print(depth_left.shape)
                    cv2.imwrite("output.png", (depth_left + 1) * 120)

                    depth_left = cv2.resize(depth_left, dsize=size)
                    depth_front = cv2.resize(depth_front, dsize=size)
                    depth_right = cv2.resize(depth_right, dsize=size)
                    # TODO: FIX ORDER OF IMAGES
                    x1 = np.expand_dims(depth_left, axis=(0, 1)) 
                    x2 = np.expand_dims(depth_front, axis=(0, 1)) 
                    x3 = np.expand_dims(depth_right, axis=(0, 1)) 

                    # print(x1.max())
                    quality = 1 - stability_network.network(torch.from_numpy(x1).to('cuda'), torch.from_numpy(x2).to('cuda'), torch.from_numpy(x3).to('cuda')).squeeze().cpu().detach().numpy()
                    print("quality: " + str(quality))

                    # Run image through quality network-------------------------------------------------------------------------------
                    # quality = 1 - quality #quality_model.predict(x)
                    if quality > highest_quality:
                        best_rotation = new_rotation
                        highest_quality = quality

                    if quality > 0.95 and rot_magnitude < 0.01:
                        use_best = False
                        break

            

                    iterations += 1

                if not completed:
                    break

                if highest_quality > 0.95 or max_closed_loop == 1:
                    break

                # Add noise to gripper angle
                _alpha_ = random.random()*2*np.pi 
                _beta_ = random.random()*np.pi 
                _gamma_ = random.random()*2*np.pi 

                gripper_matrix = Rotation.from_euler('xyz', [_alpha_, _beta_, _gamma_], degrees=False).as_matrix()
                gripper_axis = np.matmul(gripper_matrix, np.array([0, 0, 1]))
                gripper_2D = gripper_axis[:-1]
                gripper_theta = np.arctan2(gripper_2D[1], gripper_2D[0])
                transform_rotation = Rotation.from_euler('xyz', [0, 0, -gripper_theta], degrees=False)
                gripper_rotation = Rotation.from_euler('xyz', [_alpha_, _beta_, _gamma_], degrees=False)
                new_rotation = transform_rotation * gripper_rotation

                # Set new rotation of object
                try:
                    path = self.scene.robot.robot.get_path(position=[0.5, 0, 0.5],
                                                            quaternion=list(new_rotation.as_quat()),
                                                            ignore_collisions=True)

                    done = False
                    steps = 0
                    while not done:
                        done = path.step()
                        self.scene.pr.step()
                        steps += 1
                        if steps >= 5000:
                            raise ConfigurationPathError

                except ConfigurationPathError:
                    self.scene.robot.gripper.release()
                    self.scene.object_purgatory(obj_idx)
                    print('Failed to rotate to new random position')
                    completed = False

                    break

            if max_closed_loop > 1 and completed:
                if use_best:            
                # # Set new rotation of object
                    # self.scene.objects[obj_idx].set_quaternion(list(best_rotation.as_quat()))
                    # self.scene.pr.step()
                    print("Using best")
                    new_rotation = best_rotation

                    # Set new rotation of object
                    try:
                        path = self.scene.robot.robot.get_path(position=[0.5, 0, 0.5],
                                                                quaternion=list(new_rotation.as_quat()),
                                                                ignore_collisions=True)

                        done = False
                        steps = 0
                        while not done:
                            done = path.step()
                            self.scene.pr.step()
                            steps += 1
                            if steps >= 5000:
                                raise ConfigurationPathError

                    except ConfigurationPathError:
                        self.scene.robot.gripper.release()
                        self.scene.object_purgatory(obj_idx)
                        print('Failed to rotate to best position')
                        completed = False

            if fail_count > 500:
                print("Something seems to be going wrong. Aborting")
                break

            if not completed:
                self.scene.object_purgatory(obj_idx)
                self.scene.open_gripper()
                fail_count += 1
                continue


            completed = False
            obj_rotation = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())

            if teleport:
                # # Find solved rotation of object
                # obj_rotation = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
                # new_rotation = transform_rotation * obj_rotation
                # obj_rotation = new_rotation

                # self.scene.pr.step()


                # Place object at lowest possible point ---------------------------------------------------------------------------
                collision = True
                self.scene.robot.gripper.release()
                self.scene.objects[obj_idx].set_dynamic(True)
                z = 0
                while collision:
                    self.scene.objects[obj_idx].set_quaternion(list(obj_rotation.as_quat()))
                    self.scene.objects[obj_idx].set_position([0, 0.5, z])
                    self.scene.pr.step()

                    distance = np.linalg.norm((np.array(self.scene.objects[obj_idx].get_position()) - np.array([0, 0.5, z])))
                    angle_distance = 1 - np.abs(np.dot(obj_rotation.as_quat(), self.scene.objects[obj_idx].get_quaternion()))

                    if angle_distance < 1e-05 and distance < 0.1:
                        collision = False

                    z += 0.025

                self.scene.objects[obj_idx].set_dynamic(False)
                self.scene.pr.step()
                self.scene.objects[obj_idx].set_dynamic(True)

                # Record movement while object is unstable
                position_history = []
                count = 0
                t = 0
                
                while count < 5 and t < 1000:
                    position_history.append(self.scene.objects[obj_idx].get_position())
                    if len(position_history) > 1:
                        p1 = np.array(position_history[-1])
                        p2 = np.array(position_history[-2])
                        disp = np.linalg.norm(p1 - p2)
                        if (disp < 0.0005):
                            count += 1
                    self.scene.pr.step()
                    t += 1

                completed = True

            else:
                # Check if gripper is in feasible orientation for placing
                gripper_matrix = Rotation.from_quat(self.scene.robot.robot.get_tip().get_quaternion()).as_matrix()
                gripper_axis = np.matmul(gripper_matrix, np.array([0, 0, 1]))
                gripper_pitch = np.arctan(gripper_axis[2]/np.sqrt(gripper_axis[0]**2 + gripper_axis[1]**2)) * 180 / np.pi
                # print(gripper_pitch)
                if gripper_pitch > 45:
                    print('Gripper in infeasible orientation')
                    self.scene.robot.gripper.release()
                    self.scene.object_purgatory(obj_idx)
                    infeasible_count += 1
                    continue

                
                if self.scene.check_object_dropped(obj_idx):
                    print("Object Dropped :(")
                    completed = False
                    self.scene.robot.gripper.release()
                    self.scene.object_purgatory(obj_idx)
                    break


                # Place object at lowest possible point
                self.scene.table.set_position(self.scene.table_og_position)
                success = False
                down_position = copy.deepcopy(self.scene.robot.robot.get_tip().get_position())
                down_position[2] = 0
                self.scene.robot.robot.max_velocity = 0.5
                placement_position = [0.675, 0, 0.5]

                try:
                    path = self.scene.robot.robot.get_path(position=placement_position,
                                                    quaternion=self.scene.robot.robot.get_tip().get_quaternion(),
                                                    ignore_collisions=True)
                    done = False
                    while not done:
                        done = path.step()
                        self.scene.pr.step()

                        success = True

                except ConfigurationPathError:
                    print("Failed to approach table")
                    self.scene.object_purgatory(obj_idx)
                    self.scene.open_gripper()
                    continue


                if self.scene.check_object_dropped(obj_idx):
                    print("Object Dropped :(")
                    completed = False
                    self.scene.robot.gripper.release()
                    self.scene.object_purgatory(obj_idx)
                    break


                prev_force = self.scene.robot.robot.get_joint_forces()[0]
                # placement_position[2] = 0.375
                alpha = 0.5
                # try:
                #     path = self.scene.robot.robot.get_linear_path(position=placement_position,
                #                                     quaternion=self.scene.robot.robot.get_tip().get_quaternion(),
                #                                     ignore_collisions=True,
                #                                     steps=250)
                #     done = False
                #     count = 0
                #     while not done:
                #         done = path.step()
                #         self.scene.pr.step()

                #         # print(prev_force, self.scene.robot.robot.get_joint_forces()[0])
                #         print(self.scene.objects[obj_idx].check_distance(self.scene.table))
                #         if count > 25 and abs(self.scene.robot.robot.get_joint_forces()[0] - prev_force) > 2.5:
                #             print('Hit joint force limit')
                #             done = True
                #             success = False
                #         prev_force = (1 - alpha)*prev_force + alpha*self.scene.robot.robot.get_joint_forces()[0]
                #         count += 1

                # except ConfigurationPathError:
                #     print("Failed to move down")
                #     self.scene.object_purgatory(obj_idx)
                #     self.scene.open_gripper()
                #     continue

                max_force, min_force = self.scene.robot.robot.get_joint_forces()[0], self.scene.robot.robot.get_joint_forces()[0]
                while success:
                    try:
                        path = self.scene.robot.robot.get_linear_path(position=placement_position,
                                                        quaternion=self.scene.robot.robot.get_tip().get_quaternion(),
                                                        ignore_collisions=True)
                        done = False
                        count = 0
                        while not done:
                            done = path.step()
                            self.scene.pr.step()

                            current_force = self.scene.robot.robot.get_joint_forces()[0]
                            print(min_force, max_force, current_force)
                            if count > 5 and (max(current_force - max_force, min_force - current_force) > 2.0 or abs(current_force) > 25):
                                print('Hit joint force limit')
                                success = False
                                done = True
                            # prev_force = (1 - alpha)*prev_force + alpha*self.scene.robot.robot.get_joint_forces()[0]
                            max_force = max(max_force, current_force)
                            min_force = min(min_force, current_force)
                            count += 1

                        placement_position[2] -= 0.005
                        completed = True

                    except ConfigurationPathError:
                        print("Failed to place object down")
                        success = False
                        completed = False
                        self.scene.robot.gripper.release()
                        self.scene.object_purgatory(obj_idx)

                if not completed:
                    self.scene.object_purgatory(obj_idx)
                    self.scene.open_gripper()
                    continue
                
                completed = False

                self.scene.robot.gripper.release()
                self.scene.objects[obj_idx].set_dynamic(True)
                self.scene.open_gripper()


                # Retract arm
                gripper_matrix = Rotation.from_quat(self.scene.robot.robot.get_tip().get_quaternion()).as_matrix()
                gripper_axis = np.matmul(gripper_matrix, np.array([0, 0, 1]))
                retract_position = list(np.array(self.scene.robot.robot.get_tip().get_position()) - gripper_axis*0.125)
                self.scene.robot.robot.max_velocity = 1.0

                try:
                    path = self.scene.robot.robot.get_linear_path(position=retract_position,
                                                    quaternion=self.scene.robot.robot.get_tip().get_quaternion(),
                                                    ignore_collisions=True)

                    done = False
                    steps = 0
                    while not done:
                        done = path.step()
                        self.scene.pr.step()
                        steps += 1
                        if steps > 10000:
                            raise ConfigurationPathError
                    completed = True
                except ConfigurationPathError:
                    self.scene.object_purgatory(obj_idx)
                    self.scene.open_gripper()
                    completed = True
                    print('Failed to retract')

            if not completed:
                self.scene.object_purgatory(obj_idx)
                self.scene.open_gripper()
                continue
            # Calculate angle to upright vector
            gt = Rotation.from_euler('xyz', self.scene.objects_zero[obj_idx], degrees=True)
            gt_matrix = np.array(gt.as_matrix())
            axis = gt_matrix[2]

            final = Rotation.from_quat(self.scene.objects[obj_idx].get_quaternion())
            final_matrix = np.array(final.as_matrix())
            final_axis = np.matmul(final_matrix, axis)

            angle_final = py_ang([0, 0, 1], final_axis) * 180 / np.pi

            if (angle_final < 15):
                success_count += 1

            initial_matrix = np.array(obj_rotation.as_matrix())
            initial_axis = np.matmul(initial_matrix, axis)
            angle_initial = py_ang([0, 0, 1], initial_axis) * 180 / np.pi

            if np.abs(angle_final - angle_initial) < 15:
                stable_count += 1
            
            angle_history.append(angle_initial)
            iteration_history.append(iterations)


            # Send object away and open gripper
            self.scene.object_purgatory(obj_idx)
            self.scene.open_gripper()

            trial += 1
            fail_count = 0
            print("Evaluation: ", trial, "/", max_trials, "\tSuccess rate: ", success_count / max(1, trial))

            
        success_rate = success_count / max_trials
        stable_rate = stable_count / max_trials
        angle_history = np.array(angle_history)
        angle_rate = np.sum(angle_history) / max_trials
        iteration_history = np.array(iteration_history)
        iteration_rate = np.sum(iteration_history) / max_trials
        infeasible_rate = infeasible_count / (max_trials + infeasible_count)
        failed_ik_count = total_count - max_trials
        failed_ik_rate = failed_ik_count / total_count

        print("Success rate: ", success_rate)
        print("Stability rate: ", stable_rate)
        print("Angle error: ", angle_rate)
        print("Iterations: ", iteration_rate)
        print("Infeasible rate: ", infeasible_rate)
        print("Failed IK rate: ", failed_ik_rate)
        print(angle_history)

        f = open(self.loop + ".txt", 'a')
        f.writelines(["Success rate: ", str(success_rate), '\n'])
        f.writelines(["Stability rate: ", str(stable_rate), '\n'])
        f.writelines(["Angle error: ", str(angle_rate), '\n'])
        f.writelines(["Iterations: ", str(iteration_rate), '\n'])
        f.writelines(["Infeasible rate: ", str(infeasible_rate), '\n'])
        f.writelines(["Failed IK rate: ", str(failed_ik_rate), '\n'])
        f.writelines([str(angle_history), '\n\n'])
        f.close()

        return success_rate, angle_history 



    def save_upright_td(self, depth_left, depth_front, depth_right, depth_back, obj_idx):
        transform_rotation = self.scene.get_closest_trasform(obj_idx)
        quaternion = transform_rotation.as_quat()

        td = TrainingData(depth_left, depth_front, depth_right, depth_back)
        placement = to_placement(quaternion=quaternion)

        td.add_placement(placement)
        td.write_file()

    def safe_get(self, image, ij):
        try:
            return image[ij[0],ij[1]]
        except IndexError:
            return 0

    def get_segmented(self, rgb, depth):

        self.index += 7
        img1 = depth
        img2 = rgb

        hsv = cv2.cvtColor(img2, cv2.COLOR_RGB2HSV)

        img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)


        mask = cv2.inRange(hsv, np.array([1,1,0]), np.array([255,255,255]))
        res = cv2.bitwise_and(hsv,hsv, mask= mask)

        depthres = cv2.bitwise_and(img1,img1, mask= mask)

        canny = np.uint8(res)
        edges = cv2.Canny(canny,100,200)


        kernel = np.ones((4,4), np.uint8)  # note this is a horizontal kernel
        d_im = cv2.dilate(edges, kernel, iterations=1)
        e_im = cv2.erode(d_im, kernel, iterations=1)

        height, width, depth = depthres.shape

        for i in range(0, height):
            for j in range(0, width):
                if sum(depthres[i,j]) == 0:
                    if e_im[i,j] != 0:
                        neighbours = [(i-1,j+1), (i, j+1), (i+1, j+1), (i-1, j), (i+1,j), (i-1,j-1), (i,j-1), (i+1,j-1)]
                        values = np.array(list(map(lambda x : self.safe_get(depthres, x), neighbours)))
                        # print(values)	
                        depthres[i,j] = values[values!=0].mean()

        return cv2.cvtColor(depthres, cv2.COLOR_BGR2GRAY)
