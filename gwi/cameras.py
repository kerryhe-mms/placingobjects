"""
Cameras

Author: Doug Morrison
"""

import numpy as np

from pyrep.objects.vision_sensor import VisionSensor
from pyrep.const import PerspectiveMode


class CameraProperties:
    def __init__(self, resolution, extrinsic_matrix, intrinsic_matrix, near_plane=0.0, far_plane=1.0):
        self.resolution = resolution
        self.extrinsic_matrix = extrinsic_matrix
        self.intrinsic_matrix = intrinsic_matrix
        self.near_plane = near_plane
        self.far_plane = far_plane

    def to_dict(self):
        res = self.resolution
        if isinstance(res, np.ndarray):
            res = res.tolist()
        return {
            'resolution': res,
            'extrinsic_matrix': self.extrinsic_matrix.tolist(),
            'intrinsic_matrix': self.intrinsic_matrix.tolist(),
            'near_plane': self.near_plane,
            'far_plane': self.far_plane
        }

    @classmethod
    def from_dict(cls, d):
        return cls(
            d['resolution'],
            np.array(d['extrinsic_matrix']),
            np.array(d['intrinsic_matrix']),
            near_plane=d['near_plane'],
            far_plane=d['far_plane']
        )

    @property
    def camera_matrix(self):
        """ Get Camera matrix

        :return: 3x4 Camera matrix
        """
        # Note to self, great figure:
        # https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html
        return self.intrinsic_matrix @ self.extrinsic_matrix

    @property
    def pose_matrix(self):
        """ Get the inverse of the camera's extrinsic matrix.
            (transform from camera to world coordinates)
            world_P_camera

        :return:
        """
        # return np.array(self.extrinsic_matrix).reshape(3, 4)
        R = np.linalg.inv(np.vstack((self.extrinsic_matrix, [0, 0, 0, 1])))
        return R[:3, ]

    def point_to_px(self, point, check_limits=True):
        """ Convert a point to a projected pixel coordinate.

        :param point: Point in world coordinates [x, y, z]
        :return: Pixel [u, v]
        """
        p = np.array(point + [1]).reshape(4, 1)  # [[x], [y], [z], [1]]
        C = self.camera_matrix
        u, v, s = (C @ p).squeeze()
        # Perspective divide and floor
        u = int(u/s)
        v = int(v/s)

        if check_limits:
            if u < 0 or u >= self.resolution[0] or v < 0 or v >= self.resolution[1]:
                raise ValueError('Point {} lies outside of the view frustrum.'.format(point))
            if s < self.near_plane:
                raise ValueError('Point {} is behind the near clipping plane.'.format(point))
            if s > self.far_plane:
                raise ValueError('Point {} is beyond the far clipping plane.'.format(point))

        return [u, v]

    def px_to_point(self, px, depth):
        """ Convert a pixel + depth to a 3D point in world coordinates.

        :param px: Pixel [u, v]
        :param depth: Depth (metres)
        :return: 3D Point [x, y, z] in world coordinates.
        """
        I = self.intrinsic_matrix
        x = (px[0]+0.5 - I[0, 2]) / I[0, 0] * depth
        y = (px[1]+0.5 - I[1, 2]) / I[1, 1] * depth
        R = self.pose_matrix
        p = np.array([x, y, depth, 1]).reshape((4, 1))
        P = (R @ p).flatten()[:3].tolist()
        return P


class DepthImage:
    def __init__(self, img, camera_properties=None):
        self.img = img
        self.camera_properties = camera_properties


class Camera:
    def get_intrinsic_matrix(self):
        raise NotImplementedError()

    def get_extrinsic_matrix(self):
        raise NotImplementedError()

    def get_camera_properties(self):
        raise NotImplementedError()


class PyRepCamera(Camera):
    def __init__(self, camera_handle):
        camera = VisionSensor(camera_handle)
        if camera.get_perspective_mode() != PerspectiveMode.PERSPECTIVE:
            raise TypeError('Designed to work with a perspective camera only.')
        self.cam = camera

    def capture_rgb(self):
        """

        :return:
        """
        return self.cam.capture_rgb()

    def capture_depth(self):
        """

        :return:
        """
        img = self.cam.capture_depth(in_meters=True)
        return DepthImage(img, self.get_camera_properties())

    def get_intrinsic_matrix(self):
        """ Get the camera intrinsic matrix

        :return: 3x3 Camera Intrinsic Matrix
        """
        res_x, res_y = self.cam.resolution
        alpha = self.cam.get_perspective_angle()

        # Perspective angle defines angle along the longest image dimension.
        # http://www.coppeliarobotics.com/helpFiles/en/visionSensorPropertiesDialog.htm
        if res_x > res_y:
            a_x = alpha
            a_y = res_y/res_x * alpha
        else:
            a_x = res_x/res_y * alpha
            a_y = alpha

        # Compute focal lengths
        # https://en.wikipedia.org/wiki/Angle_of_view#Calculating_a_camera's_angle_of_view
        f_x = 1.0/np.tan(np.radians(a_x)/2.0) * res_x/2.0
        f_y = 1.0/np.tan(np.radians(a_y)/2.0) * res_y/2.0

        # Image Center
        c_x = res_x/2.0
        c_y = res_y/2.0

        # V-Rep convention has camera x as left and y as up, (opposite to
        # image convention) so negate f_x and f_y.
        I = np.array([
            [-f_x,  0  , c_x],
            [ 0  , -f_y, c_y],
            [ 0  ,  0  ,  1 ]
        ])
        return I

    def get_extrinsic_matrix(self):
        """ Get the camera's extrinsic matrix (transform to camera coordinates)
            camera_P_world

        :return: 3x4 extrinsic matrix
        """
        R = np.array(self.cam.get_matrix()).reshape(3, 4)
        R = np.linalg.inv(np.vstack((R, [0, 0, 0, 1])))
        return R[:3, ]

    def get_camera_properties(self):
        return CameraProperties(self.cam.resolution, self.get_extrinsic_matrix(),
                                self.get_intrinsic_matrix(),
                                near_plane=self.cam.get_near_clipping_plane(),
                                far_plane=self.cam.get_far_clipping_plane())

    def get_world_transformation_matrix(self):
        m = self.cam.get_matrix()
        m += [0,0,0,1]
        return np.reshape(np.array(m), (4,4))


class ROSCamera(Camera):
    pass
    # TODO