import numpy as np
import pickle
from scipy.spatial.transform import Rotation
import cv2

import os 
import glob

import matplotlib.pyplot as plt
import math

global_id = 99999

class TrainingData:
    def __init__(self, dl=[],df=[],dr=[], db=[], rl=[], rf=[], rr=[]):
        self.dir_path =  os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + '/data/training_data_segmentation'

        self._dict = {'depth_left': dl,
                      'depth_right': dr,
                      'depth_front': df,
                      'depth_back': db,
                      'rgb_left': rl,
                      'rgb_right': rr,
                      'rgb_front': rf,
                      'placements': []}

    def add_placement(self, placement):
        self._dict['placements'].append(placement)

    def write_file(self, name="", max_samples=None, success_count=0):
        # dir_list = sorted([int(d[:-3]) for d in os.listdir(self.dir_path)])
        global global_id
        global_id += 1
        # if max_samples:
            # if len(dir_list) > max_samples:
            #     import sys
            #     sys.exit("Reached desired number of training samples")

        # if len(dir_list) == 0:
            # file_id = str(len(self))
        # else:
            # file_id = str(dir_list[-1] + len(self))

        file_name = str(name) + "_" + str(global_id) + '.td'
        print(name)
        if success_count == 0:
            count = len(list(filter(lambda x : "/" + name in x, glob.glob(self.dir_path + "/*"))))
        else:
            count = success_count
        # print(glob.glob(self.dir_path + "/*"))
        # print(cou/ant)
        # file_name = str(global_id) + '.td'
        
        with open(self.dir_path + '/' + file_name, 'wb+') as save_file:
            pickle.dump(self._dict, save_file)

        return count + 1

    def add(self, gt_front, gt_left, gt_right):
            # td.add(depth_front, depth_left, depth_right)
            # td.write_seg_file(self.scene.obj_name[obj_idx])
        self._dict["gt_front"] = gt_front
        self._dict["gt_left"] = gt_left
        self._dict["gt_right"] = gt_right

    def import_file(self, file_name):
        try:
            with open(self.dir_path + '/' + file_name, 'rb') as open_file:
                self._dict = pickle.load(open_file)
        except:
            pass


    def import_last_file(self):
        dir_list = sorted([int(d[:-3]) for d in os.listdir(self.dir_path)])
        self.import_file(str(dir_list[-1]) + '.td')

    def safe_get(self, image, ij):
        try:
            return image[ij[0],ij[1]]
        except IndexError:
            return 0



    def get_segmented(self, rgb, depth):

        img1 = np.uint8(depth)
        img2 = np.uint8(rgb)

        hsv = cv2.cvtColor(img2, cv2.COLOR_RGB2HSV)

        img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)


        mask = cv2.inRange(hsv, np.array([10,10,0]), np.array([255,255,255]))
        res = cv2.bitwise_and(hsv,hsv, mask= mask)

        depthres = cv2.bitwise_and(img1,img1, mask= mask)

        canny = np.uint8(res)
        edges = cv2.Canny(canny,100,200)


        kernel = np.ones((4,4), np.uint8)  # note this is a horizontal kernel
        d_im = cv2.dilate(edges, kernel, iterations=1)
        e_im = cv2.erode(d_im, kernel, iterations=1)

        height, width, depth = depthres.shape

        for i in range(0, height):
            for j in range(0, width):
                if sum(depthres[i,j]) == 0:
                    if e_im[i,j] != 0:
                        neighbours = [(i-1,j+1), (i, j+1), (i+1, j+1), (i-1, j), (i+1,j), (i-1,j-1), (i,j-1), (i+1,j-1)]
                        values = np.array(list(map(lambda x : self.safe_get(depthres, x), neighbours)))
                        # print(values)	
                        try:
                            depthres[i,j] = values[values!=0].mean()
                        except:
                            pass

        # cv2.imwrite("/home/rhys/depth_result.png", depthres)
        return depthres

    def to_vector(self):
        try:
            depth_left = self._dict['depth_left']
            depth_front = self._dict['depth_front']
            depth_right = self._dict['depth_right']

            # rgb_left = self._dict["rgb_left"]
            # rgb_right = self._dict["rgb_right"]
            # rgb_front = self._dict["rgb_front"]

            # # Capture image   
            # size = (224,244)
            # # Prepare image for network
            # depth_left = cv2.resize(depth_left, dsize=size)
            # depth_front = cv2.resize(depth_front, dsize=size)
            # depth_right = cv2.resize(depth_right, dsize=size)

            # rgb_left = cv2.resize(rgb_left, dsize=size)
            # rgb_right = cv2.resize(rgb_right, dsize=size)
            # rgb_front = cv2.resize(rgb_front, dsize=size)

            # depth_left = self.get_segmented(rgb_left*255, depth_left * 255) / 255
            # depth_right = self.get_segmented(rgb_right*255, depth_right * 255) / 255
            # depth_front = self.get_segmented(rgb_front*255, depth_front * 255) / 255

            size = (64,64)
            # Prepare image for network
            depth_left = cv2.resize(depth_left, dsize=size)
            depth_front = cv2.resize(depth_front, dsize=size)
            depth_right = cv2.resize(depth_right, dsize=size)


            x = np.stack((depth_left, depth_right, depth_front), axis=0)
            x = x*2 - 0.5


            # import os
            # path = os.path.realpath('/home/kerry/test/')
            # n = int(len(os.listdir(os.path.realpath(path))))
            # cv2.imwrite('/home/kerry/test/'+ str(n) +'depth.png', np.transpose((x+0.5)*255, (1,2,0))) 

            placements = []
            for placement in self._dict['placements']:
                if not placement['distance_score'] == None:
                    placements.append(placement)
            if placements:
                placements = sorted(placements, key = lambda i: i['distance_score']) 
                quat = [placements[0]['w_quat'], placements[0]['x_quat'], placements[0]['y_quat'], placements[0]['z_quat']]
                r = Rotation.from_quat(quat)
                y = r.as_matrix()
                # e = r.as_euler('xyz', degrees=False)
                # y = [math.sin(e[0]), math.cos(e[0]), math.sin(e[1]), math.cos(e[1])]
                # y = [placements[0]['w_quat'], placements[0]['x_quat'], placements[0]['y_quat'], placements[0]['z_quat']]
                return x, y
            else:
                return None, None
        except:
            return None,None
        

    def to_vector_all(self, best=True, load_number=None):
        x_vec, y_vec = [], []
        n = len(os.listdir(self.dir_path))
        
        for i, f in enumerate(os.listdir(self.dir_path)):
            file_name = str(f)
            # print(file_name)
            self.import_file(file_name)

            x, y = self.to_vector()

            if x is not None and y is not None: 
                x_vec.append(x)
                y_vec.append(y)
        
            if  i%100 == 0:
                print('Importing {}/{}'.format(i, n))

            # if load_number:
            #     if i > load_number:
            #         break
        print('Finished importing')
     
        return x_vec, y_vec

    def __len__(self):
        return len(self._dict['placements'])

def to_placement(position=[0,0,0], quaternion=[1,0,0,0], distance_score=0, angle_score=0, stable=False):
        z = position[2]

        placement = {'z_height': z,
                'w_quat': quaternion[0],
                'x_quat': quaternion[1],
                'y_quat': quaternion[2],
                'z_quat': quaternion[3],
                'distance_score': distance_score,
                'angle_score': angle_score,
                'stable': stable}

        return placement

    

 