import torch, torchvision
from torch import nn
from torch.autograd import Variable
from torch.utils import data
from torch.utils.data import Dataset
from torchvision.models.resnet import model_urls

# model_urls['densenet121'] = model_urls['densenet121'].replace('https://', 'http://')
model_urls['resnet50'] = model_urls['resnet50'].replace('https://', 'http://')

import numpy as np

from .training_data import TrainingData
from .ortho6d import compute_rotation_matrix_from_ortho6d, compute_geodesic_distance_from_two_matrices, compute_rotation_matrix_from_euler_sin_cos

from sklearn.model_selection import train_test_split
from scipy.spatial.transform import Rotation
import matplotlib.pyplot as plt
import random
import wandb
import os 
import gc
import datetime
import math

import h5py

wandb.init()
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


# import linecache
# import tracemalloc

# def display_top(snapshot, key_type='lineno', limit=10):
#     snapshot = snapshot.filter_traces((
#         tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
#         tracemalloc.Filter(False, "<unknown>"),
#     ))
#     top_stats = snapshot.statistics(key_type)

#     print("Top %s lines" % limit)
#     for index, stat in enumerate(top_stats[:limit], 1):
#         frame = stat.traceback[0]
#         # replace "/path/to/module/file.py" with "module/file.py"
#         filename = os.sep.join(frame.filename.split(os.sep)[-2:])
#         print("#%s: %s:%s: %.1f KiB"
#               % (index, filename, frame.lineno, stat.size / 1024))
#         line = linecache.getline(frame.filename, frame.lineno).strip()
#         if line:
#             print('    %s' % line)

#     other = top_stats[limit:]
#     if other:
#         size = sum(stat.size for stat in other)
#         print("%s other: %.1f KiB" % (len(other), size / 1024))
#     total = sum(stat.size for stat in top_stats)
#     print("Total allocated size: %.1f KiB" % (total / 1024))


# tracemalloc.start()


class CustomDataset(Dataset):
    def __init__(self, data_root):
        dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + data_root
        self.samples = list(os.listdir(dir_path))

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):
        file_name = str(self.samples[idx])

        td = TrainingData()
        td.import_file(file_name)

        x, y = td.to_vector()
        x, y = x.astype('float32'), y.astype('float32')
        x, y = torch.from_numpy(x), torch.from_numpy(y)
        x, y = Variable(x), Variable(y)

        x1 = torch.unsqueeze(x[0], 0)
        x2 = torch.unsqueeze(x[1], 0)
        x3 = torch.unsqueeze(x[2], 0)

        print(y.shape)

        return x1, x2, x3, y


class HDF5Dataset(Dataset):
    
    def __init__(self, h5_path):
        path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + h5_path
        self.dataset = h5py.File(path, "r")
        self.length = len(self.dataset['depth_left']) 

    def __getitem__(self, index): #to enable indexing
        x1 = Variable(torch.from_numpy(self.dataset['depth_left'][index]))
        x2 = Variable(torch.from_numpy(self.dataset['depth_right'][index]))
        x3 = Variable(torch.from_numpy(self.dataset['depth_front'][index]))
        # x4 = Variable(torch.from_numpy(self.dataset['depth_back'][index]))

        y = Variable(torch.from_numpy(Rotation.from_quat(self.dataset['quaternion'][index]).as_matrix())).type(torch.float32)

        return x1, x2, x3, y

    def __len__(self):
        return self.length


class PCNN(nn.Module):
    def __init__(self):
        super().__init__()
        depth_trunk = torchvision.models.densenet121(pretrained=True)
        # self.depth_trunk = nn.Sequential(*list(depth_trunk.children())[:-2])
        self.densenet121 = nn.Sequential(*list(depth_trunk.children())[0])
        csize = self.densenet121(torch.zeros(1,3,64,64)).flatten().shape[0]
        self.linear_layers = nn.Sequential(
            nn.Linear(csize, 248),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(248, 6),
            nn.Tanh()
        )

    def forward(self, x):
        # x = torch.cat(3*[x], dim=1)
        # print(x.shape)
        x = self.densenet121(x)
        x = x.view(x.shape[0],-1)
        x = self.linear_layers(x)
        x = compute_rotation_matrix_from_ortho6d(x)
        
        return x

class new_PCNN(nn.Module):
    def __init__(self):
        super().__init__()
        self.resnet = torchvision.models.resnet50(pretrained=True)
        self.resnet.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.resnet.fc = nn.Linear(in_features=2048, out_features=1024, bias=True)

        self.linear_layers = nn.Sequential(
            nn.Linear(in_features=3072, out_features=1024, bias=True),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(in_features=1024, out_features=6, bias=True),
            nn.Tanh()
        )


    def forward(self, x1, x2, x3):
        x1 = self.resnet(x1)
        x2 = self.resnet(x2)
        x3 = self.resnet(x3)
        # x4 = self.resnet(x4)

        x1 = x1.view(x1.size(0), -1)
        x2 = x2.view(x2.size(0), -1)
        x3 = x3.view(x3.size(0), -1)
        # x4 = x4.view(x4.size(0), -1)

        x = torch.cat((x1, x2, x3), dim=1)
        x = self.linear_layers(x)
        x = compute_rotation_matrix_from_ortho6d(x)

        return x

class stable_PCNN(nn.Module):
    def __init__(self):
        super().__init__()
        self.resnet = torchvision.models.resnet50(pretrained=True)
        self.resnet.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.resnet.fc = nn.Linear(in_features=2048, out_features=1024, bias=True)

        self.linear_layers = nn.Sequential(
            nn.Linear(in_features=3072, out_features=1024, bias=True),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(in_features=1024, out_features=1, bias=True),
            nn.Sigmoid()
        )


    def forward(self, x1, x2, x3):
        x1 = self.resnet(x1)
        x2 = self.resnet(x2)
        x3 = self.resnet(x3)
        # x4 = self.resnet(x4)

        x1 = x1.view(x1.size(0), -1)
        x2 = x2.view(x2.size(0), -1)
        x3 = x3.view(x3.size(0), -1)
        # x4 = x4.view(x4.size(0), -1)

        x = torch.cat((x1, x2, x3), dim=1)
        x = self.linear_layers(x)
        # x = compute_rotation_matrix_from_ortho6d(x)

        return x


def weights_init_uniform(m):
    classname = m.__class__.__name__
    # for every Linear layer in a model..
    if classname.find('Linear') != -1:
        # apply a uniform distribution to the weights and a bias=0
        m.weight.data.uniform_(0.0, 1.0)
        m.bias.data.fill_(0)
        
class PlacementNetwork:
    def __init__(self, model_path=None, stable_network = False):
        self.network = stable_PCNN() if stable_network else new_PCNN()
        # wandb.watch(self.network)
        #self.network.apply(weights_init_uniform)
        self.optimizer = torch.optim.Adam(self.network.parameters(), lr=1e-5, weight_decay=2e-2)
        self.loss_function = nn.BCELoss() if stable_network else nn.SmoothL1Loss()
        # self.loss_function = self.compute_geodesic_loss()
        
        self.x1_train = [] # Input
        self.x2_train = [] # Input
        self.x3_train = [] # Input
        self.y_train = [] # Output

        self.x1_val = [] # Input
        self.x2_val = [] # Input
        self.x3_val = [] # Input
        self.y_val = [] # Output
        
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        self.epoch = 0

        self.train_losses = []
        self.val_losses = []

        self.model_path = model_path

        if torch.cuda.is_available():
            print('CUDA is available')
            self.network = self.network.cuda()
            self.loss_function = self.loss_function.cuda()

    def compute_geodesic_loss(self, gt_r_matrix, out_r_matrix):
        theta = compute_geodesic_distance_from_two_matrices(gt_r_matrix, out_r_matrix)
        error = theta.mean()
        return error

    def get_training_data(self):
        path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + '/data/dataset_gripper_1.hdf5'
        dataset = h5py.File(path, "r")
        length = len(dataset['depth_left']) 
        self.x1_train = np.zeros((length, 1, 64, 64))
        self.x2_train = np.zeros((length, 1, 64, 64))
        self.x3_train = np.zeros((length, 1, 64, 64))
        self.y_train = np.zeros((length, 3, 3))

        for index in range(length):
            self.x1_train[index] = dataset['depth_left'][index]
            self.x2_train[index] = dataset['depth_right'][index]
            self.x3_train[index] = dataset['depth_front'][index]

            self.y_train[index] = Rotation.from_quat(dataset['quaternion'][index]).as_matrix()

            if index%1000 == 0:
                print("Loaded ", index, " / ", length)

        self.x1_train = Variable(torch.from_numpy(self.x1_train))
        self.x2_train = Variable(torch.from_numpy(self.x2_train))
        self.x3_train = Variable(torch.from_numpy(self.x3_train))
        self.y_train = Variable(torch.from_numpy(self.y_train))

        dataset.close()


        # td = TrainingData()
        # x, y = td.to_vector_all()
        # x, y = np.asarray(x), np.asarray(y)
        # x, y = x.astype('float32'), y.astype('float32')

        # self.x = torch.from_numpy(x)
        # self.y = torch.from_numpy(y)

        # x_train, x_val, y_train, y_val = train_test_split(x, y, test_size = 0.00025)
        # x_train, x_val = torch.from_numpy(x_train), torch.from_numpy(x_val)
        # y_train, y_val = torch.from_numpy(y_train), torch.from_numpy(y_val)
        # x_train, x_val = Variable(x_train), Variable(x_val)
        # self.y_train, self.y_val = Variable(y_train), Variable(y_val)

        # self.x1_train = torch.unsqueeze(x_train[:, 0], 1)
        # self.x2_train = torch.unsqueeze(x_train[:, 1], 1)
        # self.x3_train = torch.unsqueeze(x_train[:, 2], 1)

        # self.x1_val = torch.unsqueeze(x_val[:, 0], 1)
        # self.x2_val = torch.unsqueeze(x_val[:, 1], 1)
        # self.x3_val = torch.unsqueeze(x_val[:, 2], 1)

        # self.x1_val = self.x1_val.to(self.device)
        # self.x2_val = self.x2_val.to(self.device)
        # self.x3_val = self.x3_val.to(self.device)
        # self.y_val = self.y_val.to(self.device)

    def train_step(self, x1, x2, x3, y):
        self.optimizer.zero_grad()

        output_train = self.network(x1, x2, x3)
        # output_val = self.network(self.x1_val, self.x2_val, self.x3_val)

        loss_train = self.compute_geodesic_loss(output_train, y)
        # loss_val = self.compute_geodesic_loss(output_val, self.y_val)
        # self.train_losses.append(loss_train)
        # self.val_losses.append(loss_val.cpu().detach().item())

        loss_train.backward()
        self.optimizer.step()

        gc.collect()

        return loss_train.cpu().detach().item(), 0.0

    def train(self, n_epoch=1000, save=True):
        self.network.train()
        # dataset = data.TensorDataset(self.x1_train, self.x2_train, self.x3_train, self.y_train)
        # dataset = CustomDataset('/data/training_data')
        dataset = HDF5Dataset('/data/dataset_gripper_1.hdf5')

        dataloader = data.DataLoader(dataset, batch_size=256, shuffle=True)

        for epoch in range(n_epoch):
            tic = datetime.datetime.now()
            loss_train_total = 0
            loss_val_total = 0
            for batch, sample in enumerate(dataloader):
                x1 = sample[0].to(self.device)
                x2 = sample[1].to(self.device)
                x3 = sample[2].to(self.device)
                # x4 = sample[3].to(self.device)
                y = sample[3].to(self.device)
                loss_train, loss_val = self.train_step(x1, x2, x3, y)

                loss_train_total += loss_train
                loss_val_total += loss_val

                if batch%50 == 0:
                    print('Epoch : ',self.epoch+1, '\t', 'Batch : ',batch+1, '/', len(dataloader), '\t', 'loss_train :', loss_train_total / (batch + 1))

            if epoch%25 == 0:
                if save:
                    checkpoint = {
                        'epoch': self.epoch + 1,
                        'state_dict': self.network.state_dict(),
                        'optimizer': self.optimizer.state_dict()
                    }
                    torch.save(checkpoint, self.model_path + str(self.epoch) + '.pt')
            
            loss_train_average = loss_train_total / (batch + 1)
            loss_val_average = loss_val_total / (batch + 1)

            wandb.log({'train_loss': loss_train_average}, step=self.epoch)
            wandb.log({'val_loss': loss_val_average}, step=self.epoch)
            toc = datetime.datetime.now()
            print('Epoch : ',self.epoch+1, '\t', 'Batch : ',batch+1, '\t', 'loss_train :', loss_train_average, '\t', 'Time elapsed :', toc - tic)
            # snapshot = tracemalloc.take_snapshot()
            # display_top(snapshot)

            self.epoch += 1

        del dataset
        del dataloader

    def import_model(self, path):
        checkpoint = torch.load(path)

        if type(checkpoint) is dict:
            self.network.load_state_dict(checkpoint['state_dict'])
            self.optimizer.load_state_dict(checkpoint['optimizer'])
            self.epoch = checkpoint['epoch']
        else:
            self.network.load_state_dict(checkpoint)
        
        self.network.eval()
