import jpype
import jpype.imports

import numpy as np

from pathlib import Path


class ReebGraph:
    def __init__(self, jsrc_path):
        """
        :param jsrc_path: (str) Path that contains compiled reeb_graph java project
                                (https://github.com/dbespalov/reeb_graph)
        """
        if not jpype.isJVMStarted():
            jpype.startJVM(classpath=[jsrc_path], convertStrings=True)
        elif not jpype.isThreadAttachedToJVM():
            jpype.attachThreadToJVM()

        # These imports are activated by jpype after starting the JVM
        from java.lang import System
        from java.io import PrintStream, File
        # Disable java output.
        System.setOut(PrintStream(File('/dev/null')))  # NUL for windows, /dev/null for unix

        self.erg = jpype.JClass('ExtractReebGraph')()
        self.crg = jpype.JClass('CompareReebGraph')()

        # Set defaults
        self.params = ['4000', '0.005', str(2 ** 7), '0.5']
        self.erg.main(self.params[:3])
        self.crg.main(self.params)
        try:
            (Path.cwd() / 'log_{}_{}_{}_{}'.format(*self.params)).unlink()
        except FileNotFoundError:
            pass

    def extract_reeb_graph(self, mesh_file):
        """
        Extract the reeb graph for a .wrl file.
        Creates a .mrg file with the same name.
        This only needs to be run once per .wrl.
        :param mesh_file: *.wrl mesh from which to extract reeb graph.
        """
        self.erg.main_one(mesh_file)

    def compare_reeb_graph(self, mesh_files):
        mat = np.zeros((len(mesh_files), len(mesh_files)))
        for idx1, mf1 in enumerate(mesh_files[:-1]):
            for idx2, mf2 in enumerate(mesh_files[idx1:]):
                self.crg.main_one([mf1, mf2])
                print(mf1.split('/')[-1], mf2.split('/')[-1], '{:0.3f}'.format(self.crg.SIM_R_S))
                v = round(self.crg.SIM_R_S, 3)
                # v = round(max((v - 0.5), 0)/0.5, 3)
                mat[idx1, idx1+idx2] = v
                mat[idx1+idx2, idx1] = v
        return mat

    def compare_two_reeb_graph(self, mf1, mf2):
        self.crg.main_one([mf1, mf2])
        # print(mf1.split('/')[-1], mf2.split('/')[-1], '{:0.3f}'.format(self.crg.SIM_R_S))
        v = round(self.crg.SIM_R_S, 3)
        return v

    @staticmethod
    def trimesh_to_vrml(mesh, output_file):
        """
        Export a trimesh.Trimesh to a VRML file.
        The reeb_graph project requires these in a specifc format.
        TODO: Link this into the trimesh export handlers.
        TODO: Fix the java to be less bad.
        :param mesh: trimesh.Trimesh
        :param output_file: *.wrl output file.
        """

        with open(output_file, 'w') as f:
            f.write('#VRML V2.0 utf8\n')
            f.write('Transform {\n'
                    '   children [\n'
                    '      Shape {\n'
                    '         geometry IndexedFaceSet {\n'
                    '            coord Coordinate {\n'
                    '               point[\n')
            for v in mesh.vertices:
                f.write('                  {:0.6f} {:0.6f} {:0.6f},\n'.format(*v.tolist()))
            f.write('               ] # end of points\n'
                    '            } # end of Coordinate\n'
                    '            coordIndex [\n')

            for face in mesh.faces:
                f.write('               {:d}, {:d}, {:d}, -1,\n'.format(*face.tolist()))

            f.write('            ] # end of coordIndex\n'
                    '         } # end of geometry\n'
                    '         appearance Appearance {\n'
                    '            material Material {\n'
                    '               diffuseColor 0.7 0.7 0.7\n'
                    '               emissiveColor 0.05 0.05 0.05\n'
                    '               specularColor 1.0 1.0 1.0\n'
                    '               ambientIntensity 0.2\n'
                    '               shininess 0.2\n'
                    '               transparency 0.0\n'
                    '            } #end of material\n'
                    '         } #end of appearance\n'
                    '      } # end of Shape\n'
                    '   ] # end of children\n'
                    '}\n')


if __name__ == '__main__':
    import umap
    import matplotlib.pyplot as plt
    from matplotlib.offsetbox import OffsetImage, AnnotationBbox
    import trimesh
    import imageio
    from .viz import get_mesh_image
    from .mesh import decompose_mesh

    p = Path('/mnt/storage/gwi_output/learn_easiest/1571958966/')
    meshes = sorted(p.glob('generation_0[123456789][02468]/*.stl'))

    mesh_imgs = []
    wrl_meshes = []
    genomes = set()
    graspabilities = []
    rg = ReebGraph('/home/doug/dev/reeb/reeb_graph/src')

    force_regen = False  # Whether to run all the extraction stuff again.
    regen_reeb = False  # Whether the reeb similarity needs to be re-run

    for stl_file in meshes:
        if str(stl_file).endswith('.decomp.stl'):
            continue
        print(str(stl_file))
        # Get unique list of meshes.
        sc, gn = str(stl_file.stem).split('_')
        if gn in genomes:
            continue
        genomes.add(gn)
        graspabilities.append(float(sc))

        decomposed_file = str(stl_file).replace('.stl', '.decomp.stl')

        wrl_file = str(decomposed_file).replace('.stl', '.wrl')
        thumbnail_file = str(stl_file).replace('.stl', '.png')
        wrl_meshes.append(wrl_file)

        m = None

        if not Path(decomposed_file).exists():
            regen_reeb = True
            m = trimesh.load_mesh(stl_file)
            d = decompose_mesh(m, maxhulls=20)
            if not isinstance(d, list):
                m = d
            else:
                m = d[0].union(d[1:])
            m.export(decomposed_file)

        if not Path(wrl_file).exists() or not Path(thumbnail_file).exists() or force_regen:
            regen_reeb = True
            if m is None:
                m = trimesh.load_mesh(decomposed_file)

        if Path(thumbnail_file).exists() and not force_regen:
            mesh_imgs.append(imageio.imread(thumbnail_file))
        else:
            im = get_mesh_image(m, resolution=(64, 64))
            mesh_imgs.append(im)
            imageio.imsave(thumbnail_file, im)

        if not Path(wrl_file).exists() or force_regen:
            regen_reeb = True
            ReebGraph.trimesh_to_vrml(m, wrl_file)
            rg.extract_reeb_graph(wrl_file)

    mat_file = Path(__file__).absolute().parent / 'data/reeb_comparison.npy'
    if force_regen or regen_reeb or not mat_file.exists():
        mat = rg.compare_reeb_graph(wrl_meshes)
        np.save(mat_file, mat)
    else:
        mat = np.load(mat_file)

    print(mat.shape)

    # mat -= 0.5

    similarity = []
    for r in mat:
        # similarity.append((1/r.mean()))
        sims = np.argsort(1 - r)
        similarity.append((1-r[sims[1:2]]).mean())
    similarity = np.array(similarity)
    #similarity = (similarity - similarity.min())/(similarity.max()-similarity.min())

    # mas = np.argsort(mat, axis=1)
    # mat *= 0
    # for rr, r in enumerate(mas):
    #     for cc, c in enumerate(r[:10]):
    #         mat[rr, c] = cc

    # for rr, r in enumerate(mat):
    #     sims = np.argsort(1 - r)
    #     v10 = max(r[sims[10]], 0.75)
    #     r[r <= v10] = 0
    #     c = mat[:, rr]
    #     c[c < v10] = 0

    # km = KMeans(n_clusters=20, random_state=0).fit(mat)

    reducer = umap.UMAP(metric='euclidean', n_components=2, target_metric='l2', target_weight=0.2, min_dist=0.9, n_neighbors=200)
    embedding = reducer.fit_transform(mat)#, np.array(graspabilities))
    # plt.scatter(embedding[:, 0], embedding[:, 1])
    # plt.gca().set_aspect('equal', 'datalim')
    # plt.show()

    # grid = []
    # idxs = np.random.randint(0, len(mat), 10)
    # for idx, sim in zip(idxs, mat[idxs]):
    #     qimg = [mesh_imgs[idx]]
    #     sims = np.argsort(1 - sim)
    #     for idy, n in enumerate(sims[1:11]):
    #         if sim[n] > 0.75:
    #             qimg.append(mesh_imgs[n])
    #         else:
    #             qimg.append(mesh_imgs[n]*0)
    #     grid.append(np.concatenate(qimg, axis=1))
    # grid = np.concatenate(grid, axis=0)
    # plt.imshow(grid)
    # plt.show()

    cm = plt.get_cmap('YlOrRd')

    fig = plt.figure(figsize=(15, 15))
    ax = fig.add_subplot(111)
    ax.scatter(embedding[:, 0], embedding[:, 1], s=0.001)
    for x, y, im, l in zip(embedding[:, 0], embedding[:, 1], mesh_imgs, similarity):
        col = (np.array(cm(l)) * 255).astype(np.uint8)
        im[:3, :] = col
        im[-3:, :] = col
        im[:, :3] = col
        im[:, -3:] = col
        im[:, :, 3] = 255
        ab = AnnotationBbox(OffsetImage(im, zoom=0.5), (x, y), frameon=False, bboxprops={'edgecolor': cm(l)})
        ax.add_artist(ab)
    ax.set_aspect('equal')
    ax.axis('off')
    plt.tight_layout()

    # plt.savefig('/home/doug/out.pdf')
    plt.savefig('/home/doug/reeb_test/000.png')
    plt.close()
    # plt.show()

    embedding_og = embedding.copy()

    for idx in range(1, len(mat)-1):
        print(idx)
        least_diverse = np.argmin(similarity)

        mat = np.delete(mat, least_diverse, 0)
        mat = np.delete(mat, least_diverse, 1)

        similarity = np.delete(similarity, least_diverse, 0)

        embedding = np.delete(embedding, least_diverse, 0)

        del mesh_imgs[least_diverse]

        fig = plt.figure(figsize=(15, 15))
        ax = fig.add_subplot(111)
        ax.scatter(embedding_og[:, 0], embedding_og[:, 1], s=0.001)
        for x, y, im, l in zip(embedding[:, 0], embedding[:, 1], mesh_imgs, similarity):
            col = (np.array(cm(l)) * 255).astype(np.uint8)
            im[:3, :] = col
            im[-3:, :] = col
            im[:, :3] = col
            im[:, -3:] = col
            im[:, :, 3] = 255
            ab = AnnotationBbox(OffsetImage(im, zoom=0.5), (x, y), frameon=False, bboxprops={'edgecolor': cm(l)})
            ax.add_artist(ab)
        ax.set_aspect('equal')
        ax.axis('off')
        plt.tight_layout()

        # plt.savefig('/home/doug/out.pdf')
        plt.savefig('/home/doug/reeb_test/knn_1_noscale/{:03d}.png'.format(idx))
        plt.close()
        # plt.show()

        similarity = []
        for r in mat:
            # similarity.append((1/r.mean()))
            sims = np.argsort(1 - r)
            similarity.append((1-r[sims[1:2]]).mean())
        similarity = np.array(similarity)
        #similarity = (similarity - similarity.min()) / (similarity.max() - similarity.min())
