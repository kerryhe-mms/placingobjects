"""
Robots

Author: Doug Morrison
"""

import numpy as np

from pyrep.robots.end_effectors.gripper import POSITION_ERROR
from pyrep.robots.arms.panda import Panda
from pyrep.robots.end_effectors.panda_gripper import PandaGripper
from pyrep.const import ConfigurationPathAlgorithms
from pyrep.errors import ConfigurationError

from scipy.spatial.transform import Rotation


class IKGraspFilter:
    def __init__(self, robot, max_checks=None):
        self.robot = robot
        self.max_checks = max_checks

    def __call__(self, grasps, max_n=None):
        kept_grasps = []
        checks = 0
        for g in grasps:
            pg = g.to_physical_grasp()
            if self.robot.check_ik_and_collision(pg.centre, quaternion=pg.get_quaternion(), ignore_collisions=False):
                kept_grasps.append(g)
            if max_n is not None and len(kept_grasps) == max_n:
                break
            checks += 1
            if self.max_checks is not None and checks > self.max_checks:
                break
        return kept_grasps


class Robot:
    """
    Robot Base Class
    """
    pass


class ExtendedPandaGripper(PandaGripper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def actuate_force(self, force, velocity=0.1):
        self.set_joint_forces([force, force])
        self.set_joint_target_velocities([-velocity, -velocity])

        current_positions = self.get_joint_positions()
        current_forces = self.get_joint_forces()

        done = True
        for i, (j, curr, prev, f) in enumerate(zip(
                self.joints, current_positions,
                self._prev_positions, current_forces)):

            not_moving = (prev is not None and
                          np.fabs(curr - prev) < POSITION_ERROR/1000)

            force_target = f >= force

            if force_target:
                continue
            done = False
        self._prev_positions = current_positions
        if done:
            self._prev_positions = [None] * self._num_joints
        return done


class PyRepRobot:
    def __init__(self, robot, gripper):
        self.robot = robot
        self.gripper = gripper
        self.og_joints = robot.get_joint_positions()
        self.og_gripper_joints = gripper.get_joint_positions()
        self.og_configtree = robot.get_configuration_tree()
        self.planner = ConfigurationPathAlgorithms.RRTConnect

        self.position_history = []
        self.robot.set_collidable(True)
        self.gripper.set_collidable(True)

    def reset(self, pr):
        self.robot.set_joint_positions(self.og_joints)
        self.gripper.set_joint_positions(self.og_gripper_joints)
        self.gripper.set_joint_target_velocities([0.0, 0.0])
        pr.set_configuration_tree(self.og_configtree)

    def align_grasp_to_gripper(self, grasp):
        """ Keep the gripper rotation within +/- pi/2.

        :param grasp:
        :return:
        """
        #TODO
        pass

    def check_ik_and_collision(self, position, euler=None, quaternion=None, ignore_collisions=False):
        """
        Check whether a valid ik solution can be found for the given pose.
        :param position:
        :param euler:
        :param quaternion:
        :param ignore_collisions:
        :return:
        """
        try:
            self.robot.get_configs_for_tip_pose(position, euler=euler, quaternion=quaternion,
                                                ignore_collisions=ignore_collisions,
                                                trials=1, max_configs=1)
            return True
        except ConfigurationError:
            return False

    def grasp_procedure(self, grasp, dt):
        """  Attempt a grasp.  Yield status throughout, e.g. to step the sim.
             Assumes that the grasp has already been checked collision free.
        :param grasp:
        :return:
        """
        PREGRASP_OFFSET = 0.10

        p = grasp.centre
        q = grasp.get_quaternion()
        p_pregrasp = list(p)
        p_pregrasp[2] += PREGRASP_OFFSET
        p_postgrasp = list(p_pregrasp)
        p_postgrasp[2] += 0.3

        # Set gripper width
        # while not self.gripper.actuate(1.0, 0.1):
        #     yield False

        # Move directly to pre-grasp pose
        pos = self.robot.get_configs_for_tip_pose(p_pregrasp, quaternion=q,
                                                  ignore_collisions=True,
                                                  trials=1, max_configs=1)[0]
        self.robot.set_joint_positions(pos)
        yield False

        # Move to grasp pose.
        path = self.robot.get_linear_path(position=p, quaternion=q, ignore_collisions=True)
        while not path.step():
            yield False

        # path.set_to_end()
        # try:
        #     pos = self.robot.get_configs_for_tip_pose(p_pregrasp, quaternion=q,
        #                                         ignore_collisions=False,
        #                                         trials=1, max_configs=1)[0]
        #     self.robot.set_joint_positions(pos)
        # except ConfigurationError:
        #     print("Failed to get solution")
        #     raise
        # yield False
        #
        # # Move to grasp
        # n_steps = int(0.2/dt)
        # delta = PREGRASP_OFFSET / n_steps
        # p_desired = list(p_pregrasp)
        # for _ in range(n_steps):
        #     p_desired[2] -= delta
        #     new_joint_angles = self.robot.solve_ik(position=p_desired, quaternion=q)
        #     self.robot.set_joint_target_positions(new_joint_angles)
        #     yield False
        #     curr_pos = self.robot.get_tip().get_position()
        #     pos_delta = np.linalg.norm(np.array(curr_pos)[:2] - np.array(p_desired)[:2])
        #     z_delta = abs(curr_pos[2] - p_desired[2])
        #     if pos_delta > 0.005 or z_delta > 0.01:
        #         # Check if the physics engine is pushing us off course == collision
        #         # Seems to be more reliable than collision checking
        #         break

        # Grasp
        steps = 0
        while not self.gripper.actuate_force(50) and steps < (2.0 / dt):
            steps += 1
            yield False

        # Lift
        q = self.robot.get_tip().get_quaternion()
        path = self.robot.get_linear_path(position=p_postgrasp, quaternion=q, ignore_collisions=True) #, algorithm=self.planner)
        while not path.step():
            yield False

        while True:
            # Yield True while done.
            yield True

    def placement_procedure(self, center, quaternion, object_handle):
        """  Attempt a grasp.  Yield status throughout, e.g. to step the sim.
             Assumes that the grasp has already been checked collision free.
        :param grasp:
        :return:
        """
        PREGRASP_OFFSET = 0.30

        p = center
        q = quaternion
        p_pregrasp = list(p)
        p_pregrasp[2] += PREGRASP_OFFSET
        p_postgrasp = list(p_pregrasp)

        # Set gripper width
        # while not self.gripper.actuate(1.0, 0.1):
        #     yield False

        # Move to grasp pose.
        path = self.robot.get_path(position=p_pregrasp, quaternion=q, ignore_collisions=True)
        while not path.step():
            yield False

        # Move to grasp pose.
        path = self.robot.get_linear_path(position=p, quaternion=q, ignore_collisions=True)
        while not path.step():
            yield False

        dist_obj_from_tip = np.linalg.norm(np.asarray(object_handle.get_position()) - np.asarray(self.robot.get_tip().get_position()))
        if dist_obj_from_tip > 0.1:
            raise ValueError('Object not grasped anymore: ', dist_obj_from_tip)

        # Release
        while not self.gripper.actuate(1.0, 0.1):
            self.gripper.release()
            self.position_history.append(object_handle.get_position())
            yield False

        # Lift
        q = self.robot.get_tip().get_quaternion()
        path = self.robot.get_linear_path(position=p_postgrasp, quaternion=q, ignore_collisions=True) #, algorithm=self.planner)
        while not path.step():
            self.position_history.append(object_handle.get_position())
            yield False

        for _ in range(100):
            self.position_history.append(object_handle.get_position())

        while True:
            # Yield True while done.
            self.position_history.append(object_handle.get_position())
            yield True

    def get_score(self):
        score = 0
        for p1, p2 in zip(self.position_history[:-1], self.position_history[1:]):
            score += np.linalg.norm([x - y for x, y in zip(p1, p2)])

        self.position_history = []
        return score


class PyRepPanda(PyRepRobot):
    def __init__(self):
        super().__init__(Panda(), ExtendedPandaGripper())


class ROSRobot:
    pass
