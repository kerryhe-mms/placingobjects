import cv2
import os
import numpy as np

import pickle
import h5py

size = (64,64)

# Get list of all training data
dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/stable"
data_list = list(os.listdir(dir_path))

objects = ["Skillet","CookieJar","Rocket","Juice","Klein","RedCup","MilkBottle","FlowerPot","HygieneSpray","PharmaceuticalBottle","Basket","ShotGlass","Martini","Milk","Mortar","CokePlasticSmall","Wineglass","ChessPawn","SmallMilk","Sprudelflasche","Salt","SaladPlate","Pot","DinnerPlate","SoapDispenser","SoupBowl","Bowl","BathDetergent","TeaCup","Pepper","Spray","Superglue","WhiteCup","Oscar","Waterglass","BabyBottle","PlasticCup","Vasepot","FlowerCup","Globe","HamburgerSauce","CokePlasticLarge","Trophycup","ChessKing","CokePlasticSmallGrasp","TrophySphere","GreenCup","FoamCup","CoffeeCup","Glassbowl"]

test_objects = ["Globe","Juice","PlasticCup","Rocket","Spray"]


# Initialise HDF5 file
file_name_train = h5py.File("dataset_stable_train_4.hdf5", "w")

dataset_left_train = file_name_train.create_dataset("depth_left", (998 * 45 * 2, 1, size[0], size[1]), dtype=np.float32)
dataset_front_train = file_name_train.create_dataset("depth_front", (998 * 45 * 2, 1, size[0], size[1]), dtype=np.float32)
dataset_right_train = file_name_train.create_dataset("depth_right", (998 * 45 * 2, 1, size[0], size[1]), dtype=np.float32)
dataset_stable_train = file_name_train.create_dataset("is_stable", (998 * 45 * 2, 1), dtype=bool)

# dataset_quaternion_train = file_name_train.create_dataset("quaternion", (199 * 45 * 2, 4), dtype=np.float32)


file_name_test = h5py.File("dataset_stable_test_4.hdf5", "w")

dataset_left_test = file_name_test.create_dataset("depth_left", (998 * 5 * 2, 1, size[0], size[1]), dtype=np.float32)
dataset_front_test = file_name_test.create_dataset("depth_front", (998 * 5 * 2, 1, size[0], size[1]), dtype=np.float32)
dataset_right_test = file_name_test.create_dataset("depth_right", (998 * 5 * 2, 1, size[0], size[1]), dtype=np.float32)
dataset_stable_test = file_name_test.create_dataset("is_stable", (998 * 5 * 2, 1), dtype=bool)


train_i = 0
test_i = 0
# dataset_quaternion_test = file_name_test.create_dataset("quaternion", (199 * 5 * 2, 4), dtype=np.float32)

# Iterate through all training data
for i, f in enumerate(data_list):

    # print(f)
    obj_idex = int(f.split("_")[0])
    # print(objects[obj_idex])

    test = objects[obj_idex] in test_objects


    pkl = open(dir_path + "/" + f, 'rb')
    dic = pickle.load(pkl)

    # print(dic["placements"])



    # Unpack pickle file
    depth_left = dic['depth_left']
    depth_front = dic['depth_front']
    depth_right = dic['depth_right']

    # cv2.imshow("left", depth_left)
    # cv2.waitKey(0)

    # quat_dict = dic['placements'][0]
    # quaternion = [quat_dict['w_quat'], quat_dict['x_quat'], quat_dict['y_quat'], quat_dict['z_quat']]

    # Reshape image
    depth_left = np.expand_dims(cv2.resize(depth_left, dsize=size), 0) * 2 - 0.5
    depth_front = np.expand_dims(cv2.resize(depth_front, dsize=size), 0) * 2 - 0.5
    depth_right = np.expand_dims(cv2.resize(depth_right, dsize=size), 0) * 2 - 0.5

    # quaternion = np.array(quaternion).astype(np.float32)

    if test:

        # Output to file
        dataset_left_test[train_i, :] = depth_left
        dataset_front_test[train_i, :] = depth_front
        dataset_right_test[train_i, :] = depth_right
        # dataset_quaternion_test[i, :] = quaternion
        dataset_stable_test[train_i, :] = True
        train_i += 1
    else:

        # Output to file
        dataset_left_train[test_i, :] = depth_left
        dataset_front_train[test_i, :] = depth_front
        dataset_right_train[test_i, :] = depth_right
        # dataset_quaternion_train[i, :] = quaternion
        dataset_stable_train[test_i, :] = True
        test_i += 1

    if i%100 == 0:
        print("Imported ", i, " / ", len(data_list))

print(train_i, test_i)

dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/unstable"
data_list = list(os.listdir(dir_path))

for j, f in enumerate(data_list):

    # print(f)
    obj_idex = int(f.split("_")[0])
    # print(objects[obj_idex])

    test = objects[obj_idex] in test_objects


    pkl = open(dir_path + "/" + f, 'rb')
    dic = pickle.load(pkl)



    # Unpack pickle file
    depth_left = dic['depth_left']
    depth_front = dic['depth_front']
    depth_right = dic['depth_right']

    # cv2.imshow("left", depth_left)
    # cv2.waitKey(0)

    # quat_dict = dic['placements'][0]
    # quaternion = [quat_dict['w_quat'], quat_dict['x_quat'], quat_dict['y_quat'], quat_dict['z_quat']]

    # Reshape image
    depth_left = np.expand_dims(cv2.resize(depth_left, dsize=size), 0) * 2 - 0.5
    depth_front = np.expand_dims(cv2.resize(depth_front, dsize=size), 0) * 2 - 0.5
    depth_right = np.expand_dims(cv2.resize(depth_right, dsize=size), 0) * 2 - 0.5

    # quaternion = np.array(quaternion).astype(np.float32)

    if test:

        # Output to file
        dataset_left_test[train_i, :] = depth_left
        dataset_front_test[train_i, :] = depth_front
        dataset_right_test[train_i, :] = depth_right
        # dataset_quaternion_test[i, :] = quaternion
        dataset_stable_test[train_i, :] = False
        train_i += 1
    else:
        # Output to file
        dataset_left_train[test_i, :] = depth_left
        dataset_front_train[test_i, :] = depth_front
        dataset_right_train[test_i, :] = depth_right
        # dataset_quaternion_train[i, :] = quaternion
        dataset_stable_train[test_i, :] = False
        test_i += 1

    if (i+j)%100 == 0:
        print("Imported ", i+j, " / ", len(data_list))

print("Transfer complete!")

file_name_train.close()
file_name_test.close()