import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data
import torch.nn.functional as F
import torchvision
import gc
print("import")
import wandb

import numpy as np

import torchvision.models as models
from torchvision import transforms
from PIL import Image
import matplotlib.pyplot as plt
import h5py
import os
print("impor1")
from torch import nn
from torch.autograd import Variable
from torch.utils import data
from torch.utils.data import Dataset
from torchvision.models.resnet import model_urls
print("import2")
wandb.init()

model_urls['resnet34'] = model_urls['resnet34'].replace('https://', 'http://')
model_urls['resnet18'] = model_urls['resnet18'].replace('https://', 'http://')
print("import3")



class HDF5Dataset(Dataset):
    
    def __init__(self, h5_path):
        path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + h5_path
        self.dataset = h5py.File(path, "r")
        self.length = len(self.dataset['depth_left']) 

    def __getitem__(self, index): #to enable indexing
        x1 = Variable(torch.from_numpy(self.dataset['depth_left'][index]))
        x2 = Variable(torch.from_numpy(self.dataset['depth_right'][index]))
        x3 = Variable(torch.from_numpy(self.dataset['depth_front'][index]))
        # x4 = Variable(torch.from_numpy(self.dataset['depth_back'][index]))

        y = Variable(torch.from_numpy(self.dataset['is_stable'][index])).type(torch.float)

        return x1, x2, x3, y

    def __len__(self):
        return self.length

# model_resnet18 = torchvision.models.resnet18(pretrained=True)
# model_resnet34 = torchvision.models.resnet34(pretrained=True)
# # model_resnet18 = torch.hub.load('pytorch/vision', 'resnet18', pretrained=True)
# # model_resnet34 = torch.hub.load('pytorch/vision', 'resnet34', pretrained=True)

# for name, param in model_resnet18.named_parameters():
#     if("bn" not in name):
#         param.requires_grad = False
        
# for name, param in model_resnet34.named_parameters():
#     if("bn" not in name):
#         param.requires_grad = False


# num_classes = 2

# model_resnet18.fc = nn.Sequential(nn.Linear(model_resnet18.fc.in_features,512),
#                                   nn.ReLU(),
#                                   nn.Dropout(),
#                                   nn.Linear(512, num_classes))

# model_resnet34.fc = nn.Sequential(nn.Linear(model_resnet34.fc.in_features,512),
#                                   nn.ReLU(),
#                                   nn.Dropout(),
#                                   nn.Linear(512, num_classes))



def train(model, optimizer, loss_fn, train_loader, val_loader, epochs=5, device="cpu"):
    for epoch in range(epochs):
        training_loss = []
        valid_loss = 0.0
        # print(model)

        model.train()
        for index, batch in enumerate(train_loader):
            optimizer.zero_grad()
            # print(batch[0].shape)
            x, targets = batch

            get_channel = lambda index : torch.from_numpy(np.expand_dims(x[:, index, :, :], 1) - 0.5)

            x1 = get_channel(0)
            x2 = get_channel(1)
            x3 = get_channel(2)
            # inputs = inputs.to(device)
            x1 = x1.to(device)
            x2 = x2.to(device)
            x3 = x3.to(device)

            output = model(x1, x2, x3).squeeze()
            targets = targets.to(device)

            loss = loss_fn(output, targets.float())
            loss.backward()
            optimizer.step()
            
            # training_loss += loss.data.item() * x1.size(0)
            # print(len(training_loss))
            if len(training_loss) == 10:
                training_loss.pop(0)
                # print(training_loss)
                print(f"batch {index} out of {len(train_loader)} for a training loss {sum(training_loss) /(10 * x1.size(0))}")
                # wandb.log({"training_loss": sum(training_loss)/(10 * x1.size(0))})

            training_loss.append(loss.data.item() * x1.size(0))
            # print(loss.data.item() * x1.size(0))
            # break
        gc.collect()
        # training_loss /= len(train_loader.dataset)

        # model.train(False)
 
        model.eval()
        num_correct = 0 
        num_examples = 0
        for batch in val_loader:
            # inputs, targets = batch
            x, targets = batch

            # print(batch[0].shape)
            x, targets = batch

            get_channel = lambda index : torch.from_numpy(np.expand_dims(x[:, index, :, :], 1) - 0.5)


            x1 = get_channel(0)
            x2 = get_channel(1)
            x3 = get_channel(2)
            # inputs = inputs.to(device)
            x1 = x1.to(device)
            x2 = x2.to(device)
            x3 = x3.to(device)
            output = model(x1, x2, x3).squeeze()
            targets = targets.to(device)
            loss = loss_fn(output,targets.float()) 
            valid_loss += loss.data.item() * x1.size(0)

            # print(output)


            output = (output>0.5).float()
            correct = (output == targets.float()).float().sum()

            # print(targets)

            # correct = torch.eq(torch.max(F.softmax(output, dim=1), dim=1)[1], targets).view(-1)
            num_correct += correct #torch.sum(correct).item()
            num_examples += x1.shape[0]
            # print(num_correct/num_examplesc)
        valid_loss /= len(val_loader.dataset)

        wandb.log({"Validation Loss": valid_loss})
        wandb.log({"Testing Acc": num_correct / num_examples})
        print('Epoch: {}, Training Loss: {:.4f}, Validation Loss: {:.4f}, accuracy = {:.4f}'.format(epoch, 0, valid_loss, num_correct / num_examples))

        checkpoint = {
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'optimizer': optimizer.state_dict()
        }
        torch.save(checkpoint, "./" + model_path + str(epoch) + '.pt')
    

class new_PCNN(nn.Module):
    def __init__(self):
        super().__init__()
        self.resnet = torchvision.models.resnet50(pretrained=True)
        self.resnet.conv1 = nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.resnet.fc = nn.Linear(in_features=2048, out_features=1024, bias=True)

        self.linear_layers = nn.Sequential(
            nn.Linear(in_features=3072, out_features=1024, bias=True),
            nn.ReLU(),
            nn.Dropout(),
            nn.Linear(in_features=1024, out_features=1, bias=True),
            nn.Sigmoid()
        )


    def forward(self, x1, x2, x3):
        x1 = self.resnet(x1)
        x2 = self.resnet(x2)
        x3 = self.resnet(x3)
        # x4 = self.resnet(x4)

        x1 = x1.view(x1.size(0), -1)
        x2 = x2.view(x2.size(0), -1)
        x3 = x3.view(x3.size(0), -1)
        # x4 = x4.view(x4.size(0), -1)

        x = torch.cat((x1, x2, x3), dim=1)
        x = self.linear_layers(x)
        # x = compute_rotation_matrix_from_ortho6d(x)

        return x


batch_size= 64
img_dimensions = 64

# Normalize to the ImageNet mean and standard deviation
# Could calculate it for the cats/dogs data set, but the ImageNet
# values give acceptable results here.
img_transforms = transforms.Compose([
    transforms.Resize((img_dimensions, img_dimensions)),
    transforms.ToTensor()
    # transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225] )
    ])

img_test_transforms = transforms.Compose([
    transforms.Resize((img_dimensions,img_dimensions)),
    transforms.ToTensor()
    # transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225] )
    ])

def check_image(path):
    try:
        im = Image.open(path)
        return True
    except:
        return False

# print("import3")

train_data_path = "./stable_vs_unstable_gripper_1/train/"
train_data = torchvision.datasets.ImageFolder(root=train_data_path,transform=img_transforms)

validation_data_path = "./stable_vs_unstable_gripper_1/validation/"
validation_data = torchvision.datasets.ImageFolder(root=validation_data_path,transform=img_test_transforms)

# test_data_path = "/home/wtf/dogs-vs-cats/test/"
# test_data = torchvision.datasets.ImageFolder(root=test_data_path,transform=img_test_transforms, is_valid_file=check_image)

num_workers = 6
train_data_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=num_workers)
validation_data_loader = torch.utils.data.DataLoader(validation_data, batch_size=batch_size, shuffle=False, num_workers=num_workers)
# test_data_loader = torch.utils.data.DataLoader(test_data, batch_size=batch_size, shuffle=False, num_workers=num_workers)

# train_dataset = HDF5Dataset('/data/dataset_stable_train_5.hdf5')

# train_data_loader = data.DataLoader(train_data, batch_size=256, shuffle=True)

# test_dataset = HDF5Dataset('/data/dataset_stable_test_5.hdf5')

# validation_data_loader = data.DataLoader(test_dataset, batch_size=32, shuffle=True)


print("1")

if torch.cuda.is_available():
    device = torch.device("cuda") 
else:
    device = torch.device("cpu")

from datetime import datetime
time = datetime.now().strftime("%Y-%m-%d-%H:%M")
model_path = 'models/' + time + '/'
try:
    os.mkdir(model_path)
except:
    pass
print("1=2")
print(f'Num training images: {len(train_data_loader.dataset)}')
print(f'Num validation images: {len(validation_data_loader.dataset)}')

network = new_PCNN()
network.to(device)
optimizer = optim.Adam(network.parameters(), lr=0.001)
train(network, optimizer, torch.nn.BCELoss(), train_data_loader, validation_data_loader, epochs=100, device=device)