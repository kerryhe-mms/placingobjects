import sys
from matplotlib import pyplot
from keras.utils import to_categorical
from keras.applications.vgg16 import VGG16
from keras.models import Model
from keras.layers import Dense
from keras.layers import Flatten
from keras import optimizers
# from wandb import magic
from tensorflow import keras
from keras.optimizers import SGD
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator
from keras import applications
# from wandb.keras import WandbCallback
from keras.callbacks import ModelCheckpoint
import cv2
 
# define cnn model
def define_model():
	# model = VGG16(weights='imagenet', include_top=False, )
	prev_model = applications.VGG16(weights='imagenet', include_top=False, input_shape=(224,224,3))
	print('Model loaded.')
	print(len(prev_model.layers))

	# build a classifier model to put on top of the convolutional model
	top_model = Sequential()
	top_model.add(Flatten(input_shape=prev_model.output_shape[1:]))
	top_model.add(Dense(256, activation='relu'))

	# top_model.add(Dropout(0.5))
	top_model.add(Dense(1, activation='sigmoid'))

	# note that it is necessary to start with a fully-trained
	# classifier, including the top classifier,
	# in order to successfully do fine-tuning
	# top_model.load_weights(top_model_weights_path)

	# add the model on top of the convolutional base
	model = Model(inputs=prev_model.input, outputs=top_model(prev_model.output))

	# set the first 25 layers (up to the last conv block)
	# to non-trainable (weights will not be updated)
	print(len(model.layers))
	for layer in model.layers[:15]:
		layer.trainable = False

	# compile the model with a SGD/momentum optimizer
	# and a very slow learning rate.
	model.compile(loss='binary_crossentropy',
				optimizer=optimizers.SGD(lr=1e-4, momentum=0.9),
				metrics=['accuracy'])

	return model
 
# plot diagnostic learning curves
def summarize_diagnostics(history):
	# plot loss
	pyplot.subplot(211)
	pyplot.title('Cross Entropy Loss')
	pyplot.plot(history.history['loss'], color='blue', label='train')
	pyplot.plot(history.history['val_loss'], color='orange', label='test')
	# plot accuracy
	pyplot.subplot(212)
	pyplot.title('Classification Accuracy')
	pyplot.plot(history.history['accuracy'], color='blue', label='train')
	pyplot.plot(history.history['val_accuracy'], color='orange', label='test')
	# save plot to file
	filename = sys.argv[0].split('/')[-1]
	pyplot.savefig(filename + '_plot.png')
	pyplot.close()
 
# run the test harness for evaluating a model
def run_test_harness():
	# define model
	model = define_model()

	train_datagen = ImageDataGenerator(
		shear_range=0.2,
		zoom_range=0.2,
		horizontal_flip=True)

	# this is the augmentation configuration we will use for testing:
	# only rescaling
	test_datagen = ImageDataGenerator()

	train_it = train_datagen.flow_from_directory('./stable_vs_unstable/train/',
		class_mode='binary', batch_size=128, target_size=(224, 224))

	test_it = test_datagen.flow_from_directory('./stable_vs_unstable/validation/',
		class_mode='binary', batch_size=128, target_size=(224, 224))
	# fit model
	filepath="weights-improvement-{epoch:02d}-{val_accuracy:.2f}.hdf5"
	checkpoint = ModelCheckpoint(filepath, monitor='val_accuracy', verbose=1, save_best_only=True, mode='max')

	callbacks_list = [checkpoint]#, WandbCallback(data_type="image", labels=["stable","unstable"])]

	history = model.fit_generator(train_it, steps_per_epoch=len(train_it),
		validation_data=test_it, validation_steps=len(test_it), epochs=50, verbose=1,
		callbacks=callbacks_list)
	# evaluate model
	print(history)
	_, acc = model.evaluate_generator(test_it, steps=len(test_it), verbose=1)
	print('> %.3f' % (acc * 100.0))
	# learning curves
	summarize_diagnostics(history)
 
# entry point, run the test harness
run_test_harness()