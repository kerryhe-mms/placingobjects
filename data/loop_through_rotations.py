import math,random, numpy as np, cv2
import sys
from matplotlib import pyplot
from keras.utils import to_categorical
from keras.applications.vgg16 import VGG16
from keras.models import Model
from keras.layers import Dense
from keras.layers import Flatten
from keras import optimizers
from tensorflow import keras
from keras.optimizers import SGD
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator
from keras import applications
from keras.callbacks import ModelCheckpoint
import cv2
from keras import backend as K
 
# define cnn model
def define_model():
    # model = VGG16(weights='imagenet', include_top=False, )
    prev_model = applications.VGG16(weights='imagenet', include_top=False, input_shape=(224,224,3))
    print('Model loaded.')
    print(len(prev_model.layers))

    # build a classifier model to put on top of the convolutional model
    top_model = Sequential()
    top_model.add(Flatten(input_shape=prev_model.output_shape[1:]))
    top_model.add(Dense(256, activation='relu'))

    top_model.add(Dense(1, activation='sigmoid'))

    model = Model(inputs=prev_model.input, outputs=top_model(prev_model.output))

    model.load_weights("weights-improvement-22-0.97.hdf5")
    return model

    # return model

model = define_model()
image = np.zeros((224,224))
for i in range(224):
    for j in range(224):

        phi = i * 2 * math.pi
        theta = i * math.pi

        y = math.sin(phi)
        x = math.cos(phi)

        qx = x * math.sin(theta/2)
        qy = y * math.sin(theta/2)
        qz = 0
        qw = math.cos(theta/2)

        quat = [qx,qy,qz,qw]

        tensor = model(K.constant(np.expand_dims(np.stack((image,)*3,axis=-1),0)))
        print(K.eval(tensor))


        
        

cv2.imshow("image", image)
cv2.waitKey(0)
        