import math,random, numpy as np, cv2
import sys
from matplotlib import pyplot
from keras.utils import to_categorical
from keras.applications.vgg16 import VGG16
from keras.models import Model
from keras.layers import Dense
from keras.layers import Flatten
from keras import optimizers
from tensorflow import keras
from keras.optimizers import SGD
from keras.models import Sequential
from keras.layers import Dropout, Flatten, Dense
from keras.preprocessing.image import ImageDataGenerator
from keras import applications
from keras.callbacks import ModelCheckpoint
import cv2
from keras import backend as K
from scipy.spatial.transform import Rotation
from keras.backend.tensorflow_backend import set_session
from keras.backend.tensorflow_backend import clear_session
from keras.backend.tensorflow_backend import get_session
import tensorflow, gc
from torch import nn
from torch.autograd import Variable
from torch.utils import data
from keras.layers.convolutional import Conv2D, AtrousConvolution2D

import torch, torchvision


def keras_to_pyt(km, pm=None):
	weight_dict = dict()
	print(km.layers)
	for index, layer in enumerate(km.layers):
		if (type(layer) is Conv2D) or (type(layer) is AtrousConvolution2D):
			weight_dict[layer.get_config()['name'] + '.weight'] = np.transpose(layer.get_weights()[0], (3, 2, 0, 1))
			weight_dict[layer.get_config()['name'] + '.bias'] = layer.get_weights()[1]
		elif type(layer) is keras.layers.Dense:
			weight_dict[layer.get_config()['name'] + '.weight'] = np.transpose(layer.get_weights()[0], (1, 0))
			weight_dict[layer.get_config()['name'] + '.bias'] = layer.get_weights()[1]
	# if pm:
	# 	pyt_state_dict = pm.state_dict()
	# 	print(pyt_state_dict.keys())
	# 	print(weight_dict.keys())

	# 	for key in pyt_state_dict.keys():
	# 		pyt_state_dict[key] = (weight_dict[key])
	# 	pm.load_state_dict(pyt_state_dict)
	# 	return pm
	return weight_dict


prev_model = applications.VGG16(weights='imagenet', include_top=False, input_shape=(224,224,3))

# build a classifier model to put on top of the convolutional model
top_model = Sequential()
top_model.add(Flatten(input_shape=prev_model.output_shape[1:]))
top_model.add(Dense(256, activation='relu'))

top_model.add(Dense(1, activation='sigmoid'))

model = Model(inputs=prev_model.input, outputs=top_model(prev_model.output))

model.load_weights("./data/weights/weights-improvement-22-0.97.hdf5")
weights = model.get_weights()


pt_model = torchvision.models.vgg16(pretrained=False)


csize = pt_model.features(torch.zeros(1,3,224,224)).flatten().shape[0]

pt_model.classifier = nn.Sequential(
            nn.Linear(csize, 256),
            nn.ReLU(),
            nn.Linear(256, 1),
            nn.Sigmoid()
        )

wd = keras_to_pyt(model, pt_model)
print(wd.keys())
# for i in weights:
#     print(np.transpose(i).shape)
#     input()



print(pt_model)

pt_model.features[0].weight.data = torch.from_numpy(wd["block1_conv1.weight"])
pt_model.features[0].bias.data = torch.from_numpy(wd["block1_conv1.bias"])

pt_model.features[2].weight.data = torch.from_numpy(wd["block1_conv2.weight"])
pt_model.features[2].bias.data = torch.from_numpy(wd["block1_conv2.bias"])

pt_model.features[5].weight.data = torch.from_numpy(wd["block2_conv1.weight"])
pt_model.features[5].bias.data = torch.from_numpy(wd["block2_conv1.bias"])

pt_model.features[7].weight.data = torch.from_numpy(wd["block2_conv2.weight"])
pt_model.features[7].bias.data = torch.from_numpy(wd["block2_conv2.bias"])

pt_model.features[10].weight.data = torch.from_numpy(wd["block3_conv1.weight"])
pt_model.features[10].bias.data = torch.from_numpy(wd["block3_conv1.bias"])

pt_model.features[12].weight.data = torch.from_numpy(wd["block3_conv2.weight"])
pt_model.features[12].bias.data = torch.from_numpy(wd["block3_conv2.bias"])

pt_model.features[14].weight.data = torch.from_numpy(wd["block3_conv3.weight"])
pt_model.features[14].bias.data = torch.from_numpy(wd["block3_conv3.bias"])


pt_model.features[17].weight.data = torch.from_numpy(wd["block4_conv1.weight"])
pt_model.features[17].bias.data = torch.from_numpy(wd["block4_conv1.bias"])

pt_model.features[19].weight.data = torch.from_numpy(wd["block4_conv2.weight"])
pt_model.features[19].bias.data = torch.from_numpy(wd["block4_conv2.bias"])

pt_model.features[21].weight.data = torch.from_numpy(wd["block4_conv3.weight"])
pt_model.features[21].bias.data = torch.from_numpy(wd["block4_conv3.bias"])

pt_model.features[24].weight.data = torch.from_numpy(wd["block5_conv1.weight"])
pt_model.features[24].bias.data = torch.from_numpy(wd["block5_conv1.bias"])

pt_model.features[26].weight.data = torch.from_numpy(wd["block5_conv2.weight"])
pt_model.features[26].bias.data = torch.from_numpy(wd["block5_conv2.bias"])

pt_model.features[28].weight.data = torch.from_numpy(wd["block5_conv3.weight"])
pt_model.features[28].bias.data = torch.from_numpy(wd["block5_conv3.bias"])


pt_model.classifier[0].weight.data = torch.from_numpy(np.transpose(weights[26], (1, 0)))
pt_model.classifier[0].bias.data = torch.from_numpy(weights[27])

pt_model.classifier[2].weight.data = torch.from_numpy(np.transpose(weights[28], (1, 0)))
pt_model.classifier[2].bias.data = torch.from_numpy(weights[29])


print(pt_model(torch.zeros(1,3,224,224)))

print(K.eval(model(K.constant(np.zeros((1,224,224,3))))))
