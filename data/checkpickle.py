import cv2, pickle, numpy as np, os
import open3d
import matplotlib.pyplot as plt

l = open('training_data/5.td', 'rb')
print(open3d.__version__)

def rotation_matrix_from_vectors(vec1, vec2):
    """ Find the rotation matrix that aligns vec1 to vec2
    :param vec1: A 3d "source" vector
    :param vec2: A 3d "destination" vector
    :return mat: A transform matrix (3x3) which when applied to vec1, aligns it with vec2.
    """
    a, b = (vec1 / np.linalg.norm(vec1)).reshape(3), (vec2 / np.linalg.norm(vec2)).reshape(3)
    v = np.cross(a, b)
    c = np.dot(a, b)
    s = np.linalg.norm(v)
    kmat = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    rotation_matrix = np.eye(3) + kmat + kmat.dot(kmat) * ((1 - c) / (s ** 2))
    return rotation_matrix

l = open('training_data/6.td', 'rb')
dic = pickle.load(l)

depth_left = dic['depth_left']
depth_front = dic['depth_front']
depth_right = dic['depth_right']

front_transform = np.array([[-8.93487276e-08,-9.99999881e-01,-1.19252895e-07,1.28871725e-07],
 [-4.88281134e-04,-8.93487276e-08,9.99999762e-01,-4.99621463e-01],
 [-9.99999762e-01,1.19252895e-07,-4.88191727e-04,7.75244066e-01],
 [0,0,0,1]])

left_transform = np.array([[-1.00000000e+00,1.80001045e-14,1.50995812e-07,4.99999925e-01],
 [ 1.50995812e-07,1.19209304e-07,1.00000006e+00,-5.00000075e-01],
 [ 1.42155439e-15,1.00000006e+00,-1.78813956e-07,2.50000104e-01],
 [0,0,0,1]])

right_transform = np.array([[ 1.00000000e+00,0.00000000e+00,0.00000000e+00,-5.00000000e-01],
 [ 0.00000000e+00,-8.34465126e-07,1.00000006e+00,-4.99999821e-01],
 [-0.00000000e+00,-1.00000006e+00,-8.34465126e-07,2.50000283e-01],
 [0,0,0,1]])


depth_left[depth_left >= 0.4] = float('NaN')
depth_right[depth_right >= 0.4] = float('NaN')
depth_front[depth_front >= 0.4] = float('NaN')



i = open3d.camera.PinholeCameraIntrinsic(300, 300, -259.81, -259.81, 150, 150)

dl = open3d.geometry.Image(depth_left)
dr = open3d.geometry.Image(depth_right)
df = open3d.geometry.Image(depth_front)




pcd_l = open3d.geometry.PointCloud.create_from_depth_image(dl, i, extrinsic=left_transform)

pcd_r = open3d.geometry.PointCloud.create_from_depth_image(dr, i, extrinsic=front_transform)

pcd_f = open3d.geometry.PointCloud.create_from_depth_image(df, i, extrinsic=right_transform)





pcd = pcd_f + pcd_l + pcd_r

pcd.transform([[1,0,0,0],[0,-1,0,0],[0,0,-1,0],[0,0,0,1]])


with open3d.utility.VerbosityContextManager(
        open3d.utility.VerbosityLevel.Debug) as cm:
    labels = np.array(
        pcd.cluster_dbscan(eps=0.02, min_points=10, print_progress=True))

max_label = labels.max()
print(f"point cloud has {max_label + 1} clusters")

colors = plt.get_cmap("tab20")(labels / (max_label if max_label > 0 else 1))

colors[labels < 0] = 0


from collections import Counter
print(Counter(labels))

pcd.colors = open3d.utility.Vector3dVector(colors[:, :3])

p = np.asarray(pcd.points)
p[labels != 0] = 0



pcd = open3d.geometry.PointCloud()
pcd.points = open3d.utility.Vector3dVector(p)

cl, ind = pcd.remove_statistical_outlier(nb_neighbors=20,
                                                    std_ratio=2.0)

pcd = pcd.select_by_index(ind)

tetra_mesh, pt_map = open3d.geometry.TetraMesh.create_from_point_cloud(pcd)

lol = None

for alpha in np.logspace(np.log10(0.5), np.log10(0.1), num=4):
    print(f"alpha={alpha:.3f}")
    mesh = open3d.geometry.TriangleMesh.create_from_point_cloud_alpha_shape(
        pcd, alpha, tetra_mesh, pt_map)
    mesh.compute_vertex_normals()
    # open3d.visualization.draw_geometries([mesh], mesh_show_back_face=True)

    lol = mesh.sample_points_uniformly(number_of_points=10000)


    # open3d.visualization.draw_geometries([lol])


# print("aaa")

# hull, _ = pcd.compute_convex_hull()
# print("aaa")

# hull_ls = open3d.geometry.LineSet.create_from_triangle_mesh(hull)
# print("aaa")      

# hull_ls.paint_uniform_color((1, 0, 0))
# print("aaa")
print(lol)

# open3d.visualization.draw_geometries([hull], width=500, height=500)

plane_model, inliers = pcd.segment_plane(0.001, 3, 1000)

[a, b, c, d] = plane_model

print(f"Plane model: {a:.2f}x + {b:.2f}y + {c:.2f}z + {d:.2f} = 0")

inlier_cloud = pcd.select_by_index(inliers)
inlier_cloud.paint_uniform_color([1.0, 0, 0])

#select_by_index

outlier_cloud = pcd.select_by_index(inliers, invert=True)

outlier_cloud.paint_uniform_color([0.0, 1.0, 0.1])

open3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])

# print(inlier_cloud.normals[0])

#Average the normal vectors
vector = sum(inlier_cloud.normals)
vector = vector / np.linalg.norm(vector)

down_vector = [0, 1, 0]

#Find Dot Product and Cross Product
dot = np.dot(vector, down_vector)
cross = np.cross(vector, down_vector)
#Get the sine value from cross product
sin = np.linalg.norm(cross)

#Construct matrix

R = rotation_matrix_from_vectors(vector, down_vector)
G = np.zeros((4, 4))
G[:3,:3] = R
G[3,3] = 1


open3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])
#Apply transformation to point cloud
inlier_cloud.transform(G)
outlier_cloud.transform(G)

open3d.visualization.draw_geometries([inlier_cloud, outlier_cloud])


vector = sum(inlier_cloud.normals)
vector = vector / np.linalg.norm(vector)

path = os.path.realpath('/home/akan/test/')
cv2.imwrite('/home/akan/test/' +'left.png', np.transpose(x*255*2, (1,2,0))) 
print(vector)
# from open3d import *

# rgbd = create_rgbd_image_from_color_and_depth(color, depth, convert_rgb_to_intensity = False)
# pcd = create_point_cloud_from_rgbd_image(rgbd, pinhole_camera_intrinsic)

path = os.path.realpath('/home/akan/test/')
cv2.imwrite('/home/akan/test/' +'front.png', np.transpose(x*255*2, (1,2,0))) 
# # flip the orientation, so it looks upright, not upside-down
# pcd.transform([[1,0,0,0],[0,-1,0,0],[0,0,-1,0],[0,0,0,1]])

#draw_geometries([pcd])    # visualize the point cloud

path = os.path.realpath('/home/akan/test/')
cv2.imwrite('/home/akan/test/' +'right.png', np.transpose(x*255*2, (1,2,0))) 
#size = (300,300)
