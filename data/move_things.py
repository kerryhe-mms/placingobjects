
import os, shutil
import cv2
import os
import numpy as np

import pickle
import h5py

objects = ["Skillet","CookieJar","Rocket","Juice","Klein","RedCup","MilkBottle","FlowerPot","HygieneSpray","PharmaceuticalBottle","Basket","ShotGlass","Martini","Milk","Mortar","CokePlasticSmall","Wineglass","ChessPawn","SmallMilk","Sprudelflasche","Salt","SaladPlate","Pot","DinnerPlate","SoapDispenser","SoupBowl","Bowl","BathDetergent","TeaCup","Pepper","Spray","Superglue","WhiteCup","Oscar","Waterglass","BabyBottle","PlasticCup","Vasepot","FlowerCup","Globe","HamburgerSauce","CokePlasticLarge","Trophycup","ChessKing","CokePlasticSmallGrasp","TrophySphere","GreenCup","FoamCup","CoffeeCup","Glassbowl"]

# test_objects = ["CoffeeCup","FoamCup","Klein","Pot","Salt"] # Set 1
test_objects = ["BabyBottle","Glassbowl","HygieneSpray","PharmaceuticalBottle","RedCup"] # Set 2
# test_objects = ["BathDetergent", "CookieJar","Oscar","Milk","ShotGlass"] # Set 3
# test_objects = ["Globe","Juice","PlasticCup","Rocket","Spray"] # Set 4
# test_objects = ["Basket","Mortar","Skillet","SoupBowl","SoapDispenser"] # Set 5

size = (64,64)

dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/stable"
data_list = list(os.listdir(dir_path))
train_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/stable_vs_unstable_4/train"

test_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/stable_vs_unstable_4/validation"

for i, f in enumerate(data_list):

        # print(f)
    obj_idex = int(f.split("_")[0])
    # print(objects[obj_idex])

    test = objects[obj_idex] in test_objects

    loc = test_location if test else train_location

    pkl = open(dir_path + "/" + f, 'rb')
    dic = pickle.load(pkl)

    # print(dic["placements"])



    # Unpack pickle file
    depth_left = dic['depth_left']
    depth_front = dic['depth_front']
    depth_right = dic['depth_right']

    # cv2.imshow("left", depth_left)
    # cv2.waitKey(0)

    # quat_dict = dic['placements'][0]
    # quaternion = [quat_dict['w_quat'], quat_dict['x_quat'], quat_dict['y_quat'], quat_dict['z_quat']]
    print(depth_left.min())
    print(depth_left.max())

    # Reshape image
    depth_left = np.expand_dims(cv2.resize(depth_left, dsize=size), 0) * 2 * 255
    depth_front = np.expand_dims(cv2.resize(depth_front, dsize=size), 0) * 2 * 255
    depth_right = np.expand_dims(cv2.resize(depth_right, dsize=size), 0) * 2 * 255

    image = np.stack((depth_left, depth_front, depth_right), axis=-1).squeeze()
    # print(depth_right.shape)
    # print(image.shape)

    # print(loc)
    cv2.imwrite(loc + "/stable/" + f[:-3] +".png", image)    
    # shutil.copy(dir_path + '/' + f, loc + "/stable/" + f)
    print(i,"/",49900*2)

dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/unstable"
data_list = list(os.listdir(dir_path))

train_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/stable_vs_unstable_4/train"

test_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/stable_vs_unstable_4/validation"

for i, f in enumerate(data_list):


        # print(f)
    obj_idex = int(f.split("_")[0])
    # print(objects[obj_idex])

    test = objects[obj_idex] in test_objects

    loc = test_location if test else train_location

    pkl = open(dir_path + "/" + f, 'rb')
    dic = pickle.load(pkl)

    # print(dic["placements"])



    # Unpack pickle file
    depth_left = dic['depth_left']
    depth_front = dic['depth_front']
    depth_right = dic['depth_right']

    # cv2.imshow("left", depth_left)
    # cv2.waitKey(0)

    # quat_dict = dic['placements'][0]
    # quaternion = [quat_dict['w_quat'], quat_dict['x_quat'], quat_dict['y_quat'], quat_dict['z_quat']]

    # Reshape image
    depth_left = np.expand_dims(cv2.resize(depth_left, dsize=size), 0) * 2 * 255
    depth_front = np.expand_dims(cv2.resize(depth_front, dsize=size), 0) * 2 * 255
    depth_right = np.expand_dims(cv2.resize(depth_right, dsize=size), 0) * 2 * 255

    image = np.stack((depth_left, depth_front, depth_right), axis=-1).squeeze()

    # print(loc)
    cv2.imwrite(loc + "/unstable/" + f[:-3] + ".png", image)    
    # shutil.copy(dir_path + '/' + f, loc + "/stable/" + f)
    # print(i,"/",49900*2)

        # print(f)
    # obj_idex = int(f.split("_")[0])
    # # print(objects[obj_idex])

    # test = objects[obj_idex] in test_objects

    # loc = test_location if test else train_location

    # shutil.copy(dir_path + '/' + f, loc + "/unstable/" + f)
    print(i+49900,"/",49900*2)
