
import os, shutil
import cv2
import os
import numpy as np

import pickle
import h5py

from collections import defaultdict

objects = ["Skillet","CookieJar","Rocket","Juice","Klein","RedCup","MilkBottle","FlowerPot","HygieneSpray","PharmaceuticalBottle","Basket","ShotGlass","Martini","Milk","Mortar","CokePlasticSmall","Wineglass","ChessPawn","SmallMilk","Sprudelflasche","Salt","SaladPlate","Pot","DinnerPlate","SoapDispenser","SoupBowl","Bowl","BathDetergent","TeaCup","Pepper","Spray","Superglue","WhiteCup","Oscar","Waterglass","BabyBottle","PlasticCup","Vasepot","FlowerCup","Globe","HamburgerSauce","CokePlasticLarge","Trophycup","ChessKing","CokePlasticSmallGrasp","TrophySphere","GreenCup","FoamCup","CoffeeCup","Glassbowl"]

test_objects = ["CoffeeCup","FoamCup","Klein","Pot","Salt"] # Set 1
# test_objects = ["BabyBottle","Glassbowl","HygieneSpray","PharmaceuticalBottle","RedCup"] # Set 2
# test_objects = ["BathDetergent", "CookieJar","Oscar","Milk","ShotGlass"] # Set 3
# test_objects = ["Globe","Juice","PlasticCup","Rocket","Spray"] # Set 4
# test_objects = ["Basket","Mortar","Skillet","SoupBowl","SoapDispenser"] # Set 5

size = (64,64)

train_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/stable_vs_unstable_gripper_1/train"
test_location = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/stable_vs_unstable_gripper_1/validation"

dict_stable = defaultdict(int)
dict_unstable = defaultdict(int)

dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/training_data_gripper"
data_list = list(os.listdir(dir_path))

for i, f in enumerate(data_list):

    obj_name = f.split("/")[-1].split("_")[0].split('-')[0]
    test = obj_name in test_objects
    loc = test_location if test else train_location

    pkl = open(dir_path + "/" + f, 'rb')
    dic = pickle.load(pkl)

    # Unpack pickle file
    depth_left = dic['depth_left']
    depth_front = dic['depth_front']
    depth_right = dic['depth_right']

    # Reshape image
    depth_left = np.expand_dims(cv2.resize(depth_left, dsize=size), 0) * 2 * 255
    depth_front = np.expand_dims(cv2.resize(depth_front, dsize=size), 0) * 2 * 255
    depth_right = np.expand_dims(cv2.resize(depth_right, dsize=size), 0) * 2 * 255

    image = np.stack((depth_left, depth_front, depth_right), axis=-1).squeeze()

    if dic["placements"][0]['stable']:
        dict_stable[obj_name] += 1
        if dict_stable[obj_name] <= 1000:
            cv2.imwrite(loc + "/stable/" + f[:-3] +".png", image)
    else:
        dict_unstable[obj_name] += 1
        if dict_unstable[obj_name] <= 1000:
            cv2.imwrite(loc + "/unstable/" + f[:-3] + ".png", image)    

    if i % 100 == 0:
        print(i, "/", len(data_list))


dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/training_data_gripper_stable"
data_list = list(os.listdir(dir_path))

for i, f in enumerate(data_list):

    obj_name = f.split("/")[-1].split("_")[0].split('-')[0]
    test = obj_name in test_objects
    loc = test_location if test else train_location

    pkl = open(dir_path + "/" + f, 'rb')
    dic = pickle.load(pkl)

    # Unpack pickle file
    depth_left = dic['depth_left']
    depth_front = dic['depth_front']
    depth_right = dic['depth_right']

    # Reshape image
    depth_left = np.expand_dims(cv2.resize(depth_left, dsize=size), 0) * 2 * 255
    depth_front = np.expand_dims(cv2.resize(depth_front, dsize=size), 0) * 2 * 255
    depth_right = np.expand_dims(cv2.resize(depth_right, dsize=size), 0) * 2 * 255

    image = np.stack((depth_left, depth_front, depth_right), axis=-1).squeeze()

    if dic["placements"][0]['stable']:
        dict_stable[obj_name] += 1
        if dict_stable[obj_name] <= 1000:
            cv2.imwrite(loc + "/stable/" + f[:-3] +".png", image)
    else:
        dict_unstable[obj_name] += 1
        if dict_unstable[obj_name] <= 1000:
            cv2.imwrite(loc + "/unstable/" + f[:-3] + ".png", image)    

    if i % 100 == 0:
        print(i, "/", len(data_list))