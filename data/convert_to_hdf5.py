import cv2
import os
import numpy as np

import pickle
import h5py

size = (64,64)

# Get list of all training data
dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/training_data_4"
data_list = list(os.listdir(dir_path))

# Initialise HDF5 file
file_name = h5py.File("data/dataset_4.hdf5", "w")

dataset_left = file_name.create_dataset("depth_left", (len(data_list), 1, size[0], size[1]), dtype=np.float32)
dataset_front = file_name.create_dataset("depth_front", (len(data_list), 1, size[0], size[1]), dtype=np.float32)
dataset_right = file_name.create_dataset("depth_right", (len(data_list), 1, size[0], size[1]), dtype=np.float32)
# dataset_back = file_name.create_dataset("depth_back", (len(data_list), 1, size[0], size[1]), dtype=np.float32)

dataset_quaternion = file_name.create_dataset("quaternion", (len(data_list), 4), dtype=np.float32)

# Iterate through all training data
for i, f in enumerate(data_list):
    try:
        pkl = open(dir_path + "/" + f, 'rb')
        dic = pickle.load(pkl)

        # Unpack pickle file
        depth_left = dic['depth_left']
        depth_front = dic['depth_front']
        depth_right = dic['depth_right']
        # depth_back = dic['depth_back']

        quat_dict = dic['placements'][0]
        quaternion = [quat_dict['w_quat'], quat_dict['x_quat'], quat_dict['y_quat'], quat_dict['z_quat']]

        # Reshape image
        depth_left = np.expand_dims(cv2.resize(depth_left, dsize=size), 0) * 2 - 0.5
        depth_front = np.expand_dims(cv2.resize(depth_front, dsize=size), 0) * 2 - 0.5
        depth_right = np.expand_dims(cv2.resize(depth_right, dsize=size), 0) * 2 - 0.5
        # depth_back= np.expand_dims(cv2.resize(depth_back, dsize=size), 0) * 2 - 0.5

        quaternion = np.array(quaternion).astype(np.float32)

        # Output to file
        dataset_left[i, :] = depth_left
        dataset_front[i, :] = depth_front
        dataset_right[i, :] = depth_right
        # dataset_back[i, :] = depth_back
        dataset_quaternion[i, :] = quaternion

        if i%100 == 0:
            print("Imported ", i, " / ", len(data_list))
    except:
        print("Skipping: ", f)

print("Transfer complete!")

file_name.close()