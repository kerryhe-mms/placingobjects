import cv2
import os
import numpy as np

import pickle
import h5py
from collections import defaultdict

size = (64,64)

# Get list of all training data
dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/training_data_gripper_forwards"
data_list = list(os.listdir(dir_path))

stable_count = 0
unstable_count = 0

dict_stable = defaultdict(int)
dict_unstable = defaultdict(int)
dict_total = defaultdict(int)

# for line in open("stable.csv"):
#     data = line.strip().split(",")
#     dict_stable[data[0]] = int(data[1])

# for line in open("unstable.csv"):
#     data = line.strip().split(",")
#     dict_unstable[data[0]] = int(data[1])

# Iterate through all training data
for i, f in enumerate(data_list):
    try:
        pkl = open(dir_path + "/" + f, 'rb')
        dic = pickle.load(pkl)

        if dic["placements"][0]['stable']:
            dict_stable[f.split("/")[-1].split("_")[0]] += 1
        else:
            dict_unstable[f.split("/")[-1].split("_")[0]] += 1

        dict_total[f.split("/")[-1].split("_")[0]] += 1

        if i%250 == 0 and i > 0:
            print("Imported ", i, " / ", len(data_list))

    except Exception as e:
        print(e)
        print("Skipping: ", f)


# # Get list of all training data
# dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/training_data_gripper_stable"
# data_list = list(os.listdir(dir_path))

# # Iterate through all training data
# for i, f in enumerate(data_list):
#     try:
#         pkl = open(dir_path + "/" + f, 'rb')
#         dic = pickle.load(pkl)

#         if dic["placements"][0]['stable']:
#             dict_stable[f.split("/")[-1].split("_")[0]] += 1
#         else:
#             dict_unstable[f.split("/")[-1].split("_")[0]] += 1

#         if i%250 == 0 and i > 0:
#             print("Imported ", i, " / ", len(data_list))

#     except Exception as e:
#         print(e)
#         print("Skipping: ", f)

import csv

with open('./stable_gf.csv', 'w') as f:
    for key in dict_stable.keys():
        f.write("%s,%s\n"%(key,dict_stable[key]))

with open('./unstable_gf.csv', 'w') as f:
    for key in dict_unstable.keys():
        f.write("%s,%s\n"%(key,dict_unstable[key]))

with open('./total_gf.csv', 'w') as f:
    for key in dict_total.keys():
        f.write("%s,%s\n"%(key,dict_total[key]))
            

print("Transfer complete!")