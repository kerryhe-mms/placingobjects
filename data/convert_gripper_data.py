import cv2
import os
import numpy as np

import pickle
import h5py

size = (64,64)

test_objects = ["CoffeeCup","FoamCup","Klein","Pot","Salt"] # Set 1
# test_objects = ["BabyBottle","Glassbowl","HygieneSpray","PharmaceuticalBottle","RedCup"] # Set 2
# test_objects = ["BathDetergent", "CookieJar","Oscar","Milk","ShotGlass"] # Set 3
# test_objects = ["Globe","Juice","PlasticCup","Rocket","Spray"] # Set 4
# test_objects = ["Basket","Mortar","Skillet","SoupBowl","SoapDispenser"] # Set 5

# Get list of all training data
dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/training_data_gripper"
data_list = list(os.listdir(dir_path))

# Initialise HDF5 file
file_name = h5py.File("dataset_gripper_1.hdf5", "w")

dataset_left = file_name.create_dataset("depth_left", (2000 * 45, 1, size[0], size[1]), dtype=np.float32)
dataset_front = file_name.create_dataset("depth_front", (2000 * 45, 1, size[0], size[1]), dtype=np.float32)
dataset_right = file_name.create_dataset("depth_right", (2000 * 45, 1, size[0], size[1]), dtype=np.float32)

dataset_quaternion = file_name.create_dataset("quaternion", (2000 * 45, 4), dtype=np.float32)
index = 0
# Iterate through all training data
for i, f in enumerate(data_list):
    try:
        
        obj_name = f.split("/")[-1].split("_")[0].split('-')[0]
        # print(obj_name)
        if obj_name in test_objects:
            continue
        # print(obj_name in test_objects)
        pkl = open(dir_path + "/" + f, 'rb')
        dic = pickle.load(pkl)

        # Unpack pickle file
        depth_left = dic['depth_left']
        depth_front = dic['depth_front']
        depth_right = dic['depth_right']
        # depth_back = dic['depth_back']

        quat_dict = dic['placements'][0]
        quaternion = [quat_dict['w_quat'], quat_dict['x_quat'], quat_dict['y_quat'], quat_dict['z_quat']]

        # Reshape image
        depth_left = np.expand_dims(cv2.resize(depth_left, dsize=size), 0) * 2 - 0.5
        depth_front = np.expand_dims(cv2.resize(depth_front, dsize=size), 0) * 2 - 0.5
        depth_right = np.expand_dims(cv2.resize(depth_right, dsize=size), 0) * 2 - 0.5

        quaternion = np.array(quaternion).astype(np.float32)

        # Output to file
        dataset_left[index, :] = depth_left
        dataset_front[index, :] = depth_front
        dataset_right[index, :] = depth_right
        dataset_quaternion[index, :] = quaternion
        index += 1
        if i%100 == 0:
            print("Imported ", i, " / ", len(data_list))
    except Exception as e:
        print(e)
        print("Skipping: ", f)

print("Transfer complete!")

file_name.close()