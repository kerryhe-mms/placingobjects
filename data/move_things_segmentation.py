
import os, shutil
import cv2
import os
import numpy as np

import pickle
import h5py

from collections import defaultdict

objects = ["Skillet","CookieJar","Rocket","Juice","Klein","RedCup","MilkBottle","FlowerPot","HygieneSpray","PharmaceuticalBottle","Basket","ShotGlass","Martini","Milk","Mortar","CokePlasticSmall","Wineglass","ChessPawn","SmallMilk","Sprudelflasche","Salt","SaladPlate","Pot","DinnerPlate","SoapDispenser","SoupBowl","Bowl","BathDetergent","TeaCup","Pepper","Spray","Superglue","WhiteCup","Oscar","Waterglass","BabyBottle","PlasticCup","Vasepot","FlowerCup","Globe","HamburgerSauce","CokePlasticLarge","Trophycup","ChessKing","CokePlasticSmallGrasp","TrophySphere","GreenCup","FoamCup","CoffeeCup","Glassbowl"]

test_objects = ["CoffeeCup","FoamCup","Klein","Pot","Salt"] # Set 1
# test_objects = ["BabyBottle","Glassbowl","HygieneSpray","PharmaceuticalBottle","RedCup"] # Set 2
# test_objects = ["BathDetergent", "CookieJar","Oscar","Milk","ShotGlass"] # Set 3
# test_objects = ["Globe","Juice","PlasticCup","Rocket","Spray"] # Set 4
# test_objects = ["Basket","Mortar","Skillet","SoupBowl","SoapDispenser"] # Set 5

size = (256,256)

save_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/segmentation_data_new/"

dir_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__))) + "/data/training_data_segmentation"

data_list = list(os.listdir(dir_path))

for i, f in enumerate(data_list):

    obj_name = f.split("/")[-1].split("_")[0].split('-')[0]
    test = obj_name in test_objects
    if test:
        continue
    # loc = test_location if test else train_location
    
    pkl = open(dir_path + "/" + f, 'rb')
    dic = pickle.load(pkl)

    # Unpack pickle file
    # print(dic.keys())
    rgb_left = np.array(dic['depth_back'])
    rgb_front = np.array(dic['rgb_left'])
    rgb_right = np.array(dic['rgb_front'])
    # print(rgb_right.shape)
    # Reshape im
    rgb_left = np.expand_dims(cv2.resize(rgb_left, dsize=size), 0) * 255
    rgb_front = np.expand_dims(cv2.resize(rgb_front, dsize=size), 0) * 255
    rgb_right = np.expand_dims(cv2.resize(rgb_right, dsize=size), 0) * 255

    rgb_left.astype(np.uint8)
    # Unpack pickle file
    depth_left = dic['depth_left']
    depth_front = dic['depth_front']
    depth_right = dic['depth_right']

    # Reshape image
    depth_left = np.expand_dims(cv2.resize(depth_left, dsize=size), 0) * 2 * 255
    depth_front = np.expand_dims(cv2.resize(depth_front, dsize=size), 0) * 2 * 255
    depth_right = np.expand_dims(cv2.resize(depth_right, dsize=size), 0) * 2 * 255

    # Unpack pickle file
    gt_left = dic['gt_left']
    gt_front = dic['gt_front']
    gt_right = dic['gt_right']

    # Reshape image
    gt_left = np.expand_dims(cv2.resize(gt_left, dsize=size), 0) * 2 * 255
    gt_front = np.expand_dims(cv2.resize(gt_front, dsize=size), 0) * 2 * 255
    gt_right = np.expand_dims(cv2.resize(gt_right, dsize=size), 0) * 2 * 255

    # print(gt_left.shape)
    # output = cv2.hconcat([depth_left, gt_left])
    cv2.imwrite(save_dir + str(i) + "left_gt.png", gt_left[0])
    cv2.imwrite(save_dir + str(i) + "right_gt.png", gt_right[0])
    cv2.imwrite(save_dir + str(i) + "front_gt.png", gt_front[0])

    # print(rgb_left[0].shape)
    # print(np.expand_dims(depth_left[0],-1).shape)
    rgbd_left = np.concatenate((rgb_left[0], np.expand_dims(depth_left[0],-1)), 2)
    rgbd_right = np.concatenate((rgb_right[0], np.expand_dims(depth_right[0],-1)), 2)
    rgbd_front = np.concatenate((rgb_front[0], np.expand_dims(depth_front[0],-1)), 2)

    # cv2.imwrite(save_dir + str(i) + "left_rgb.png", rgb_left[0])
    # cv2.imwrite(save_dir + str(i) + "right_rgb.png", rgb_right[0])
    # cv2.imwrite(save_dir + str(i) + "front_rgb.png", rgb_front[0])
    
    np.save(save_dir + str(i) + "left_rgbd", rgbd_left)
    np.save(save_dir + str(i) + "right_rgbd", rgbd_right)
    np.save(save_dir + str(i) + "front_rgbd", rgbd_front)
    # cv2.imwrite("rgbd.tif", rgbd[0])
    # cv2.waitKey(0)



    if i % 100 == 0:
        print(i, "/", len(data_list))